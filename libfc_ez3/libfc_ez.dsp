# Microsoft Developer Studio Project File - Name="libfc_visoly_xg1" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=libfc_visoly_xg1 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libfc_visoly_xg1.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libfc_visoly_xg1.mak" CFG="libfc_visoly_xg1 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libfc_visoly_xg1 - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "libfc_visoly_xg1 - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "libfc_visoly_xg1 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f libfc_visoly_xg1.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "libfc_visoly_xg1.exe"
# PROP BASE Bsc_Name "libfc_visoly_xg1.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "make"
# PROP Rebuild_Opt "rebuild"
# PROP Bsc_Name "libfc_visoly_xg1.bsc"
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "libfc_visoly_xg1 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f libfc_visoly_xg1.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "libfc_visoly_xg1.exe"
# PROP BASE Bsc_Name "libfc_visoly_xg1.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "make"
# PROP Rebuild_Opt "rebuild"
# PROP Bsc_Name "libfc_visoly_xg1.bsc"
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "libfc_visoly_xg1 - Win32 Release"
# Name "libfc_visoly_xg1 - Win32 Debug"

!IF  "$(CFG)" == "libfc_visoly_xg1 - Win32 Release"

!ELSEIF  "$(CFG)" == "libfc_visoly_xg1 - Win32 Debug"

!ENDIF 

# Begin Group "Source"

# PROP Default_Filter "C"
# Begin Source File

SOURCE=.\main.c
# End Source File
# End Group
# Begin Group "Include"

# PROP Default_Filter "H"
# End Group
# Begin Source File

SOURCE=.\dual.s
# End Source File
# Begin Source File

SOURCE=.\Makefile
# End Source File
# End Target
# End Project
