/*
 * Core device handling functions (font.c)
 * Part of libpogo, a c-library replacent for GBA
 * Programmed by Jonas Minnberg (Sasq)
 *
 * DESCRIPTION
 * Functions for handling devices - allowing users to
 * register new devices that gets called through the
 * open(),close(),read() etc functions defined here.
 *
 **/

#include "core.h"
#include "device.h"
//#include "gba_defs.h"


typedef struct {
	uint16 fd;
	Device *dev;
} FdMap;

typedef struct {
	Device *device;
	char *name;
	volatile void (*irqfunc)(void);
} RegDevice;

#define MAX_DEVICE 8
#define MAX_FD 32

static int dev_count = 0;
static RegDevice deviceList[MAX_DEVICE];

static int fdmap_count = 3;
static FdMap fdList[MAX_FD];

static int default_dev = 0;

void device_init(void)
{
	fdList[0].fd = -1;
	fdList[0].dev = NULL;
	fdList[1].fd = -1;
	fdList[1].dev = NULL;
	fdList[2].fd = -1;
	fdList[2].dev = NULL;
}

static Device *dev_fromname(const char *name, char **cutname)
{
	int i,l;
	char *cn = *cutname;
	for(i=0; i<dev_count; i++) {
		l = strlen(deviceList[i].name);
		if(strncmp(name, deviceList[i].name, l) == 0)
		{
			cn = (char *)&name[l];
			while(*cn == '/')
				cn++;
			*cutname = cn;
			return deviceList[i].device;
		}
	}
	*cutname = (char *)name;
	return deviceList[0].device;
}

static Device *dev_fromhandle(int *fd)
{
	Device *dev = fdList[*fd].dev;
	*fd = fdList[*fd].fd;
	return dev;
}

int device_register(Device *dev, char *name, volatile void (*irqfunc)(void), int fd)
{
	RegDevice *d = &deviceList[dev_count++];
	d->name = name;
	d->irqfunc = irqfunc;
	d->device = dev;

	if(fd > -1 && fd < 3)
	{
		fdList[fd].dev = dev;
		fdList[fd].fd = 0;
	}
	else
		default_dev = dev_count-1;

	return dev_count-1;
}

void device_doirq(void)
{
	int i;
	for(i=0; i<dev_count; i++)
		if(deviceList[i].irqfunc)
			deviceList[i].irqfunc();
}

int open(const char *name, int flags)
{
	int fd;
	Device *dev;
	char *cutname;

	if((dev = dev_fromname(name, &cutname)))
		if(dev->open) {
			fd = dev->open(cutname, flags);
			if(fd < 0)
				return fd;
			fdList[fdmap_count].dev = dev;
			fdList[fdmap_count].fd = fd;
			return fdmap_count++;
		}
	return -1;
}

int read(int fd, void *dest, int size)
{
	Device *dev;
	if((dev = dev_fromhandle(&fd)))
		if(dev->read)
			return dev->read(fd, dest, size);
	return -1;
}

int write(int fd, const void *dest, int size)
{
	Device *dev;
	if((dev = dev_fromhandle(&fd)))
		if(dev->write)
			return dev->write(fd, dest, size);
	return -1;
}

int close(int fd)
{
	FdMap *f;
	int oldfd = fd;
	Device *dev;
	if((dev = dev_fromhandle(&fd)))
		if(dev->close) {

			fdList[oldfd].dev = NULL;
			f = &fdList[fdmap_count-1];
			while(fdmap_count && f->dev == NULL) {
				fdmap_count--;
				f = &fdList[fdmap_count-1];
			}


			return dev->close(fd);
		}
	return -1;
}

int lseek(int fd, int offset, int origin)
{
	Device *dev;
	if((dev = dev_fromhandle(&fd)))
		if(dev->lseek)
			return dev->lseek(fd, offset, origin);
	return -1;
}

int ioctl(int fd, int request, ...)
{
	int rc = -1;
	Device *dev;
	va_list vl;
	va_start(vl, request);

	if((dev = dev_fromhandle(&fd)))
		if(dev->ioctl)
			rc = dev->ioctl(fd, request, vl);
	va_end(vl);
	return rc;
}

int stat(const char *name, struct stat *buffer)
{
	Device *dev;
	char *cutname;
	if((dev = dev_fromname(name, &cutname)))
	{
		if(dev->stat)
			return dev->stat(cutname, buffer);
	}
	return -1;
}

int remove(const char *name)
{
	Device *dev;
	char *cutname;
	if((dev = dev_fromname(name, &cutname)))
		if(dev->remove)
			return dev->remove(cutname);
	return -1;
}

int tell(int fd)
{
	return lseek(fd, 0, SEEK_CUR);
}
