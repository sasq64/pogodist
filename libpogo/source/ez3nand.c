/*
 * EZ3 NAND Filesystem (ez3nand.c)
 * Part of libpogo, a c-library replacent for GBA
 * Programmed by Jonas Minnberg (Sasq)
 *
 * DESCRIPTION
 **/

#define PACKED __attribute__ ((packed))
#include "core.h"
#include "device.h"
#include "gba_defs.h"

static volatile uint16 *IME = 0x04000208;

#define IRQ_ON() (*IME = 1)
#define IRQ_OFF() (*IME = 0)

#include <fat16.h>

enum {FAT_INIT, FAT_DEINIT};

#define fat_close_withouwrit _Z20fat_close_withouwriti
#define fat_init _Z8fat_initi
#define fat_deinit _Z10fat_deinitv
#define fat_search _Z10fat_searchmPKcP5_file
#define fat_getfirst _Z12fat_getfirstPKcPc
#define fat_getnext _Z11fat_getnextPc
#define fat_open _Z8fat_openPKc
#define fat_close _Z9fat_closei
#define fat_lseek _Z9fat_lseekili
#define fat_read  _Z8fat_readiPvj
#define fat_write _Z9fat_writeiPKcj
#define fat_remove _Z10fat_removePKc
#define fat_get_stat _Z12fat_get_statPKcP5_stat
#define fat_set_stat _Z12fat_set_statPKcP5_stat
#define fat_creat _Z9fat_creatPKch

static Device nanddev;

static void fixname(const char *name, char *dst)
{
	char *p = dst;
	
	if(*name != '\\' && *name != '/')
		*p++ = '\\';
	
	strcpy(p, name);
	while(*p)
	{
		if(*p == '/')
			*p = '\\';
		p++;
	}
}

static char dir_filename[16];
static char dir_basename[96];

int nand_open(const char *name, int flags)
{
	int root,rc;
	char tmp[256];
	struct _stat st;
	
	fixname(name, tmp);
	root = (strcmp(tmp, "\\") == 0);
	
	IRQ_OFF();

	if(flags && O_CREAT)
	{
		rc =  fat_creat(tmp, 0);
		IRQ_ON();
		return rc;
	}

	if(fat_get_stat(tmp, &st) != 0)
	{
		IRQ_ON();
		return -1;
	}

	if(root || (st.Attr & ATTR_DIRECTORY))
	{
		int rc = fat_getfirst(tmp, dir_filename);
		while(rc == 0 && ((strcmp(dir_filename, ".") == 0) ||
						  (strcmp(dir_filename, "..") == 0)) )
			rc = fat_getnext(dir_filename);

		if(rc != 0)
			*dir_filename = 0;
		else
		{	
			if(root)
				*dir_basename = 0;
			else
				strcpy(dir_basename, tmp);
		}
		
		IRQ_ON();
		return 99;
	}
	
	rc = fat_open(tmp);	
	IRQ_ON();

	
	fprintf(2, "%s:%d\n", tmp, rc);

	return rc;
}

#define BUF_SIZE 2048

static uchar *readbuf = NULL;
static uchar *bufptr = NULL;
static int buf_size = 0;

void fillbuf(int fd)
{
	if(!readbuf)
		readbuf = (uchar *)malloc(BUF_SIZE);
	
	bufptr = (uchar *)readbuf;

	IRQ_OFF();
	buf_size = fat_read(fd, readbuf, BUF_SIZE);
	IRQ_ON();
	
	
	
}


int nand_read(int fd, void *dest, int size)
{
	if(fd == 99)
	{
		if(strlen(dir_filename))
		{
			int rc = 1;

			char tmp[128];
			struct stat st;
			
			Romfile *rf = (Romfile *)dest;
			
			strcpy(rf->name, dir_filename);
			sprintf(tmp,"%s\\%s", dir_basename, dir_filename);
			
			IRQ_OFF();

			//while((rc != 0) && ((retry--) != 0))
			//	rc = fat_get_stat(tmp, &st);
			rc = nand_stat(tmp, &st);

			if(rc != 0)
			{
				fprintf(2, "stat %s = %d\n", dir_filename, rc);
				IRQ_ON();
				return -1;
			}
			
			rf->size = st.st_size;
			rf->start = 0;
			
			rc = fat_getnext(dir_filename);
			
			while(rc == 0 && ((strcmp(dir_filename, ".") == 0) ||
				  (strcmp(dir_filename, "..") == 0)) )
				rc = fat_getnext(dir_filename);

			if(rc != 0)
				*dir_filename = 0;
			
			IRQ_ON();

			return sizeof(Romfile);
		}
		return 0;
	}
	else
	{
		/*int left,rsize;
		uchar *dst = dest;
		int rc = 0;
				
		while(size > 0)
		{
			if(!buf_size)
				return rc;

			left = &readbuf[buf_size]-bufptr;
			if(left <= size)
			{
				memcpy(dst, bufptr, left);
				dst += left;
				size -= left;
				rc += left;
				fillbuf();
			}
			else
			{
				memcpy(dst,bufptr, size);
				bufptr += size;
				return size;
			}
		}		
		return rc;*/
		return fat_read(fd, dest, size);
	}
}

int nand_write(int fd, const void *dest, int size)
{
	return fat_write(fd, dest, size);
}


int nand_close(fd)
{
	if(fd != 99)
	{
		int rc;
		IRQ_OFF();
		rc = fat_close_withouwrit(fd);
		IRQ_ON();
		return rc;
	}
	return 0;
}

int nand_lseek(int fd, int offset, int origin)
{
	int rc;
	if(origin == SEEK_MEM)
		return 0;

	IRQ_OFF();
	rc = fat_lseek(fd, offset, origin);
	IRQ_ON();
	return rc;
	
}

int nand_ioctl(int fd, int request, va_list vlist)
{
	//va_arg(vl, int)
	switch(request)
	{
	case FAT_INIT:
		return fat_init(0);
		break;
	case FAT_DEINIT:
		fat_deinit();
		return 0;
		break;
	}
	return -1;
}


int nand_remove(const char *path)
{
	int rc;
	char tmp[128];
	struct _stat st;
	fixname(path, tmp);
	fat_remove(tmp);
}

int nand_stat(const char *path, struct stat *buffer)
{
	int rc;
	int retry = 3;
	char tmp[128];
	struct _stat st;
	fixname(path, tmp);
	
	IRQ_OFF();
	while((rc != 0) && ((retry--) != 0))
		rc = fat_get_stat(tmp, &st);
	IRQ_ON();
	
	//fprintf(2, "%s %d %d\n", tmp, st.Attr, rc);

	if(rc == 0)
	{
		buffer->st_size = st.FileSize;
		buffer->st_mode = 0;
		if(st.Attr & ATTR_DIRECTORY)
			buffer->st_mode |= S_IFDIR;
	}
	
	return rc;
}

static int init = 0;

void nand_init(void)
{
	if(!init)
	{
		*dir_filename = 0;
		
		IRQ_OFF();
		fat_init(0);
		IRQ_ON();
		
		memset(&nanddev, 0, sizeof(Device));
		nanddev.open = nand_open;
		nanddev.close = nand_close;
		nanddev.read = nand_read;
		nanddev.write = nand_write;
		nanddev.remove = nand_remove;
		nanddev.ioctl = nand_ioctl;
		nanddev.lseek = nand_lseek;
		nanddev.stat = nand_stat;
		//PrintStr("Init\n");
		device_register(&nanddev, "/flash", NULL, -1);
	}

	init = 1;
}
