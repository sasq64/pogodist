
#include "core.h"
#include "device.h"

static Device mbdev;

int dfopen (const char *file, const char *type);
int dfclose (int fp);
int dfgetc (int fp);
int dfclose (int fp);
int dfputc (int ch, int fp);
void drewind (int fp);

static int mb_open(const char *name, int flags)
{
	//fprintf(2, "opening %s\n", name);
	return dfopen(name, "rb");
	
}

static int mb_read(int fd, void *dest, int size)
{
	int l = size;
	uchar *d = dest;
	
	while(l--)
		*d++ = dfgetc(fd);
	
	return size;
}


static int mb_write(int fd, const void *dest, int size)
{
	return -1;
}

static int mb_lseek(int fd, int offset, int origin)
{
	switch(origin) {
	case SEEK_SET:
		drewind(fd);
		while(offset > 0)
		{
			dfgetc(fd);
			offset--;
		}
		break;
	case SEEK_CUR:
		while(offset > 0)
		{
			dfgetc(fd);
			offset--;
		}
		break;
	case SEEK_END:
		return -1;
		break;
	case SEEK_MEM:
		return 0;
		break;
	}
	
	return -1;
}

static int mb_close(int fd)
{
	return dfclose(fd);
}


void host_init(void)
{
	memset(&mbdev, 0, sizeof(mbdev));
	mbdev.open = mb_open;
	mbdev.close = mb_close;
	mbdev.read = mb_read;
	mbdev.lseek = mb_lseek;
	mbdev.write = mb_write;
	//mbdev.stat = mb_stat;
	device_register(&mbdev, "/host", NULL, -1);
}
