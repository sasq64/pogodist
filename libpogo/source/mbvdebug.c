/*
 * MBV2 Debug functions (mbvdebug.c)
 * Part of libpogo, a c-library replacent for GBA
 * Programmed by Jonas Minnberg (Sasq)
 *
 * DESCRIPTION
 * stderr device for text output to debug console
 *
 **/

#include "core.h"
#include "device.h"
#include "font.h"

int dputchar (int c);

void PrintMBV2(char *str)
{
	//dprint(str);
	while(*str)
		dputchar(*str++);
}

static Device debdev;

static int deb_open(const char *name, int mode)
{
	return 0;
}

static int deb_write(int fd, const void *dest, int size)
{
	char tmp[256];
	memcpy(tmp, dest, size);
	tmp[size] = 0;
	PrintMBV2(tmp);
	return size;
}

void mbvdeb_init(void)
{
	memset(&debdev, 0, sizeof(debdev));
	debdev.open = deb_open;
	debdev.write = deb_write;
	device_register(&debdev, "/dev/deb", NULL, 2);
}
