/*
 * Basic standard functions (core_misc.c)
 * Part of libpogo, a c-library replacent for GBA
 * Programmed by Jonas Minnberg (Sasq)
 *
 * DESCRIPTION
 * Standard functions needed by the core
 *
 **/

#include "core.h"

void *memset(void *dest, int v, int l)
{	uchar *d = dest;
	while(l--)
		*d++ = v;
	return dest;
}

void *memcpy8(void *dst, const void *src, int l)
{
	uchar *d, *s;
	d = (uchar *)dst;
	s = (uchar *)src;
	while(l--)
		*d++ = *s++;
	return dst;
}

void *memcpy(void *dst, const void *src, int l)
{
	uchar *d, *s;
	uint16 *d16, *s16;

	if( ((int)src & 1) || ((int)dst & 1) || (l & 1))
	{
		d = (uchar *)dst;
		s = (uchar *)src;
		while(l--)
			*d++ = *s++;
	} else {
		d16 = (uint16 *)dst;
		s16 = (uint16 *)src;
		l /= 2;
		while(l--)
			*d16++ = *s16++;
	}
	return dst;
}

void *memmove(void *dst, const void *src, int l)
{
	uchar *d = (uchar *)dst;
	uchar *s = (uchar *)src;

	if(d-s < l) {
		d+=l;
		s+=l;
		while(l--)
			*(--d)= *(--s);
	} else
		while(l--)
			*d++ = *s++;
	return dst;
}

int strcmp(const char *s1, const char *s2)
{
	while(*s1 && (*s1 == *s2))
	{
		s1++;
		s2++;
	}
	return (s1[0] - s2[0]);
}

int memcmp(const char *s1, const char *s2, int n)
{
	while(--n && (*s1 == *s2))
	{
		s1++;
		s2++;
	}

	return (s1[0] - s2[0]);
}

int strncmp(const char *s1, const char *s2, int n)
{
	while(*s1 && --n && (*s1 == *s2))
	{
		s1++;
		s2++;
	}

	return (s1[0] - s2[0]);
}

int strlen(const char *s)
{
	const char *t = s;
	while(*t++);
	return t-s-1;
}

char *strcpy(char *dst, const char *src)
{
	register char *d = dst;
	while(*src)
		*dst++ = *src++;
	*dst++ = 0;
	return d;
}

char *strncpy(char *dst, const char *src, int n)
{
	register char *d = dst;
	while(*src && n--)
		*dst++ = *src++;
	*dst++ = 0;
	return d;
}
