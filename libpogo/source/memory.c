/*
 * Core memory handling functions (memory.c)
 * Part of libpogo, a c-library replacent for GBA
 * Programmed by Jonas Minnberg (Sasq)
 *
 * DESCRIPTION
 * Functions for allocating and freeing memory in WRAM
 *
 **/

#include "core.h"

typedef struct _MemHead {
	struct _MemHead *next;
	uint32 size;
	uint32 data[0];
} MemHead;


typedef struct
{
	uint32 *base;
	MemHead *first;
	int size;
	MemHead block;
} MemArea;


static int current_area;

static MemArea mem_areas[4];


static uint32 *mem_base;
static MemHead *first_block;
static int mem_size;


/* Exported by linkscript as start of available memory (after any WRAM variables) */
extern uint32 __eheap_start;
extern uint32 __iheap_start;

void memory_init(void)
{
	MemArea *ma;
	ma = &mem_areas[0];
	ma->first = NULL;
	ma->base = &__eheap_start;
	ma->size = (0x02040000 - (int)ma->base) / 4;

	ma = &mem_areas[1];
	ma->first = NULL;
	ma->base = &__iheap_start;
	ma->size = (0x03007E00 - (int)ma->base) / 4;

	// EZ3 memory area
	ma = &mem_areas[2];
	ma->first = NULL;
	ma->base = (void *)(0x08400000);
	ma->size = 16*1024*1024 / 4;

	current_area = 0;
	mem_base = mem_areas[0].base;
	mem_size = mem_areas[0].size;
}

extern void dprint(const char *sz);
/*
void mem_check(void)
{
	MemHead *last_block = NULL;
	MemHead *block = first_block;

	while(block)
	{
		if((int)block > 0x03004000 || (int)block < 0x03000000)
		{
			char tmp[20];
			sprintf(tmp, "last %p block %p\n", last_block, block);
			dprint(tmp);
			while(1);
		}

		last_block = block;
		block = block->next;
	}
}
*/

void memory_setarea(int area)
{

	if(!mem_base)
		memory_init();


	mem_areas[current_area].base = mem_base;
	mem_areas[current_area].first = first_block;
	mem_areas[current_area].size = mem_size;
	
	mem_base = mem_areas[area].base;
	first_block = mem_areas[area].first;
	mem_size = mem_areas[area].size;

	current_area = area;
}

/* Alloc 32bit words */
uint32 *memory_alloc(int alloc_size)
{
	int free_after;
	MemHead *newblock;
	MemHead *block;

	if(!mem_base)
		memory_init();
	
	block = first_block;
	
	while(block)
	{
		if(block->next)
			free_after = (uint32 *)block->next - (uint32*)&block->data[block->size];
		else
			free_after = mem_size - (uint32)(&block->data[block->size] - mem_base);

		if(free_after >= (alloc_size + 3))
		{
			uint32 *area_start = (uint32*)&block->data[block->size];
			if(start)
			{
				if(start >= area_start+2)
				{
					newblock = (MemHead *)start[-2];
					newblock->next = block->next;
					newblock->size = alloc_size;
					block->next = newblock;
					return newblock->data;
				}
				
				continue;
			}
			newblock = (MemHead *)&block->data[block->size];
			newblock->next = block->next;
			newblock->size = alloc_size;
			block->next = newblock;
			return newblock->data;
		}

		block = block->next;
	}

	//dprint("Out of memory!\n");
	return NULL;
/*
	block->next = (MemHead *)&block->data[block->size];
	newblock = block->next;
	newblock->next = NULL;
	newblock->size = alloc_size;
	return newblock->data;
*/
}

void memory_free(uint32 *mem)
{
	MemHead *last_block = NULL;
	MemHead *block = first_block;
	int i = 0;//current_area;

/*	mem_areas[current_area].first = first_block;
	
	// Find area
	for(i=0; (i < 3) && ((mem < mem_areas[i].base) || (mem >= (mem_areas[i].base+mem_areas[i].size))); i++);

	if(i <3)
		block = mem_areas[i].first;
	else
	{
		dprint("NO FREE\n");
		while(1);
		return;
	}

	fprintf(2, "%p\n", block);
*/	
	while(block)
	{
		if(mem == block->data)
		{
			if(!last_block)
			{
				//first_block = NULL;
				dprint("CANT FREE FIRST BLOCK!");
				//while(1);
				block->size = 4;
			}
			else
				last_block->next = block->next;
			return;
		}
		last_block = block;
		//fprintf(2, "%p -> %p\n", block, block->next);
		block = block->next;
	}
	
	fprintf(2, "No free %x in area %d (%x)\n", mem, i, mem_areas[i].first);
	//while(1);
	return;

}

uint32 *memory_realloc(uint32 *mem, int alloc_size)
{
	int free_after;
	void *newmem;
	MemHead *block = first_block;

	while(block) {
		if(mem == block->data)
		{
			/* If we decrease size or if theres enough free space after, just change the size and
			   return the same pointer */
			if(block->next)
				free_after = (uint32 *)block->next - (uint32*)&block->data[block->size];
			else
				free_after = mem_size - (uint32)(&block->data[block->size] - mem_base);
			if((alloc_size < block->size) ||
			   (alloc_size - block->size < free_after))
			{
				block->size = alloc_size;
				return mem;
			}

			/* Try allocating a new block */
			newmem = memory_alloc(alloc_size);
			if(newmem)
			{
				memcpy(newmem, mem, block->size);
				memory_free(mem);
				return newmem;
			}
			//dprint("Out of memory!\n");
			/* No memory for block, fail */
			return NULL;

		}
		block = block->next;
	}
	return NULL;
}

/* Return available memory (also calculates largest block) */
int memory_avail(void)
{
	int free_after;
	int free_total = 0;
	int largest_block = 0;
	MemHead *block = first_block;

	if(!block)
		return mem_size;

	while(block)
	{
		if(block->next)
			free_after = (uint32 *)block->next - (uint32*)&block->data[block->size];
		else
			free_after = mem_size - (uint32)(&block->data[block->size] - mem_base);

		if(free_after > largest_block)
			largest_block = free_after;
		free_total += free_after;
		block = block->next;
	}
	return free_total;
}

#ifndef MALLOC_DEBUG

/* C-library versions of allocation functions */

void *malloc(int x)
{
	void *p = (void *)memory_alloc((x+3)/4);
	return p;
};

void *realloc(void *p, int l)
{
	return memory_realloc((uint32*)p, (l+3)/4);
}

void free(void *x)
{ 
	memory_free((uint32*)x); 
}

#endif
