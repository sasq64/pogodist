#include "core.h"
#include "device.h"
#include "gba_defs.h"
#include "rtc.h"
#include "cartlib.h"

extern void executeCart(uint32 a, uint32 b, uint32 c) IN_IWRAM;
extern void setRamStart(uint32 a) IN_IWRAM;

int cartSupported;

static int current_bank = -1;

void set_ram_start(int i)
{
	if(current_bank != i)
	{
		if(i >= 0)
#ifdef CARTLIB
			fcSetSRamStart(i * 0x10000);
#else
			__FarProcedure(setRamStart, i);
#endif
		current_bank = i;
	}
}

void Reset(void)
{
	SETW(REG_IME, 1);
	SETW(REG_IE, 0);
	SETW(REG_IF, 0);
#ifdef CARTLIB
	fcExecuteRom(0, 0x8C);
#else
	__FarProcedure(executeCart, 0x8C, 0, 0);
#endif
}

//#ifdef CARTLIB
//	cartSupported = fcGetSupport();
//#endif
//#ifdef CARTLIB
//	fcExecuteRom(0, 0);
//#else

/* Rom execute routines - not sure if the should be in device.c */

extern void executeCart(uint32 a, uint32 b, uint32 c) IN_IWRAM;

static int jump_adress = 0;

void make_arguments(const char *cmdname, const char *const *argv)
{
	int fd;
	uint32 *p;
	uchar *ptr;
	int i = 0;
//	int l = 0;
	
/*	fd = open(cmdname, 0);
	if(fd >= 0)
	{
		l = lseek(fd, 0, SEEK_MEM);
		fprintf(stderr, "Found %s at %p\n", cmdname, l);
		close(fd);
	}
*/
	p = (uint32 *)(0x02000000+255*1024);
	p[0] = 0xFAB0BABE;	
	ptr = (uchar *)&p[2];
	strcpy(ptr, cmdname);
	ptr += (strlen(ptr)+1);
	while(argv && argv[i])
	{
		const char *s;
		if(strncmp(argv[i], "/rom", 4) == 0)
			s = &argv[i][4];
		else
			s = argv[i];

		strcpy(ptr, s);
		ptr += (strlen(ptr)+1);
		i++;
	}
	p[1] = i+1;
	
	//p = (uint32 *)(0x0203FC00-4);
	if(argv && argv[0] && (fd = open(argv[0], 0)))
	{
		int m = lseek(fd, 0, SEEK_MEM);
		int s = lseek(fd, 0, SEEK_END);
		//m -= (l - 0x08000000);
		//fprintf(2, "Found %s at %x\n", argv[0] , m);
		p[-1] = m;
		p[-2] = s;
		close(fd);
	} else
		p[-1] = 0;

}


void execv(const char *cmdname, const char *const *argv)
{
	uint32 *p;
	uint32 l;
	int i = 0;

	int fd = open(cmdname, 0);
	if(fd >= 0)
	{
		l = lseek(fd, 0, SEEK_MEM);
		close(fd);
		if((l & 0x00007FFF) == 0)
		{

			make_arguments(cmdname, argv);

			// Remap for start position of rom
			p = (uint32 *)(0x02000000+255*1024);
			//if(p[-1] >= 0x08000000)
				p[-1] -= (l - 0x08000000);

			if(!argv[0])
			{
				p = (uint32 *)(0x02000000);
				for(i=0; i<256*256; i++)
					p[i] = 0;
			}

			//reset_io();

			SETW(REG_IE, 0);
			SETW(REG_IF, 0);
			SETW(REG_IME, 0);


			//SETW(REG_DISPCNT, DISP_MODE_0 | DISP_BG1_ON );
			//SETW(REG_BG1CNT, 0);

			//p = (uint32 *)0x03007FC0;
			//for(i=0; i<4*4; i++)
			//	p[i] = 0;

			SETW(REG_SOUNDBIAS, 0x0200);

#ifdef CARTLIB
			fcExecuteRom(l, jump_adress);
#else
			__FarProcedure(executeCart, jump_adress, l);
#endif

			while(1);
		}
	}
}

void execv_jump(const char *cmdname, const char *const *argv, void *jump)
{
	jump_adress = (int)jump;
	execv(cmdname, argv);
	jump_adress = 0;
}

void execv_mb(const char *cmdname, const char *const *argv)
{
	int fd = open(cmdname, 0);
	if(fd >= 0)
	{
		read(fd, (uchar *)0x02000000, 1024*256);

		SETW(REG_IE, 0);
		SETW(REG_IF, 0);
		SETW(REG_IME, 1);
		SETW(REG_DISPCNT, DISP_MODE_0 | DISP_BG1_ON );
		SETW(REG_BG1CNT, 0);
		((void(*)(void))0x02000000)();
	}

}
