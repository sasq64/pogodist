/*
 * Core memory handling functions (memory.c)
 * Part of libpogo, a c-library replacent for GBA
 * Programmed by Jonas Minnberg (Sasq)
 *
 * DESCRIPTION
 * Functions for allocating and freeing memory in WRAM
 *
 * New System:
 * "MemInfo" structures keep track of allocated areas and
 * are stored as a reverse-growing vector at the end of
 * memory in no particular order (allocation just gets the
 * next availble info-structure.
 * 
 * 
 **/

#undef MALLOC_DEBUG
#include "core.h"

void *mdbgptr;


//typedef unsigned int uint32;
//#include <stdio.h>
//#include <stdlib.h>

#ifndef MEM_STRUCTS

typedef struct _MemInfo
{
	uint32 size;
	uint32 *data;
} MemInfo;


typedef struct
{
	uint32 *base;
	int size;
	MemInfo *bottom;
	MemInfo *top;

} MemArea;

#endif

void init_area(MemArea *area, uint32 *memory, int size)
{
	area->base = memory;
	area->size = size;
	area->bottom = area->top = (MemInfo *)&area->base[size-2];
	
	area->top->data = (uint32*)(area->top-1);
	area->top->size = 0;
	area->top--;
}

// Bottom points to FIRST memeinfo
// Top points to next unsused meminfo

/* Alloc 32bit words */
uint32 *memory_alloc(MemArea *area, uint32 *start, int alloc_size)
{
	int free_before;
	MemInfo saved_info;
	uint32 *ptr;

	uint32 *last_end = area->base;
	MemInfo *info = area->bottom;

	while(info > area->top)
	{
		if(start)
			last_end = start;

		free_before = info->data - last_end;
		
		//if(start)
		//	fprintf(2, "Free before %p : %d\n", info->data, free_before);

		if(free_before >= alloc_size)
		{
			
			saved_info = *info;
			info->data = last_end;
			info->size = alloc_size;
			ptr = last_end;
			
			while(info > area->top)
			{
				MemInfo tmp_info = saved_info;
				info--;
				saved_info = *info;
				*info = tmp_info;
			}
			area->top--;			
			area->top[1].data -= 2;
			
			//fprintf(2, "alloc %d => %p\n", alloc_size, ptr);
			return ptr;

		}

		last_end = &info->data[info->size];

		info--;
	}

	return NULL;
}
/*
void print_stack(MemArea *area)
{
	char tmp[32];
	MemInfo *info = area->bottom;
	sprintf(tmp, "---------------------\n");
	dprint(tmp);
	while(info > area->top)
	{
		sprintf(tmp, "%p %d\n", info->data, info->size);
		dprint(tmp);
		info--;		
	}
	sprintf(tmp, "---------------------\n");
	dprint(tmp);
}
*/

uint32 *memory_free(MemArea *area, uint32 *start)
{
	MemInfo *info = area->bottom;
	while(info > area->top)
	{
		if(info->data == start)
		{
			while(info > area->top)
			{
				info--;
				info[1] = info[0];
			}
			area->top++;
			area->top[1].data += 2;
			return start;
		}
		info--;		
	}
	return NULL;
}

uint32 *memory_realloc(MemArea *area, uint32 *start, int size)
{
	MemInfo *info = area->bottom;	

	while((info-1) > area->top)
	{
		if(info->data == start)
		{
			uint32 *ptr;			
			int free_after = info[-1].data - &info->data[info->size];

			// If new size is smaller just shrink and return
			if((size <= info->size) || (size <= (info->size + free_after)))
			{
				info->size = size;
				return info->data;
			}			
			
			// Allocate new area and return
			ptr = memory_alloc(area, NULL, size);
			memcpy(ptr, info->data, info->size * 4);
			memory_free(area, info->data);
			return ptr;
		}
		
		info--;		
	}
	return NULL;
}
int memory_avail(MemArea *area)
{

	int total = 0;
	uint32 *last_end = area->base;
	MemInfo *info = area->bottom;

	while(info > area->top)
	{
		total += (info->data - last_end);
		info--;
	}
	
	return total;

}


///////////// PART TWO //////////////



static int current_area = -1;
static MemArea mem_areas[4];

/* Exported by linkscript as start of available memory (after any WRAM variables) */
extern uint32 __eheap_start;
extern uint32 __iheap_start;

void memory_init(void)
{
	init_area(&mem_areas[0], &__eheap_start, (0x02040000 - (int)&__eheap_start) / 4);
	init_area(&mem_areas[1], &__iheap_start, (0x03007E00 - (int)&__iheap_start) / 4);
	init_area(&mem_areas[2], (uint32*)0x08400000, 4*1024*1024);
	init_area(&mem_areas[3], (uint32*)0x06000000, 96*256);

	current_area = 0;
}

void memory_setarea(int area)
{
	if(current_area == -1)
		memory_init();
	current_area = area;
}


void *_malloc(int x)
{
	void *p = (void *)memory_alloc(&mem_areas[current_area], NULL, (x+3)/4);
	return p;
};

void *_malloc_fixed(void *ptr, int x)
{
	void *p = (void *)memory_alloc(&mem_areas[current_area], ptr, (x+3)/4);
	return p;
};

void *_realloc(void *p, int l)
{
	return (void *)memory_realloc(&mem_areas[current_area], (uint32*)p, (l+3)/4);
}

void _free(void *x)
{ 
	memory_free(&mem_areas[current_area], (uint32*)x); 
}

void *malloc(int x)
{
	void *p = (void *)memory_alloc(&mem_areas[current_area], NULL, (x+3)/4);
	return p;
};

void *malloc_fixed(void *ptr, int x)
{
	void *p = (void *)memory_alloc(&mem_areas[current_area], ptr, (x+3)/4);
	return p;
};

void *realloc(void *p, int l)
{
	return (void *)memory_realloc(&mem_areas[current_area], (uint32*)p, (l+3)/4);
}

void free(void *x)
{ 
	memory_free(&mem_areas[current_area], (uint32*)x); 
}


int avail(void)
{
	return memory_avail(&mem_areas[current_area]);
}

void dump_mem(void)
{
	//print_stack(&mem_areas[current_area]);

}

/*
int main(int argc, char **argv)
{
	uint32 *tmp = malloc(128*1204);	
	MemArea area;
	
	init_area(&area, tmp, 32*1024);
	
	{
		char *ptr = (char *)memory_alloc(&area, NULL, 12);
		char *ptr2 = (char *)memory_alloc(&area, NULL, 280);
		
		print_stack(&area);
		
		memory_alloc(&area, NULL, 80);
		memory_alloc(&area, NULL, 13);		
		print_stack(&area);
		printf("Freeing\n");
		memory_free(&area, ptr2);
		
		memory_alloc(&area, tmp+80, 70);
		
		print_stack(&area);
		printf("Alloc\n");
		ptr2 = (char *)memory_alloc(&area, NULL, 6);
		(char *)memory_alloc(&area, NULL, 7);
		print_stack(&area);
		(char *)memory_alloc(&area, NULL, 17);
		memory_free(&area, ptr2);
		print_stack(&area);
	}
	
	return 0;
}
*/
