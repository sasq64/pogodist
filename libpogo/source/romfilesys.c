/*
 * ROM Filesystem Device (romfilesys.c)
 * Part of libpogo, a c-library replacent for GBA
 * Programmed by Jonas Minnberg (Sasq)
 *
 * DESCRIPTION
 * A rom filesystem that supports recursive directories. The filesystem 
 * is placed in the cart after the actual program/game rom by a PC
 * program called MAKEFS
 **/

#include "core.h"
#include "device.h"

char *rootdir;

Romfile rootfile;

typedef struct {
	int pos;
	Romfile *file;
} OpenFile;

static int open_files = 0;
static OpenFile open_filetab[32];


static Device rfdev;

static char *strchr2(const char *str, int c)
{
	while(*str && *str != c) str++;
	if(*str)
		return (char *)str;
	else
		return NULL;
}

static Romfile *findfile(Romfile *rf, char *name, int size)
{
	size &= 0x7FFFFFFF;
	while(size)
	{
		if(strcmp(rf->name, name) == 0)
			return rf;
		rf++;
		size -= sizeof(Romfile);
	}
	return NULL;
}



static int rf_open(const char *name, int flags)
{
	char tmp[128];
	int size;
	char *p, *p2;
	OpenFile *f;
	Romfile *rf;

	while(*name == '/')
		name++;
	strcpy(tmp, name);

	if(!strlen(name))
	{
		f = &open_filetab[open_files++];
		f->file = &rootfile;
		f->pos = 0;
		return open_files-1;
	}

	p = NULL;
	p = strchr2(tmp, '/');
	if(p)
		*p = 0;

	rf = findfile((Romfile *)rootdir, tmp, rootfile.size);
	if(!rf)
		return -1;
	while(p)
	{
		p++;
		p2 = strchr2(p, '/');
		if(p2)
			*p2 = 0;

		size = rf->size;
		rf = (Romfile *)&rootdir[rf->start];
		rf = findfile(rf, p, size);
		if(!rf)
			return -1;
		p = p2;
	}

	f = &open_filetab[open_files++];
	f->file = rf;
	f->pos = 0;
	return open_files-1;
}


void high_copy(char *dest, char *p, int size)
{

}

static int rf_read(int fd, void *dest, int size)
{
	Romfile *rf;
	OpenFile *f;
	int rsize;

	f = &open_filetab[fd];
	rf = f->file;
	rsize = rf->size & 0x7fffffff;

	/* Get ptr to current pos in ROM */
	uchar *p = &((uchar *)rootdir)[rf->start + f->pos];
	if(f->pos + size > rsize)
		size = (rsize - f->pos);

	if(size) 
	{
		memcpy(dest, p, size);
		f->pos += size;
	}
	return size;
}

static int rf_close(int fd)
{
	OpenFile *f;

	f = &open_filetab[fd];
	f->pos = -1;

	f = &open_filetab[open_files-1];
	while(open_files && f->pos == -1) {
		open_files--;
		f = &open_filetab[open_files-1];
	}
	return 0;
}

static int rf_stat(const char *name, struct stat *stat)
{
	Romfile *rf;
	int fd = rf_open(name, 0);
	memset(stat, 0, sizeof(struct stat));
	if(fd >= 0)
	{
		rf = open_filetab[fd].file;

		if(rf->size & 0x80000000)
		{
			stat->st_mode |= S_IFDIR;
		}
		stat->st_size = rf->size & 0x7FFFFFFF;
		stat->st_ino = (int)&rootdir[rf->start];
		open_files--;
		return 0;
	}
	return -1;
}

static int rf_lseek(int fd, int offset, int origin)
{
	int rsize, newpos = 0;
	Romfile *r;
	OpenFile *f;

	f = &open_filetab[fd];
	r = f->file;
	rsize = r->size & 0x7fffffff;
	
	switch(origin) {
	case SEEK_SET:
		newpos = offset;
		break;
	case SEEK_CUR:
		newpos = f->pos + offset;
		break;
	case SEEK_END:
		newpos = rsize - offset;
		break;
	case SEEK_MEM:
		return (int)&((uchar *)rootdir)[r->start + f->pos];
		break;
	}

	if(newpos < 0)
		return -1;
	if(newpos > rsize) {
		newpos = rsize;
		//dprint("Seeking beyond end of file\n");
	}

	return f->pos = newpos;
}

extern void dprint(const char *sz);

static volatile uint16 *xrom = (uint16*)0x09000000;


void filesys_init(void)
{
	uint32 *ptr = (uint32 *)0x08000000;
	rootdir = NULL;
	
	*xrom = 0x8000;
	*xrom = 0;
	
	//dprint("Looking for filesys\n");
	/* Look through entire cart in 32KB increments for the magic word */
	while(ptr < (uint32*)0x0A000000)
	{
		ptr += 4*1024;
		if(*ptr == 0xFAB0BABE)
		{
			//ptr += (0xC0/4);
			strcpy(rootfile.name, "/");
			rootfile.size = 0x80000000 | ptr[1];
			rootfile.start = 0;
			rootdir = (char *)&ptr[2];
			//fprintf(2, "Romfilesys found at %p\n", ptr);
			break;
		}
	}

	memset(&rfdev, 0, sizeof(rfdev));
	rfdev.open = rf_open;
	rfdev.close = rf_close;
	rfdev.read = rf_read;
	rfdev.lseek = rf_lseek;
	rfdev.stat = rf_stat;
	device_register(&rfdev, "/rom", NULL, -1);
}

/* Skip to the next filesystem in the cart if several are present */
void filesys_next(void)
{
	uint32 *ptr = (uint32 *)((int)rootdir & 0xFFFF8000);
	ptr += 4*1024;

	while(ptr < (uint32*)0x0A000000)
	{
		ptr += 4*1024;
		if(*ptr == 0xFAB0BABE)
		{
			//ptr += (0xC0/4);
			strcpy(rootfile.name, "/");
			rootfile.size = 0x80000000 | ptr[1];
			rootfile.start = 0;
			rootdir = (char *)&ptr[2];
			dprint("Romfilesys found\n");
			break;
		}
	}
}
