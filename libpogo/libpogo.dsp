# Microsoft Developer Studio Project File - Name="libpogo" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=libpogo - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libpogo.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libpogo.mak" CFG="libpogo - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libpogo - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "libpogo - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "libpogo - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f libpogo.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "libpogo.exe"
# PROP BASE Bsc_Name "libpogo.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "make"
# PROP Rebuild_Opt "rebuild"
# PROP Bsc_Name "libpogo.bsc"
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "libpogo - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f libpogo.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "libpogo.exe"
# PROP BASE Bsc_Name "libpogo.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "make"
# PROP Rebuild_Opt "rebuild"
# PROP Bsc_Name "libpogo.bsc"
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "libpogo - Win32 Release"
# Name "libpogo - Win32 Debug"

!IF  "$(CFG)" == "libpogo - Win32 Release"

!ELSEIF  "$(CFG)" == "libpogo - Win32 Debug"

!ENDIF 

# Begin Group "Source"

# PROP Default_Filter "C"
# Begin Source File

SOURCE=.\source\console.c
# End Source File
# Begin Source File

SOURCE=.\source\core.c
# End Source File
# Begin Source File

SOURCE=.\source\core_misc.c
# End Source File
# Begin Source File

SOURCE=.\source\cursor.c
# End Source File
# Begin Source File

SOURCE=.\source\debug.c
# End Source File
# Begin Source File

SOURCE=.\source\device.c
# End Source File
# Begin Source File

SOURCE=.\source\editbuf.c
# End Source File
# Begin Source File

SOURCE=.\source\filesys.c
# End Source File
# Begin Source File

SOURCE=.\source\font.c
# End Source File
# Begin Source File

SOURCE=.\source\io.c
# End Source File
# Begin Source File

SOURCE=.\source\keyboard.c
# End Source File
# Begin Source File

SOURCE=.\source\memory.c
# End Source File
# Begin Source File

SOURCE=.\source\misc.c
# End Source File
# Begin Source File

SOURCE=.\source\romfilesys.c
# End Source File
# Begin Source File

SOURCE=.\source\rtc.c
# End Source File
# Begin Source File

SOURCE=.\source\screen.c
# End Source File
# Begin Source File

SOURCE=.\source\smartkey.c
# End Source File
# Begin Source File

SOURCE=.\source\sram_access.c
# End Source File
# Begin Source File

SOURCE=.\source\sramfile.c
# End Source File
# Begin Source File

SOURCE=.\source\string.c
# End Source File
# Begin Source File

SOURCE=.\source\vkeyboard.c
# End Source File
# End Group
# Begin Group "Include"

# PROP Default_Filter "H"
# Begin Source File

SOURCE=.\include\cartlib.h
# End Source File
# Begin Source File

SOURCE=.\include\console.h
# End Source File
# Begin Source File

SOURCE=.\include\core.h
# End Source File
# Begin Source File

SOURCE=.\source\cursor.h
# End Source File
# Begin Source File

SOURCE=.\include\device.h
# End Source File
# Begin Source File

SOURCE=.\source\editbuf.h
# End Source File
# Begin Source File

SOURCE=.\include\font.h
# End Source File
# Begin Source File

SOURCE=.\source\gba_defs.h
# End Source File
# Begin Source File

SOURCE=.\include\io.h
# End Source File
# Begin Source File

SOURCE=.\include\pogo.h
# End Source File
# Begin Source File

SOURCE=.\include\rtc.h
# End Source File
# Begin Source File

SOURCE=.\include\screen.h
# End Source File
# Begin Source File

SOURCE=.\include\signal.h
# End Source File
# Begin Source File

SOURCE=.\include\smartkey.h
# End Source File
# Begin Source File

SOURCE=.\source\sram_access.h
# End Source File
# Begin Source File

SOURCE=.\include\stdio.h
# End Source File
# Begin Source File

SOURCE=.\include\stdlib.h
# End Source File
# Begin Source File

SOURCE=.\include\string.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\source\dual.s
# End Source File
# Begin Source File

SOURCE=.\Makefile
# End Source File
# Begin Source File

SOURCE=.\source\visoly.s
# End Source File
# End Target
# End Project
