#ifndef CORE_H
#define CORE_H

typedef unsigned int uint32;
typedef int int32;
typedef unsigned short uint16;
typedef short int16;
typedef unsigned char uchar;

#ifndef NULL
#define NULL 0
#endif

#ifndef EOF
#define EOF (-1)
#endif

// Map stdargs to gcclibs implementation - should be in its own file
typedef __builtin_va_list va_list; 
#define va_start(v,l) __builtin_stdarg_start((v),l)
#define va_end __builtin_va_end
#define va_arg __builtin_va_arg


void *memset(void *dest, int v, int l);
void *memmove(void *dst, const void *src, int l);
void *memmove8(void *dst, const void *src, int l);
void *memcpy(void *dst, const void *src, int l);
void *memcpy8(void *dst, const void *src, int l);
int strcmp(const char *s1, const char *s2);
int strncmp(const char *s1, const char *s2, int n);
int strlen(const char *s);
char *strcpy(char *dst, const char *src);
char *strncpy(char *dst, const char *src, int n);
int memcmp(const char *s1, const char *s2, int n);

#define MEM_STRUCTS

typedef struct _MemInfo
{
	uint32 size;
	uint32 *data;
} MemInfo;


typedef struct
{
	uint32 *base;
	int size;
	MemInfo *bottom;
	MemInfo *top;

} MemArea;


void memory_init();
uint32 *memory_alloc(MemArea *area, uint32 *mem, int size);
uint32 *memory_realloc(MemArea *area, uint32 *mem, int size);
uint32 *memory_free(MemArea *area, uint32 *mem);
int memory_avail(MemArea *area);

void Halt(void);
void set_ram_start(int i);

typedef uint32 time_t;
#define CLOCKS_PER_SEC 50
int clock(void);

struct tm
{
	uint16 tm_sec;     /* seconds */
	uint16 tm_min;     /* minutes */
	uint16 tm_hour;    /* hours */
	uint16 tm_mday;    /* day of the month */
	uint16 tm_mon;     /* month */
	uint16 tm_year;    /* year */
	uint16 tm_wday;    /* day of the week */
	//int     tm_yday;        /* day in the year */
	//int     tm_isdst;       /* daylight saving time */
};

time_t time(time_t *);
struct tm *time2(struct tm *dst);
struct tm *localtime(time_t t);

#define gmtime localtime



#define memmove8 memmove

#define CLOCKS_PER_SEC 50


#ifdef MALLOC_DEBUG

void *_malloc(int x);
void *_malloc_fixed(void *ptr, int x);
void *_realloc(void *p, int l);
void _free(void *x);

extern void *mdbgptr;

#define malloc(x) (_dprintf("%s:%d %d bytes => %p\n", __FILE__, __LINE__, x, mdbgptr = _malloc(x)), mdbgptr )
#define realloc(p, l)  (_dprintf("%s:%d (%p)%d bytes => %p\n", __FILE__, __LINE__, p, l, mdbgptr = _realloc(p,l) ), mdbgptr )
#define free(p) _free(p)

#else

void *malloc(int x);
void *malloc_fixed(void *ptr, int x);
void *realloc(void *p, int l);
void free(void *x);

#endif

// Should be renamed
void memory_setarea(int area);
int avail(void);





#endif
