#ifndef POGO_H
#define POGO_H

#ifdef __cplusplus
extern "C" {
#endif

#include "core.h"
#include "device.h"
#include "font.h"
#include "screen.h"
#include "io.h"
#include "string.h"

#ifdef __cplusplus
}
#endif

#ifdef DEBUG
#define DPRINTF _dprintf
#else
#define DPRINTF (void)
#endif

#endif
