#include <pogo.h>

#include "gba_defs.h"

#include "bitmap.h"
#include "filesys.h"
#include "filetype.h"
#include "savesystem.h"
#include "modplayer.h"
#include "window.h"
#include "widgets/listview.h"
#include "widgets/textbar.h"
#include "settings.h"
#include "misc.h"

int fcExecuteRom(unsigned int offset, unsigned int jump);

void bmp_view(char *fname);

static BitMap **icon_set;

static int ftcount = 1;
static FileType *filetypes[32];

extern ListView *MainList;
//extern TextBar *StatusBar;
void statusbar_set(char *);
extern Screen *MainScreen;

extern void LZ77UnCompWram(void *Srcp, void *Destp);
extern void MBExecute(void *Srcp);
extern void SoftReset(unsigned int flags);

extern void __FarProcedure2 (void (*ptr)(), ...);  // Reference to routine in crt0.S


void save_state(void);

int ends_with(char *s, char c)
{
	char *t = &s[strlen(s)-1];
	return ((*t == tolower(c)) || (*t == toupper(c)));
}


static int check_dir(char *data, DirList *entry)
{
	return entry->type == 1;
}

static int check_dev(char *data, DirList *entry)
{
	//fprintf(stderr, "Testing %s %s\n", data, entry->name);
	return entry->type == 2;
}

static int check_extention(char *data, DirList *entry)
{
	char *p = strrchr(entry->name, '.');
	if(p)
	{
		p++;
		return (stricmp(p, data) == 0);
	}

	return 0;
}

int execute_mb(char *cmd, char *fname, int keys)
{
	int fd;
	uchar *ptr;

	mod_stop();
	save_state();
		
	fd = open(fname, 0);
	ptr = mmap(NULL, 0, 0, ends_with(fname, 'z') ? MAP_COMPRESSED : 0, fd, 0);
	close(fd);
	
	if(!ptr)
		return -1;

	//fprintf(2, "%p\n", ptr);
	
	set_ram_start(0);

	SETW(REG_IE, 0);
	SETW(REG_IF, 0);
	SETW(REG_IME, 0);
	//SETW(REG_DISPCNT, DISP_MODE_0 | DISP_BG1_ON );
	//SETW(REG_BG1CNT, 0);
	
	__FarProcedure2(MBExecute, ptr);

	return 1;

}

#ifdef EZ3

struct Screen;
extern Screen *MainScreen;
void statusbar_set(char *text);


void exec_l77(char *cmd, char *fname, int keys)
{
	char text[32];
	int fd;

	//savesys_handleexec(fname, NULL);
	//set_ram_start(0);

	fd = open(fname, 0);
	if(fd >= 0)
	{
		char head[4];
		uint32 index[130];
		uchar *dst;
		volatile uint32 *src;
		uint32 *ptr;
		uint32 comp;
		int i,len,l,rc;

		int fsize = lseek(fd, 0, SEEK_END);

		lseek(fd, 0, SEEK_SET);
		
		ptr = (uint32 *)(0x09400000-((fsize+3)&0xFFFFFFFC) - 0x60000);

		{
			uchar *d = (uchar *)ptr;
			
			read(fd, head, 4);
			read(fd, &len, 4);
			read(fd, index, 4*len);
			lseek(fd, 8, SEEK_CUR);

			for(i=0; i<len; i++)
			{
				l = index[i];
				
				if((d <= 0x09000000) && (d+l > 0x09000000))
					d = 0x09040000;
				
				sprintf(text, "Loading %02d%%", i*100/len);
				statusbar_set(text);
				screen_redraw(MainScreen);

				rc = read(fd, d, l);
				//fprintf(stderr, "%p:%d\n", d,l);

				index[i] = (uint32)d;
				d += l;
			}
			index[i] = (uint32)d;
		}
		close(fd);

		//execute_mb("", "/rom/Tools/gbamon.mb", 0);

		dst = (uchar *)0x08400000;

		SETW(REG_IE, 0);
		SETW(REG_IF, 0);
		SETW(REG_IME, 0);
		SETW(REG_SOUNDBIAS, 0x0200);

		//PrintStr("Unpacking");

		for(i=0; i<len; i++)
		{
			src = (uint32 *)(index[i]);
			l = index[i+1] - index[i];

			//fprintf(stderr, ".\n");
			sprintf(text, "Unpacking %02d%%", i*100/len);
			statusbar_set(text);
			screen_redraw(MainScreen);

			comp = *src;

			if((l != 0x20000) && ((comp&0xFF) == 0x10))
			{
				LZ77UnCompWram(src, 0x02020000);
				memcpy16(dst, 0x02020000, 0x10000);
			}
			else
				memcpy16(dst, src, 0x10000);
			dst += 0x020000;
		}
		
		//fcExecuteRom(0x0A000000, 0);

		/*if(keys == 2)
			execv_jump(fname, args, (void *)0x0000008C);
		else
		if(keys == 1 || settings_get(SF_INTROSKIP))
			execv_jump(fname, args, (void *)0x08000000);
		else
			execv(fname, args);*/
	}
}

#endif

int execute_rom(char *cmd, char *fname, int keys)
{
	int fd;
	uchar *ptr = NULL;
	char *s;
	//const char *args[] = {NULL, NULL};

	mod_stop();
	save_state();

	savesys_handleexec(fname, "XXX");
	set_ram_start(0);
	
	fd = open(fname, 0);
	
	if(fd < 0)
		return -2;

	s = strrchr(fname, '.');

#ifdef EZ3
	if(s && (strnicmp(s, ".LZ7", 4) == 0))
	{
		fprintf(stderr, "L77\n");
		exec_l77(cmd, fname, keys);
		ptr = 0x08400000;
	}
	else
#endif
	{
		ptr = mmap(NULL, 0, 0, MAP_ALIGN32, fd, 0);
	}

	if(!ptr)
		return -1;

	fprintf(stderr, "Running %p\n", ptr);
	fcExecuteRom(((int)ptr) & 0xFFFFFF, 0);
	
	//reset_io();
	/*if(keys == 2)
		execv_jump(fname, args, (void *)0x0000008C);
	else
	if(keys == 1 || settings_get(SF_INTROSKIP))
		execv_jump(fname, args, (void *)0x08000000);
	else
		execv(fname, args);*/
	return 1;

}

void make_arguments(const char *cmdname, const char *const *argv);


void make_arguments2(const char *cmdname, const char *const *argv)
{
	int fd;
	uint32 *p;
	uchar *ptr;
	int i = 0;

	p = (uint32 *)(0x02000000+255*1024);
	p[0] = 0xFAB0BABE;	
	ptr = (uchar *)&p[2];
	strcpy(ptr, cmdname);
	ptr += (strlen(ptr)+1);
	while(argv && argv[i])
	{
		const char *s;
		if(strncmp(argv[i], "/rom", 4) == 0)
			s = &argv[i][4];
		else
		if(strncmp(argv[i], "/flash", 6) == 0)
			s = &argv[i][6];
		else
			s = argv[i];

		strcpy(ptr, s);
		ptr += (strlen(ptr)+1);
		i++;
	}
	p[1] = i+1;
	
	//p = (uint32 *)(0x0203FC00-4);
	if(argv && argv[0] && (fd = open(argv[0], 0)))
	{
		int cc = 0;
		uchar *m;
		int s = lseek(fd, 0, SEEK_END);
		//close(fd);
		//m = file2mem(argv[0], 0x08600000, 0);//lseek(fd, 0, SEEK_MEM);
		m = mmap(NULL, 0, 0, ends_with((char *)argv[0], 'z') ? MAP_COMPRESSED : 0, fd, 0);
		close(fd);

		for(i=0; i<s; i++)
		{
			cc += m[i];
			//if((i&0xFFF) == 0)
			//{
			//	fprintf(stderr, "%x/%x\n", *((uint32*)&m[i]), cc);
			//}
			
		}
		
		fprintf(stderr, "ARG-%p %d %x (%x)\n", m, s, *((uint32*)m), cc);
		//m -= (l - 0x08000000);
		//fprintf(2, "Found %s at %x\n", argv[0] , m);
		p[-1] = (uint32)m;
		p[-2] = s;
	} else
	{
		p[-1] = 0;
		fprintf(stderr, "Couldnt make arguments\n");
	}

}

int execute_plugin(char *cmd, char *fname, int keys)
{
	uint32 *p;
	char *s;
	char *ptr;
	char tmp[64];
	const char *args[] = {NULL, NULL};
	int fd;
	args[0] = fname;

	mod_stop();
	save_state();

	strcpy(tmp, GET_PATH(PLUGINS));
	strcat(tmp, cmd);

	//if(keys)
	//	reset_io();

	s = strrchr(cmd, '.');
	if(s && (strncmp(s, ".mb", 3) == 0))
	{
		// Multiboot plugin
		char id[4];
		
		fd = open(tmp, 0);
		
		ptr = mmap(NULL, 256, 0, ends_with(tmp, 'z') ? MAP_COMPRESSED : 0, fd, 0); //file2mem(tmp, NULL, 0);//255*1024-8);
		
		if(!ptr)
			return 0;
					
		id[0] = ptr[0xAD];
		id[1] = ptr[0xAE];
		id[2] = ptr[0xBD];
		
		unmmap(ptr);
		
		close(fd);

		if(keys == 1)
			savesys_handleexec(fname, NULL);
		else
		if(keys == 2)
			savesys_handleexec(cmd, id);


		fd = open(tmp, 0);
		ptr = mmap(NULL, 0, 0, ends_with(tmp, 'z') ? MAP_COMPRESSED : 0, fd, 0);
		
		fprintf(stderr, "ARGS\n");
		make_arguments2(cmd, args);

		set_ram_start(0);

		fprintf(stderr, "%p %x\n", ptr, *((uint32*)ptr));

		SETW(REG_IE, 0);
		SETW(REG_IF, 0);
		SETW(REG_IME, 0);
		SETW(REG_SOUNDBIAS, 0x0200);
		__FarProcedure2(MBExecute, ptr);
		
		//((void(*)(void))0x02000000)();

	}

	// Normal ROM

	if(keys == 1)
		savesys_handleexec(fname, NULL);
	else
	if(keys == 2)
		savesys_handleexec(cmd, NULL);

	set_ram_start(0);

	fd = open(tmp, 0);
	
	if(fd < 0)
		return -2;

#ifdef EZ3
	ptr = mmap(0x08400000, 0, 0, MAP_FIXED | MAP_ALIGN32, fd, 0);
#else
	ptr = mmap(NULL, 0, 0, MAP_ALIGN32, fd, 0);
#endif
	if(!ptr)
		return -1;
	fprintf(stderr, "ARGS\n");
	make_arguments2(cmd, args);
	p = (uint32 *)(0x02000000+255*1024);
	p[-1] = p[-1] - (uint32)ptr + 0x08000000;
	
	fprintf(stderr, "EXEC %p, ARG at %x\n", ptr, p[-1]);
	
	fcExecuteRom(((int)ptr) & 0x0FFFFFFF, 0);

	return 1;

}

int set_font(char *cmd, char *fname, int keys)
{
	Font *font = font_load(fname);
	font->flags |= FFLG_TRANSP;
	if(MainList->font)
		free(MainList->font);
	listview_set_attribute(MainList, WATR_FONT, font);
	return 1;
}

int changedir(char *cmd, char *fname, int keys)
{
	filesys_cd(fname);
	return 2;
}

int playmod(char *cmd, char *fname, int keys)
{
	//textbar_set_attribute(StatusBar, WATR_TEXT, "Playing music");
	SETW(REG_SOUNDBIAS, 0x0200);
	mod_play(fname);
	return 1;
}

int textreader_show_text(char *name);

int show_text(char *cmd, char *fname, int keys)
{
	textreader_show_text(fname);
	MainScreen->firstWindow->widget->flags |= WFLG_REDRAW;
	return 1;
}

int showbmp(char *cmd, char *fname, int keys)
{
	bmp_view(fname);
	MainScreen->firstWindow->widget->flags |= WFLG_REDRAW;
	return 1;
}

/*
void filetype_register(char *ext, BitMap *bm, int handler)
{
	FileType *ft;
	
	ft = filetypes[ftcount++] = malloc(sizeof(FileType));

	strcpy(ft->data, ext);
	if(handler == 0)
		ft->compare_func = check_extention;
	else
		ft->compare_func = check_dir;
	ft->icon = bm;
	ft->handle_func = NULL;

	if(!(ext && strlen(ext)))
		default_type = ftcount-1;
}
*/

void filetype_readtypes(FILE *fp)
{
	char buf[80];
	int i, rc;
	FileType *f;
	char *p, *ptr;

	if(find_section(fp, "filetypes"))
	{
		/* Every line is one filetype */
		while((rc = read_line(buf, sizeof(buf), fp)) >= 0)
		{
			if(rc > 0)
			{
				f = filetypes[ftcount++] = malloc(sizeof(FileType));
				//fprintf(2, "FTCOUNT:%d %p\n", ftcount, f);
				/*{
					char tt[80];
					sprintf(tt, "-%s-\n", buf);
					PrintStr(tt);
				}*/
		
				ptr = buf;				
				while(*ptr && *ptr++ != ' ');

				f->desc = NULL;
				f->command = NULL;
				f->compare_func = check_extention;
				f->handle_func = NULL;
				f->saver = 0;

				ptr[-1] = 0;

				/* First word is the extention */
				strcpy(f->data, buf);
				//sprintf(tmp, "EXT:%s\n", buf);
				//PrintStr(tmp);

				/* Second word is the icon */
				if(isdigit(*ptr))
				{
					i = atoi(ptr);
					//fprintf(stderr, "ICON:%d\n", i);

					f->icon = icon_set[i];
					while(isdigit(*ptr++));
				}
				else
				{
					//FILE *fp2;
					p = ptr;
					while(!isspace(*ptr)) ptr++;
					*ptr++ = 0;
					//fp2 = fopen(p, "rb");
					//f->icon = bitmap_readbm(fp2);
					//fclose(fp2);
					f->icon = bitmap_loadbm(p);
				}

				p = ptr;

				/* Third word is the handler */
				while(*ptr && (*ptr != ' ')) ptr++; // && (*ptr != 10) && (*ptr != 13)) ptr++;
				if(*ptr == ' ')
				{
					char *q = ptr+1;

					if(isdigit(*q))
					{
						f->saver = atoi(q);
						//fprintf(stderr, "SAVER:%d\n", f->saver);
						while(isdigit(*q++));
						*ptr = 0;
						ptr = q-1;
					}

					/* The rest of the line is the description, if it exists */
					while(*q) q++; //(*q != 10) && (*q != 13)) q++;
					*q = 0;
					f->desc = malloc(strlen(ptr+1)+1);
					strcpy(f->desc, ptr+1);
					//fprintf(stderr, "DESC:%s\n", ptr+1);
				}
				*ptr = 0;
				/* Set correct internal handler for filetype */

				if(strcmp(p, "EXE") == 0)
					f->handle_func = execute_rom;
				else
				if(strcmp(p, "DIR") == 0)
				{
					f->compare_func = check_dir;
					f->handle_func = changedir;
				}
				else
				if(strcmp(p, "DEF") == 0)
				{	
					filetypes[0] = f;
					ftcount--;
				}
				else
				if(strcmp(p, "SET") == 0)
				{	
					settings_icon(f->icon);		
					ftcount--;
				}
				else
				if(strcmp(p, "DEV") == 0)
				{	
					f->compare_func = check_dev;
					f->handle_func = changedir;
				}
				else
				if(strcmp(p, "TXT") == 0)
					f->handle_func = show_text;
				else
				if(strcmp(p, "MB") == 0)
					f->handle_func = execute_mb;
				else
				if(strcmp(p, "FNT") == 0)
					f->handle_func = set_font;
				else
				if(strcmp(p, "MOD") == 0)
					f->handle_func = playmod;
				else
				if(strcmp(p, "BMP") == 0)
					f->handle_func = showbmp;
				else
				{
					/* No internal filetype found so assume handler is an actual rom that will be
					executed for files with this extention */
					f->handle_func = execute_plugin;
					f->command = malloc(strlen(p)+1);
					strcpy(f->command, p);
					//fprintf(stderr, "PLUGIN:%s\n", p);

				}
			}
		}
	}

}

void filetype_set_iconset(BitMap **icons)
{
	icon_set = icons;
}

int filetype_lookup(DirList *entry)
{
	int i;
	for(i=1; i<ftcount; i++)
	{
		if(filetypes[i]->compare_func(filetypes[i]->data, entry))
			return i;
	}

	return 0;
}

int filetype_handle(char *fname, int type, int keys)
{
	if(!type)
		return 0;

	//fprintf(stderr, "Handling '%s' type %d\n", fname, type);
	if(filetypes[type]->desc)
	{
		//textbar_set_attribute(StatusBar, WATR_TEXT, filetypes[type]->desc);
		statusbar_set(filetypes[type]->desc);
		screen_redraw(MainScreen);
	}
	return filetypes[type]->handle_func(filetypes[type]->command, fname, keys | filetypes[type]->saver);
}


BitMap *filetype_icon(int type)
{
	return filetypes[type]->icon;
}
