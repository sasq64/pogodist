
#include <pogo.h>

#include "gba_defs.h"
#include "nmodgcc.h"

#include "misc.h"

typedef void (*VoidFunc)(void);
typedef uint32 (*IntFunc)(void);

extern void LZ77UnCompWram(void *Srcp, void *Destp);

//extern void __FarProcedure (void (*ptr)(), ...);  // Reference to routine in crt0.S
extern void __FarProcedure2 (void (*ptr)(), ...);  // Reference to routine in crt0.S


static volatile void mod_update(void)
{
	int i = GETW(REG_IF);
	if(i & TIMER1_INTR_FLAG)
	{
		__FarFunction((IntFunc)NMOD_Timer1iRQ);
	}
}

static Device moddev;

static uchar *play = NULL;

static int mod_open(const char *name, int flags)
{
	char tmp[80] = "";
	int fd;
	const char *s;

	if(*name != '/')
		strcpy(tmp, "/");
	strcat(tmp, name);
	
	s = &name[strlen(name)-1];
	if((*s == 'z') || (*s == 'Z'))
		flags |= MAP_COMPRESSED;

	fd = open(tmp, 0);
	play = mmap(NULL, 0, 0, flags, fd, 0);
	close(fd);
	
	if(play)
	{
		fprintf(stderr, "MOD PLAY %s %p\n", name, play);
		__FarProcedure2((VoidFunc)NMOD_SetMasterVol, 64, 0);
		__FarProcedure2((VoidFunc)NMOD_SetMasterVol, 64, 1);
		__FarProcedure2((VoidFunc)NMOD_SetMasterVol, 64, 2);
		__FarProcedure2((VoidFunc)NMOD_SetMasterVol, 64, 3);
		__FarProcedure2((VoidFunc)NMOD_Play, play);
		return 0;
	}

	return -1;

}

static int mod_close(int fd)
{
	//int l;

	NMOD_Stop();
	//l = GETW(REG_IE);
    //SETW(REG_IE, l & (~TIMER1_INTR_FLAG));
	SETW(REG_IF, TIMER1_INTR_FLAG);
	
	//memory_setarea(2);
	if(play)// && play < 0x08000000)
		unmmap(play);
	play = NULL;
	//memory_setarea(0);

	return 0;
}


void mod_init(void)
{
	//int l;
	memset(&moddev, 0, sizeof(moddev));
	moddev.open = mod_open;
	moddev.close = mod_close;
	device_register(&moddev, "/mod", mod_update, -1);

	//SETW(REG_IME, 1);
	//l = GETW(REG_IE);
    //SETW(REG_IE, l | TIMER1_INTR_FLAG);
}

static int modfd = -1;

int mod_play(char *current)
{
	char tmp[128];
	char *p = current;
	//while(*p == '/') p++;
	//if(strncmp(p, "rom", 3) == 0) p += 3;
	//while(*p == '/') p++;

	sprintf(tmp, "/mod%s", p);

	//fprintf(stderr, "MOD %s\n", tmp);

	if(modfd >= 0)
		close(modfd);

	modfd = open(tmp, 0);
	return 1;
}

void mod_stop(void)
{
	if(modfd >= 0)
		close(modfd);
	modfd = -1;
}
