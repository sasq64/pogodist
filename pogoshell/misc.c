#include <pogo.h>
#include "misc.h"

extern void LZ77UnCompWram(void *Srcp, void *Destp);
extern void LZ77UnCompVram(void *Srcp, void *Destp);

void memcpy16(uint16 *d, uint16 *s, int len)
{
	while(len--)
		*d++ = *s++;
}

void memset16(uint16 *d, uint16 v, int len)
{
	while(len--)
		*d++ = v;
}

void Uncompress(char *src, char *dst)
{
	int size;
	uint32 *s;

	s = (uint32 *)src;
	
	size = ((s[0]>>8)+3) & 0xFFFFFFFC;

	/*if(dst >= 0x08400000)
	{
		fprintf(stderr, "Temp unpack to WRAM\n");
		LZ77UnCompWram(src, 0x02040000-size);
		memcpy16(dst, 0x02040000-size, size);
	}
	else*/
	if(dst < (char *)0x03000000)
		LZ77UnCompWram(src, dst);
	else
		LZ77UnCompVram(src, dst);
}

int read_safe(int fd, void *dest, int length)
{
	int rc = 1;
	int total = 0;
	uint16 buf[512];
	uint16 *d = dest;
	
	while((rc > 0) && (length > 0))
	{
		rc = read(fd, buf, length > 1024 ? 1024 : length);
		if(rc < 0)
			return rc;
		if(rc)
		{
			total += rc;
			memcpy16(d, buf, (rc+1)/2);
			d += ((rc+1)/2);
			length -= rc;
		}
	}
	
	return total;
}

char *strdup(char *str)
{
	char *p = malloc(strlen(str)+1);
	strcpy(p, str);
	return p;
}

char *basename(char *str)
{
	char *p = strrchr(str, '/');
	if(p)
		return p+1;
	else
		return str;
}

int read_line(char *line, int size, FILE *fp)
{
	//char tmp[size+2];
	int l;
	char *p, *d, *endp;
	
	if(fgets(line, size, fp))
	{
		//strncpy(line, tmp, size-1);
		line[size-1] = 0;
		
		//fprintf(2, "RL '%s'\n", line);
		
		endp = line;
		
		// Goto end
		while(*endp) endp++;

		// Erase LF and trailing whitespace
		endp--;
		while((endp >= line) && ((*endp == 10) || (*endp == 13) || (*endp == 32)))
		{
			*endp = 0;
			endp--;
		}
		
		// Erase leading whitespace
		d = p = line;		
		while(*p && (*p == ' ' || *p == 9))
			p++;
		while(*p)
			*d++ = *p++;
		*d = 0;
		
		
		if((l = strlen(line)))
		{
			//p = &line[l-1];
			//while((p > line) && (*p == 10 || *p == 13 || *p == 32))
			//	p--;
			//p[1] = 0;

			if(*line == '#')// || !strlen(line))
				return 0;

			if(*line == '[' && *endp == ']')
				return -1;
			
			return 1;
		}
		return 0;
	}
	return -2;
}


int find_section(FILE *fp, char *name)
{
	char tmp[24];
	char line[32];
	sprintf(tmp, "[%s]", name);

	fseek(fp, 0, SEEK_SET);

	//fprintf(2, "Looking for %s\n", tmp);

	while(read_line(line, sizeof(line), fp) != -2)
	{
		//fprintf(2, "LINE '%s'\n", line);
		if(strcmp(line, tmp) == 0)
		{
			//fprintf(2, "'%s' == '%s'\n", tmp, line);
			return 1;
		}
	}
	//fprintf(2, "Didnt find %s\n", name);
	return 0;
}

void *mmap(void *start, int length, int prot, int flags, int fd, int offset)
{
	char *ptr = NULL; // Allocated pointer if any
	char *rptr; // Return pointer
	int compsize = 0; // Compressed size if present
	int fsize, align;

#ifdef EZ3
	memory_setarea(2);
#endif

	
	fsize = lseek(fd, 0, SEEK_END);
	lseek(fd, offset, SEEK_SET);
	rptr = (uchar *)lseek(fd, 0, SEEK_MEM);
		
	if(flags & MAP_COMPRESSED)
	{
		int cs;
		read(fd, &cs, sizeof(int));
		fprintf(stderr, "COMP:%x\n", cs);

		if((cs&0xff) == 0x10)
			compsize = ((cs>>8)+3) & 0xFFFFFFFC;
		else
			compsize = 0;
		
		lseek(fd, offset, SEEK_SET);
	}
	
	fsize = (fsize-offset+3) & 0xFFFFFFFC;

	if(!length)
		length = fsize;

	fprintf(stderr, "rptr=%08x, length=%d\n", rptr, length);
	
	if(flags & MAP_FIXED)
		fprintf(stderr, "Trying to FIX to %p\n", start);

	if(!rptr)
	{
		int rc = -1;
		if(compsize)
		{
			if(flags & MAP_FIXED)
				rptr = ptr = malloc_fixed(start, compsize + 2048);
			else
				rptr = ptr = malloc(compsize + 2048);

			if(ptr)
			{
				char *unpack =  ptr+compsize+2048-length;
				rc = read_safe(fd, unpack, length);
				//fprintf(stderr, "%x %x %x %x\n", unpack[0], unpack[1], unpack[2], unpack[3]);
				fprintf(stderr, "Unpacking from %p to %p\n", unpack, ptr);
				Uncompress(unpack, ptr);

			}
		}
		else
		{
			if(flags & MAP_FIXED)
				rptr = ptr = malloc_fixed(start, length);
			else
				rptr = ptr = malloc(length);

			if(ptr)
				rc = read_safe(fd, ptr, length);	
		}

		fprintf(stderr, "Read %d bytes to %p\n", rc, ptr);
	}
	else
	{
		if(compsize)
		{
			if(flags & MAP_FIXED)
				ptr = malloc_fixed(start, compsize);
			else
				ptr = malloc(compsize);

			if(ptr)
				Uncompress(rptr, ptr);
			fprintf(stderr, "Unpacking from %p to %p\n", rptr, ptr);
			rptr = ptr;
		}
		else
		{
			if(flags & MAP_FIXED)
			{
				fprintf(stderr, "FIXED forces copy from %p -> %p\n", rptr, start);
				ptr = malloc_fixed(start, length);
				if(ptr)	
				{
					memcpy16((uint16 *)ptr, (uint16 *)rptr, length);

				}
				rptr = ptr;
			}
		}
		
		
	}
	
	if(!rptr)
	{
		fprintf(stderr, "READ failed\n");
	}
	
	if((prot & PROT_EXEC) && (rptr < (char *)0x08000000))
	{
		rptr = NULL;
		fprintf(stderr, "EXEC failed\n");
	}

	if((prot & PROT_WRITE) && (rptr > (char *)0x03008000))
	{
		rptr = NULL;
		fprintf(stderr, "WRITE failed\n");
	}

	align = flags & ALIGN_MASK;
	
	// Check that pointer is aligned properly
	if(align && ((align<<10)-1) & (uint32)rptr )
	{
		rptr = NULL;
		fprintf(stderr, "ALIGN failed (%x)\n", align<<10);
	}

	// Check that fixed mapping was possible
	if((flags & MAP_FIXED) && (rptr != start))
	{
		rptr = NULL;
		fprintf(stderr, "FIXED failed\n");
	}
		
	
	if(!rptr && ptr)
		free(ptr);
	
#ifdef EZ3
	memory_setarea(0);
#endif

	return rptr;
}

void unmmap(void *ptr)
{
#ifdef EZ3
	memory_setarea(2);
#endif

	free(ptr);

#ifdef EZ3
	memory_setarea(0);
#endif
}

int parse_assignment(char *line, char **name, char **val)
{
	char *p;
	if(strchr(line, '='))
	{
		p = line;
		while(!isalpha(*p)) p++;
		*name = p;
		while(*p != '=') p++;
		*p++ = 0;
		*val = p;
		while(*p >= 0x20) p++;
		*p = 0;
		return 1;
	}
	return 0;
}

Font *font_load_path(char *name)
{
	Font *f;
	char tmp[80];
	strcpy(tmp, GET_PATH(FONTS));
	strcat(tmp, name);
	//sprintf(tmp, ".shell/fonts/%s", name);
	f = font_load(tmp);
	//fprintf(stderr, "%s -> %p\n", tmp, f);
	return f;
}
