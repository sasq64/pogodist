
#include <pogo.h>

#include "rle.h"
#include "text.h"
#include "settings.h"
#include "users.h"
#include "misc.h"
#include "msgbox.h"
void statusbar_set(char *text);

int save_banks = 1;

//extern int sram_game_size = 64;


// Save the last game from bank 0 to a file in the sram filesystem
int savesys_savelastgame()
{
	int rc = 0, fd, b;
	int found;
	char id[4];

	// See if we just executed a ROM and should save a savefile for it
	fd = open("/sram/.lastexec", 0);
	if(fd >= 0)
	{
		uchar usr;
		char savefile[40];
		read(fd, &usr, 1);
		read(fd, id, 3);
		read(fd, savefile, 128);
		close(fd);

		//sram_setuser(usr);

		// Check if there is anything to save
		found = 0;
		for(b=0; !found && b<save_banks; b++)
		{
			uchar *ptr = (uchar *)0x0E00FFFF;
			set_ram_start(b);
			while(((uint32)ptr >= 0x0E000000) && (ptr[0] == 0)) ptr--;
			found = (ptr - 0x0E000000 + 1 > 0);
		}

		if(found)
		{
			int do_save = 1;

			int testf = open(savefile, 0);
			close(testf);

			if(settings_get(SF_ASKSAVE))
			{
				char q[64];
				if(testf >= 0)
					sprintf(q, TEXT(WISH_OVERWRITE), basename(savefile));
				else
					sprintf(q, TEXT(WISH_SAVE), basename(savefile));
				do_save = msgbox_yesno(q);
			}
			
			if(do_save)
			{
				fd = open(savefile, O_CREAT);
				write(fd, "SAVE", 5);
				write(fd, &id, 3);
				rc = 0;
				for(b=0; (rc >= 0) && (b < save_banks); b++)
					rc = save_rle(fd, b);
				if(rc >= 0)
				{
					char q[64];
					sprintf(q, TEXT(SAVE_DONE), basename(savefile));
					statusbar_set(q);
				}
				close(fd);
			}
			else
			{
				statusbar_set(TEXT(SAVE_CANCEL));
			}
		}

		if(rc >= 0)
			remove("/sram/.lastexec");
		else
			statusbar_set(TEXT(SAVE_FAILED));
	}

	return rc;
}

void make_savename(char *dst, char *src, char *ext)
{

	char *ptr = basename(src);

	sprintf(dst, "/sram/%s", ptr);
	if(strlen(dst) > 34)
		dst[34] = 0;

	ptr = strrchr(dst, '.');
	if(ptr)
		*ptr = 0;
	strcat(dst, ext);
}


void make_saveheader(char *dest, char *rom)
{
	int fd = -1;
	char *p, *name;
	// Get ID from rom to execute
	strcpy(dest, "SAVE");

	name = basename(rom);

	//p = strrchr(rom, '.');
	//if(p && (strncmp(p, ".mb", 3) == 0))
		//p = (char *)0x02000000;
	//	return;

	//else
	{
		fd = open(rom, 0);
		p = (char *)lseek(fd, 0, SEEK_MEM);
	}

	if(p < (char*)0x0A000000)
	{
		// Get 2byte ID and complement (checksum)
		dest[5] = p[0xAD];
		dest[6] = p[0xAE];
		dest[7] = p[0xBD];
	}
	else
	{
		int i;
		int cc = 0;
		int l = strlen(name);
		// Rom is above 256MBit limit, use name as ID instead
		for(i=0; i<l; i++)
			cc += name[i];
		dest[5] = cc>>8;
		dest[6] = cc&0xff;
		dest[7] = name[l-1];
	}


	if(fd >= 0)
		close(fd);
}

/* Rom (GBA/BIN) executor */
int savesys_handleexec(char *romname, char *id)
{
	int do_load = 0;
	int found = 0;
	int b,i,fd, rc = 1;
	char savename[40];
	char header[64];
	char *p;

	make_saveheader(header, romname);
	if(id)
	{
		header[5] = id[0];
		header[6] = id[1];
		header[7] = id[2];
	}
	
	make_savename(savename, romname, ".sav");

	// Try to open a savefile with the same name as rom
	if((fd = open(savename, 0)) < 0)
	{
		struct dirent *de;
		DIR *d;

		// If no name-match, check all files in sram for a matching savefile
		d = opendir("/sram");
		if(d)
		{
			p = strrchr(savename, '/');
			p++;
			while(!found && (de = readdir(d)))
			{
				sprintf(p, "%s", de->d_name);
				fd = open(savename, 0);
				if(fd >= 0)
				{
					read(fd, id, 8);
					if(memcmp(id, header, 8) == 0)
					{
						found = 1;
						close(fd);
					}
				}
			}
			closedir(d);
		}
	}
	else
	{
		close(fd);
		found = 1;
	}

	// Clear savebank(s)
	for(b=0; b<save_banks; b++)
	{
		set_ram_start(b);
		p = (char *)0x0E000000;
		i = 64*1024;
		while(i--)
			*p++ = 0;
	}
	/* Load the save file */
	if(found && rc)
	{
		do_load = 1;
		if(settings_get(SF_ASKLOAD))
		{
			char q[64];
			sprintf(q, TEXT(WISH_LOAD), basename(savename));
			do_load = msgbox_yesno(q);
		}

		if(do_load)
		{
			char lhead[8];
			fd = open(savename, 0);
			read(fd, lhead, 8);
			if(!(lhead[0] == 'S' && lhead[1] == 'A' && lhead[2] == 'V' && lhead[3] == 'E'))
				lseek(fd, 0, SEEK_SET);
			for(b=0; b<save_banks; b++)
				load_rle(fd, b);
			close(fd);
		}
	}

	// Save the last_exec file with info on the savefile
	remove("/sram/.lastexec"); // Just to make sure
	fd = open("/sram/.lastexec", O_CREAT);
	if(fd >= 0)
	{
		uchar usr = CurrentUser;
		if(!do_load)
			make_savename(savename, romname, ".sav");
		write(fd, &usr, 1);
		write(fd, header+5, 3);
		write(fd, savename, strlen(savename)+1);
		close(fd);
	}
	
	return 1;
}
