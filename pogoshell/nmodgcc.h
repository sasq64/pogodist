/*************************************************************
             NMOD v1 beta by NEiM0D/QTX!
     The smallest, baddest, fastest module player for GBA.
**************************************************************/
#ifndef NMOD_PLAYER
#define NMOD_PLAYER
/********************** DEFINES ******************/
#define MAX_CHANNELS	4
#define BUFFERS		2
#define BUFFER_SIZE	1344
/*************************************************/

typedef unsigned int u32;
typedef signed int s32;
typedef unsigned char u8;
typedef signed char s8;

/******************** VARIABLES ******************/
/*u32 NMOD_scanlines;
u8 NMOD_volume[MAX_CHANNELS];
u8 NMOD_instrument[MAX_CHANNELS];
u32 NMOD_period[MAX_CHANNELS];
u8 NMOD_pattern;
s8 NMOD_row;
u32 NMOD_modaddress;
u32 NMOD_effect[MAX_CHANNELS];
u8 NMOD_tick;
u8 NMOD_speed;
s8 NMOD_buffera[BUFFERS][BUFFER_SIZE];
s8 NMOD_bufferb[BUFFERS][BUFFER_SIZE];*/
/*************************************************/

/****************** FUNCTIONS ********************/
void NMOD_Play(u32 pt_modaddress);
 /*
 NMOD_Play(): Play the protracker module song.
     u32 pt_modaddress: Holds the address where 
                        the module is stored.
 */

void NMOD_SetMasterVol(u8 mastervol,u8 soundchan);
 /*
 NMOD_SetMasterVol(): Sets the mastervolume for soundchan.
     mastervol: volume to be set (0-64)
     soundchan: sound channels to set volume (0-3)
 */

void NMOD_Stop(void);
 /*
 NMOD_Stop(): Stop playing.
 */

void NMOD_Timer1iRQ(void);
 /*
 Has no use for the user, just to let the compiler know to add
 this function to the project.
 */
/*************************************************/
#endif
