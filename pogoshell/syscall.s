	.ALIGN
	.ARM

	.GLOBAL		LZ77UnCompWram
	.GLOBAL		LZ77UnCompVram	
	.GLOBAL		SoftReset
	.GLOBAL		MBExecute

@extern void LZ77UnCompWram(void *Srcp, void *Destp)
LZ77UnCompWram:

	stmfd	sp!,{r0-r12,lr}
0:
	ldrb	r3,[r0],#1
	cmp		r3,#0x20
	beq		0b

	sub		r0,r0,#1

	swi	#0x110000
	ldmfd	sp!,{r0-r12,lr}
	bx lr

	.ALIGN

@extern void LZ77UnCompVram(void *Srcp, void *Destp)
LZ77UnCompVram:
	stmfd	sp!,{r0-r12,lr}
0:
	ldrb	r3,[r0],#1
	cmp		r3,#0x20
	beq		0b

	sub		r0,r0,#1

	swi	#0x120000
	ldmfd	sp!,{r0-r12,lr}
	bx lr

	.ALIGN
	.POOL

SoftReset:

	stmfd	sp!,{r0-r12,lr}

	swi	#0x010000
	ldmfd	sp!,{r0-r12,lr}
	bx lr


	.SECTION	.iwram
	.ALIGN
	.ARM

MBExecute:

	ldr  r1,=0x02000000
	ldr	 r3,=0x0203F000
0:
	ldrh  r5,[r0],#2
	strh  r5,[r1],#2
	cmp	 r1,r3
	bne	 0b
		
	mov	 r0,#0xFC
	mov	 r1,#0
	swi	 #0x010000
	ldr	 r1,=0x02000000
	bx	 r1

	.ALIGN
	.POOL

	.END


