#ifndef MISC_H
#define MISC_H

char *basename(char *str);
int read_line(char *line, int size, FILE *fp);
int find_section(FILE *fp, char *name);
uchar *file2mem(char *fname, void *mem, int msize);
int parse_assignment(char *line, char **name, char **val);
Font *font_load_path(char *name);
char *strdup(char *str);
extern char *path[4];
enum {PATH_PLUGINS, PATH_THEMES, PATH_FONTS, PATH_BITMAPS};
#define GET_PATH(x) (path[PATH_ ## x])

#define MBIT_SHIFT 17

#define MAP_FIXED      1
#define MAP_COMPRESSED 2
#define MAP_ALIGN32   32
#define MAP_ALIGN64   64
#define MAP_ALIGN128 128

#define ALIGN_MASK 0xE0

#define PROT_NONE 0
#define PROT_READ 1
#define PROT_WRITE 2
#define PROT_EXEC 4

void *mmap(void *start, int length, int prot, int flags, int fd, int offset);
void unmmap(void *ptr);


#endif
