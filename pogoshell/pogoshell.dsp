# Microsoft Developer Studio Project File - Name="pogoshell" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=pogoshell - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "pogoshell.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "pogoshell.mak" CFG="pogoshell - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "pogoshell - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "pogoshell - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "pogoshell - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f pogoshell.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "pogoshell.exe"
# PROP BASE Bsc_Name "pogoshell.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "make"
# PROP Rebuild_Opt "rebuild"
# PROP Target_File "pogo.gba"
# PROP Bsc_Name "pogoshell.bsc"
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "pogoshell - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f pogoshell.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "pogoshell.exe"
# PROP BASE Bsc_Name "pogoshell.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "make"
# PROP Rebuild_Opt "rebuild"
# PROP Target_File "pogo.gba"
# PROP Bsc_Name "pogoshell.bsc"
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "pogoshell - Win32 Release"
# Name "pogoshell - Win32 Debug"

!IF  "$(CFG)" == "pogoshell - Win32 Release"

!ELSEIF  "$(CFG)" == "pogoshell - Win32 Debug"

!ENDIF 

# Begin Group "Source"

# PROP Default_Filter "C"
# Begin Source File

SOURCE=.\backdrop.c
# End Source File
# Begin Source File

SOURCE=.\bitmap.c
# End Source File
# Begin Source File

SOURCE=.\bmpview.c
# End Source File
# Begin Source File

SOURCE=.\filesys.c
# End Source File
# Begin Source File

SOURCE=.\filetype.c
# End Source File
# Begin Source File

SOURCE=.\gameromfs.c
# End Source File
# Begin Source File

SOURCE=.\guiparser.c
# End Source File
# Begin Source File

SOURCE=.\main.c
# End Source File
# Begin Source File

SOURCE=.\misc.c
# End Source File
# Begin Source File

SOURCE=.\modplayer.c
# End Source File
# Begin Source File

SOURCE=.\msgbox.c
# End Source File
# Begin Source File

SOURCE=.\rle.c
# End Source File
# Begin Source File

SOURCE=.\savesystem.c
# End Source File
# Begin Source File

SOURCE=.\settings.c
# End Source File
# Begin Source File

SOURCE=.\smallstub.c
# End Source File
# Begin Source File

SOURCE=.\sram_convert.c
# End Source File
# Begin Source File

SOURCE=.\text.c
# End Source File
# Begin Source File

SOURCE=.\users.c
# End Source File
# Begin Source File

SOURCE=.\window.c
# End Source File
# End Group
# Begin Group "Include"

# PROP Default_Filter "H"
# Begin Source File

SOURCE=.\backdrop.h
# End Source File
# Begin Source File

SOURCE=.\bitmap.h
# End Source File
# Begin Source File

SOURCE=.\filesys.h
# End Source File
# Begin Source File

SOURCE=.\filetype.h
# End Source File
# Begin Source File

SOURCE=.\gammatab25.h
# End Source File
# Begin Source File

SOURCE=.\gammatab55.h
# End Source File
# Begin Source File

SOURCE=.\gammatab75.h
# End Source File
# Begin Source File

SOURCE=.\gba_defs.h
# End Source File
# Begin Source File

SOURCE=.\guiparser.h
# End Source File
# Begin Source File

SOURCE=.\misc.h
# End Source File
# Begin Source File

SOURCE=.\modplayer.h
# End Source File
# Begin Source File

SOURCE=.\msgbox.h
# End Source File
# Begin Source File

SOURCE=.\NMODGCC.H
# End Source File
# Begin Source File

SOURCE=.\rle.h
# End Source File
# Begin Source File

SOURCE=.\savesystem.h
# End Source File
# Begin Source File

SOURCE=.\settings.h
# End Source File
# Begin Source File

SOURCE=.\text.h
# End Source File
# Begin Source File

SOURCE=.\users.h
# End Source File
# Begin Source File

SOURCE=.\window.h
# End Source File
# End Group
# Begin Group "Widgets"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\widgets\listview.c
# End Source File
# Begin Source File

SOURCE=.\widgets\listview.h
# End Source File
# Begin Source File

SOURCE=.\widgets\textbar.c
# End Source File
# Begin Source File

SOURCE=.\widgets\textbar.h
# End Source File
# Begin Source File

SOURCE=.\widgets\textflow.c
# End Source File
# Begin Source File

SOURCE=.\widgets\textflow.h
# End Source File
# Begin Source File

SOURCE=.\widgets\textreader.c
# End Source File
# Begin Source File

SOURCE=.\widgets\textreader.h
# End Source File
# Begin Source File

SOURCE=.\widgets\tricontainer.c
# End Source File
# Begin Source File

SOURCE=.\widgets\tricontainer.h
# End Source File
# Begin Source File

SOURCE=.\widgets\widgets.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\Makefile
# End Source File
# Begin Source File

SOURCE=..\PogoShellDist\root\.shell\pogo.cfg
# End Source File
# End Target
# End Project
