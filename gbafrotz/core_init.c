/* Standard core_init() for pogo programs
 * -- Jonas Minnberg 2002
 */

#include "device.h"
#include "font.h"

extern void deb_init(void); /* /dev/deb */
extern void screen_init(void); /* /dev/screen */
extern void vkey_init(void); /* /dev/vkey */
extern void key_init(void); /* /dev/key */
extern void console_init(void); /* /dev/con */
extern void filesys_init(void); /* /rom */
extern void sram_init(void); /* /sram */

void core_init(void)
{
	//smartkey_init();
}
