#include "pogo.h"
#include "frotz.h"

#include "console.h"

/* Attributes */

static Font *fixed_font;
static Font *prop_font;

//static int inverse = 0;
//static int bold = 0;
static int current_style = 0;

static int fg = 15;
static int bg = 0;

int os_char_width (zchar z)
{
	char s[2] = {0, 0};
	*s = z;
	return ioctl(1, CC_GETWIDTH, s);
}

int os_string_width (const zchar *s)
{
	int rc = 0;

	while(*s) {
		if(*s != ZC_NEW_FONT && *s != ZC_NEW_STYLE)
			rc += os_char_width(*s);
		else
			s++;
		s++;
	}
	return rc;
}

void os_set_cursor(int row, int col)
{
	DPRINTF("<cursor %d,%d>", row, col);

	printf("\033[%d;%dH", row-1, col-1);
}

void os_display_char (zchar c)
{
	putchar(c);
}

void os_display_string (const zchar *s)
{
	zchar c;
	DPRINTF("%s", s);
	while ((c = *s++) != 0)
	{
		if(c == ZC_NEW_FONT)
			os_set_font(*s++);
		else if(c == ZC_NEW_STYLE)
			os_set_text_style(*s++);
		else
			os_display_char(c);
	}
}

void os_erase_area (int top, int left, int bottom, int right)
{
	DPRINTF("<clear>");
	ioctl(1, CC_CLEAR, top-1, bottom-top+1);
}

void console_scroll(int start, int rows, int count);

void os_scroll_area (int top, int left, int bottom, int right, int units)
{
	DPRINTF("<scroll>");
	printf("\033[%d;%dr\033M", top-1, bottom-top+1);
}

int os_font_data(int font, int *height, int *width)
{
    if(font == TEXT_FONT)
	{
      *height = 1; *width = prop_font->charwidth; return 1;
    }
	else
    if(font == FIXED_WIDTH_FONT || font == GRAPHICS_FONT)
	{
      *height = 1; *width = fixed_font->charwidth; return 1;
    }
    return 0;
}


void os_set_text_style(int x)
{
	current_style = x;
	//bold = (x&2) * 4;
	//inverse = x&1;

	DPRINTF("<style %d>", x);

	if(x & FIXED_WIDTH_STYLE)
		printf("\0331f");
	else
		printf("\0330f");
	os_set_colour(fg, bg);
}

int def_fgcol = 7;
int def_bgcol = 0;
int def_fgrev = -1;
int def_bgrev = -1;

void os_set_colour (int x, int y)
{
	int f,b,a = 0;
	int inverse = current_style&1;

	if(x == 0)
		x = def_fgcol+2;
	if(y == 0)
		y = def_bgcol+2;

	fg = x;
	bg = y;

	if(def_fgrev > 0)
		f =  inverse ? 30+def_fgrev : 30+fg-2;	
	else
		f =  inverse ? 30+bg-2 : 30+fg-2;	

	if(def_bgrev > 0)
		b =  inverse ?  40+def_bgrev : 40+bg-2;	
	else
		b =  inverse ?  40+fg-2 : 40+bg-2;	

	if(f > 37)
		f -= 8;

	printf("\033%d;%d;%dm", f, b, (current_style & 0x6));
}

void os_set_font(int x)
{
	switch(x)
	{
	case FIXED_WIDTH_FONT:
		printf("\0331f");
		break;
	case TEXT_FONT:
		printf("\0330f");
		break;
	case GRAPHICS_FONT:
		printf("\0332f");
		break;
	}
}

void os_reset_screen(void)
{
}

void os_beep (int volume)
{
}

void os_prepare_sample (int x) {}
void os_finish_with_sample (void) {}
void os_start_sample (int x, int y, int z) {}
void os_stop_sample (void) {}

void gba_init_output(void)
{
	int width, height;

	
	width = ioctl(1, IO_GETPARAM, CP_WIDTH);
	height = ioctl(1, IO_GETPARAM, CP_HEIGHT);
	prop_font = (Font *)ioctl(1, CC_GETFONT, 0);
	fixed_font = (Font *)ioctl(1, CC_GETFONT, 1);

	fprintf(2, "%d %d\n", width, height);

	h_screen_height = height/prop_font->height;
	h_screen_width = width;
	h_screen_cols = width/fixed_font->charwidth;
	h_screen_rows = height/prop_font->height;
	h_font_width = fixed_font->charwidth;
	h_font_height = 1;
	bg = 9;
	os_set_colour(fg, bg);
	bg = 0;
}
