#include "pogo.h"
#include "frotz.h"
#include "device.h"
#include "smartkey.h"

FILE *in_fp;

//extern int con_mode;

void Halt(void);

zchar os_read_key (int timeout, bool show_cursor)
{
	int c = EOF;
	int tm = clock();

	DPRINTF("Read key %d\n", show_cursor);

	ioctl((int)in_fp, IO_SETMODE, show_cursor ? CM_SHOWCURSOR : 0);

	timeout = timeout * CLOCKS_PER_SEC / 10;
	while(c == EOF) {
		c = getc(in_fp);
		if(timeout && ((clock() - tm)  > timeout))
			return ZC_TIME_OUT;
		if(c == EOF)
			Halt();
	}
	if(c == 10)
		c = ZC_RETURN;

	ioctl((int)in_fp, IO_SETMODE, 0);
	return c;
}

static char *last_buf;
void smart_linemode(int);

zchar os_read_line (int max, zchar *buf, int timeout, int width, int continued)
{
	int rc = 0;
	int c = 0;
	int tm = clock();
	last_buf = buf;

	ioctl((int)in_fp, IO_SETMODE, CM_SHOWCURSOR);
	*buf = 0;

	if(timeout)
		timeout = tm + (timeout * CLOCKS_PER_SEC / 10);

	smart_linemode(1);

	while(!rc)
	{
		rc = read((int)in_fp, buf, 999);
		if(timeout && clock() > timeout) {
			smart_linemode(0);
			return ZC_TIME_OUT;
		}
		if(!rc)
			Halt();
	}

	smart_linemode(0);

	ioctl((int)in_fp, IO_SETMODE, 0);

	c = buf[rc-1];
	buf[rc-1] = 0;

	if(strlen(buf))
		ioctl((int)in_fp, KC_ADDHISTORY, buf);

	if(c == 10) {
		c = ZC_RETURN;
		putchar(10);
	}

	return c;

}

int os_read_file_name(char *file_name, const char *default_name, int flag)
{
	char *s, *endp;
	if(strncmp(default_name, "/sram/", 6) == 0)
		strcpy(file_name, "");
	else
		strcpy(file_name, "/sram/");

	endp = s = &story_name[strlen(story_name)];
	while(s >= story_name && *s != '/') {
		if(*s == '.')
			endp = s;
		s--;
	}
	s++;

	strcpy(file_name, "/sram/");
	strcat(file_name, s);
	file_name[endp-s+6] = 0;
	strcat(file_name, ".sav");

	//fprintf(2, "%s > %s filename\n", default_name, file_name);

	return TRUE;
}

void os_more_prompt (void)
{
	ioctl((int)in_fp, IO_SETMODE, 0);
	printf("[ENTER]");
	while(getc(in_fp) != 10)
		Halt();

	putchar(250);
}

#define BIND(a, b, c) ioctl((int)in_fp, KC_BINDKEY, a, b, c);
#define BIND0(a, b) ioctl((int)in_fp, KC_BINDKEY, a, b, 0);

const static char *predef1[] = {
	"in",
	"north",
	"up",
	"west",
	"east",
	"out",
	"south",
	"down",
	"restore",
	"save",
};

const static char *predef2[] = {
	"open ",
	"get ",
	"close ",
	"inventory",
	"look",
	"open ",
	"drop ",
	"read ",
	"examine ",
	"enter ",
};

const static char *predef3[] = {
	"climb ",
	"eat ",
	"attack ",
	"move ",
	"unlock ",
	"push ",
	"jump ",
	"pull ",
};

const static char *predef4[] = {
	"load",
	"score",
	"save",
	"wait",
	"again",
	"help ",
	"talk to ",
	"exit ",
};

const static int commands[] = 
{
	GO_MODE0,
	GO_MODE1,
	GO_MODE2,
	GO_MODE3,
	HISTORY_UP,
	HISTORY_DOWN,
	CURSOR_LEFT,
	CURSOR_RIGHT,
	SHOW_KEYBOARD,
	COMPLETE,
	MARKER_UP,
	MARKER_DOWN,
	MARKER_WORDLEFT,
	MARKER_WORDRIGHT,
	MARKER_INSERTBACK,
	MARKER_INSERT,
	MARKER_DICTBACK,
	MARKER_DICT,
	GET_DICT0,
	GET_DICT1,
	GET_DICT2,
	GET_DICT3,
};

const static char *cmdstrings[] =
{
	"GO_MODE0",
	"GO_MODE1",
	"GO_MODE2",
	"GO_MODE3",
	"HISTORY_UP",
	"HISTORY_DOWN",
	"CURSOR_LEFT",
	"CURSOR_RIGHT",
	"SHOW_KEYBOARD",
	"COMPLETE",
	"MARKER_UP",
	"MARKER_DOWN",
	"MARKER_WORDLEFT",
	"MARKER_WORDRIGHT",
	"MARKER_INSERTBACK",
	"MARKER_INSERT",
	"MARKER_DICTBACK",
	"MARKER_DICT",
	"GET_DICT0",
	"GET_DICT1",
	"GET_DICT2",
	"GET_DICT3",
};

extern int completion (const zchar *buffer, zchar *result);

static int inits = 0;

void smartkey_init(void);

//char *tmp_dict[10];
char **dlist[10];

void smartkey(void)
{
	int i;
	if(!inits)
	{
		inits = 1;
		while(getchar() != EOF);

		smartkey_init();

		in_fp = fopen("/dev/smartkey", "rb");

		ioctl((int)in_fp, KC_SETDICT, 0, predef1);
		ioctl((int)in_fp, KC_SETDICT, 1, predef2);
		ioctl((int)in_fp, KC_SETDICT, 2, predef3);
		ioctl((int)in_fp, KC_SETDICT, 3, predef4);
		ioctl((int)in_fp, KC_SETCOMPLETION, completion);
		for(i=0; i<10; i++)
			dlist[i] = NULL;
	}

}


void set_dictword(int dict, int pos, char *str)
{
	int i;
	smartkey();

	if(!dlist[dict])
	{
		dlist[dict] = malloc(10 * sizeof(char *));
		for(i=0; i<10; i++)
			dlist[dict][i] = NULL;

	}

	//if(tmp_dict[pos])
	//	free(tmp_dict[pos]);
	dlist[dict][pos] = malloc(strlen(str)+1);
	strcpy(dlist[dict][pos], str);

	//_dprintf("dict %d %s\n", dict, tmp_dict[pos]);
	ioctl((int)in_fp, KC_SETDICT, dict, dlist[dict]);
}

void gba_bind(char *str)
{
	char tmp[32];
	int i,cmd = -1;
	int mode = 0;
	char *ptr = tmp;

	smartkey();
	inits = 2;

	while(*str != ' ')
		*ptr++ = *str++;
	*ptr = 0;
	str++;
	if(isdigit(*str))
	{
		cmd = atoi(str);
	}
	else
	if(*str == '\'')
		cmd = str[1];
	else
	{
		for(i=0; (i<sizeof(commands)/sizeof(int)) && (cmd < 0); i++)
			if(strcmp(cmdstrings[i], str) == 0)
				cmd = commands[i];
	}
	if(cmd == -1) {
		printf("\014\n Unknown config command %s\n", str);
		while(getchar() == EOF);
	}
	while(*str++ != ' ');

	if(isdigit(*str))
		mode = *str - '0';

	BIND(tmp, cmd, mode);	

}

void gba_init_input(void)
{
	smartkey();

	if(inits != 2)
	{
		BIND("U!", 0x81, 4);
		BIND("D!", 0x82, 4);
		BIND("<!", 0x83, 4);
		BIND(">!", 0x84, 4);

		BIND("L!", 'y', 4);
		BIND("R!", 'n', 4);

		BIND0("LR!", GO_MODE1);
		BIND0("RL!", GO_MODE1);
		BIND0("U!", HISTORY_UP);
		BIND0("D!", HISTORY_DOWN);
		BIND0(">!", CURSOR_RIGHT);
		BIND0("<!", CURSOR_LEFT);

		BIND0("S!", 10);
		BIND0("RS!", 8);
		BIND0("LS!", ' ');
		BIND0("E!", SHOW_KEYBOARD);
		BIND0("RE!", COMPLETE);
		BIND0("L*", GET_DICT0);
		BIND0("R*", GET_DICT1);
		BIND0("A*", GET_DICT2);
		BIND0("B*", GET_DICT3);

		BIND("U!", MARKER_UP, 1);
		BIND("D!", MARKER_DOWN, 1);
		BIND(">!", MARKER_WORDRIGHT, 1);
		BIND("<!", MARKER_WORDLEFT, 1);
		BIND("A!", MARKER_INSERTBACK, 1);
		BIND("B!", MARKER_INSERT, 1);
		BIND("LR!", GO_MODE0, 1);
		BIND("RL!", GO_MODE0, 1);
		BIND("S!", 10, 1);

		BIND("L*", GET_DICT0,1);
		BIND("R*", GET_DICT1,1);
		BIND("A*", GET_DICT2,1);
		BIND("B*", GET_DICT3,1);
	}

	h_config |= (CONFIG_COLOUR|CONFIG_PROPORTIONAL|CONFIG_BOLDFACE);

	if ((h_version >= V4))
	   h_config |= CONFIG_TIMEDINPUT;
   
   if (h_version >= V5)
	   h_flags &= ~(MOUSE_FLAG | MENU_FLAG);
}
