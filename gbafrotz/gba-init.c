#include "pogo.h"
#include "frotz.h"

void gba_init_input(void);
void gba_init_output(void);

void Halt(void);

void os_init_screen(void)
{

	//h_flags &= ~UNDO_FLAG;
	h_interpreter_number = h_version == 6 ? INTERP_MSDOS : INTERP_DEC_20;
	h_interpreter_version = 'F';
	
	putchar(12);	
	gba_init_input();
	gba_init_output();
	//gba_init_pictures(graphics_filename);
}

int os_random_seed (void)
{
	return (clock() & 0x7fff);
}

void os_restart_game (int stage) {}

void os_fatal (const char *s)
{
	fprintf(stderr, "\nFatal error: %s\n", s);
	while(1);
}


int gcount = 0;
int games[30];

Font *fixed_font;

void Reset(void);
void set_dictword(int dict, int pos, char *str);

int curr_dict = 0;
int dict_pos = 0;

extern int def_fgcol;
extern int def_bgcol;
extern int def_fgrev;
extern int def_bgrev;

char normname[32] = "";
char fixname[32] = "";
char italname[32] = "";

Font *font = NULL;

zchar *story_rom_ptr = NULL;

void gba_bind(char *str);

int show_bmp(char *name)
{
	int i,c,l;
	uint16 bpp;
	uchar colors[3*256];
	uint32 start, size, width, height;
	int fd = open(name, 0);

	fprintf(stderr, "%s -> %d\n", name, fd);
	if(fd >= 0) {
		int sfd = open("/dev/screen", 0);
		uint16 *ptr, *p = (uint16 *)lseek(sfd, 0, SEEK_MEM);
		lseek(fd, 10, SEEK_SET);
		read(fd, &start, 4);
		read(fd, &size, 4);
		read(fd, &width, 4);
		read(fd, &height, 4);
		read(fd, &bpp, 2);
		read(fd, &bpp, 2);
		fprintf(stderr, "bmp %d bpp\n", bpp);
		if(bpp == 8)
		{
			lseek(fd, size+14, SEEK_SET);
			for(i=0; i<256; i++) {
				read(fd, &colors[i*3], 3);
				c = colors[i*3];
				colors[i*3] = colors[i*3+2];
				colors[i*3+2] = c;
				lseek(fd, 1, SEEK_CUR);
			}
			ioctl(sfd, SC_SETPAL, colors, 0, 256);
		}
		putchar(12);
		lseek(fd, start, SEEK_SET);

		if(bpp == 8)
		{
			ioctl(sfd, SC_SETMODE, 1);
			ptr = p+120*(height-1) + (120-width/2)/2;
			for(; ptr >= p; ptr -= 120) {
				read(fd, ptr, (width+3)&0xFFFFFFFC);
			}
		} else
		if(bpp == 16)
		{
			ioctl(sfd, SC_SETMODE, 2);
			ptr = p+240*(height-1) + (240-width/2)/2;
			for(; ptr >= p; ptr -= 240) {
				read(fd, ptr, 2*((width+3)&0xFFFFFFFC));
			}
		}
		else
		if(bpp == 24)
		{
			unsigned char cols[4];
			ioctl(sfd, SC_SETMODE, 2);
			l = 240*160;
			while(l--)
			{
				if(!(l%480))
					fprintf(stderr, "%x %x %x\n", cols[0], cols[1], cols[2]);
				read(fd, cols, 3);
				*p++ = ((cols[0]&0xf1)<<8) | ((cols[1]&0xf1)<<3) | ((cols[2]&0xf1)>>2);
			}
			fprintf(stderr, "done\n");
		}

		close(fd);
		close(sfd);
		return 1;
	}
	return 0;
}

void parse_config(char *name)
{
	char tmp[40];
	int fd = open(name, 0);
	if(fd < 0) {
		fd = open("/rom/default.cfg", 0);
	}
	if(fd >= 0) {
		int quit = 0;
		while(!quit)
		{
			*tmp = 0;
			fgets(tmp, sizeof(tmp), (FILE *)fd);
			if(!strlen(tmp))
				quit = 1;
			else
			if(strncmp(tmp, "bgcol=", 6) == 0)
				def_bgcol = atoi(&tmp[6]);
			else
			if(strncmp(tmp, "fgcol=", 6) == 0)
				def_fgcol = atoi(&tmp[6]);
			else
			if(strncmp(tmp, "bgrev=", 6) == 0)
				def_bgrev = atoi(&tmp[6]);
			else
			if(strncmp(tmp, "fgrev=", 6) == 0)
				def_fgrev = atoi(&tmp[6]);
			else
			if(strncmp(tmp, "font=", 5) == 0)
				strcpy(normname, &tmp[5]);
			else
			if(strncmp(tmp, "fixed=", 6) == 0)
				strcpy(fixname, &tmp[6]);
			else
			if(strncmp(tmp, "italic=", 6) == 0)
				strcpy(italname, &tmp[6]);
			else
			if(strncmp(tmp, "bind ", 5) == 0)
			{
				gba_bind(&tmp[5]);
			}
			else
			if(strncmp(tmp, "dict=", 5) == 0)
			{
				curr_dict = atoi(&tmp[5]);
				dict_pos = 0;
			}
			else
			if(tmp[0] == '\"')
			{
				char *p=&tmp[1];
				while(*p && *p != '\"')
					p++;
				*p = 0;
				set_dictword(curr_dict, dict_pos++, &tmp[1]);
			}
			else
				quit = 1;
		}
		close(fd);
	}
}

static char story[64];
char *ilist[32];
int items = 0;

int menu(char **items, int count)
{
	int y,selection,l,i;
	int rc = ioctl(1, CC_GETXY)&0xffff;
	printf("\0331f");
	for(i=0; i<count; i++)
	{
		printf("  %s\n", items[i]);
	}
	ioctl(0, IO_SETMODE, KM_RAW);
	y = selection = 0;
	while(!selection) {
		printf("\033[%d;%dH", y+rc, 0);
		putchar('>');
		while((l = getchar())== EOF) Halt();
		putchar(8);
		if(l == RAWKEY_DOWN)
			y++;
		if(l == RAWKEY_UP)
			y--;
		if(y < 0) y = count-1;
		if(y > count-1) y = 0;
		if(l == RAWKEY_A || l == RAWKEY_START)
			selection = y+1;
		if(l == RAWKEY_B)
			selection = -1;
	}
	printf("\0330f");
	printf("\033[%d;%dH", count+rc, 0);
	return selection-1;
}

/* Find story files in ROM and presents a menu.
   Handles intro screens and config files.
   */
int gba_mainmenu(int argc, char **argv)
{
	struct stat sbuf;
	int selection = 0;
	int count, i, fd, l;
	int c;
	char *p, *name;
	DIR *dir;
	struct dirent *de;
	int done = 0;
	char current[128];

	printf("%c[%d;%dm", 27, 40, 37);
	printf("\0331f");
	putchar(12);

	ioctl(0, IO_SETMODE, KM_RAW);
	while(getchar() != EOF) Halt();

	if(argc == 2)
	{
		strcpy(current, argv[1]);
	}
	else
	{
		strcpy(current, "");
		while(!done)
		{
			putchar(12);
			printf("%c[%d;%dm", 27, 42, 37);
			printf("\n   GBAFrotz v1.18 (2.42) ported by Sasq / DCS    \n\n");
			printf("%c[%d;%dm", 27, 40, 37);
			dir = opendir(current);
			if(dir)
			{
				while((de = readdir(dir)))
				{
					p = &current[strlen(current)];
					*p = '/';
					strcpy(p+1, de->d_name);
					stat(current, &sbuf);
					*p = 0;
					if(sbuf.st_mode & S_IFDIR)
					{
						//printf("[%s]\n", de->d_name);
						ilist[items] = malloc(strlen(de->d_name)+3);
						sprintf(ilist[items++], "[%s]", de->d_name);
					}
					else
					{
						p = strrchr(de->d_name, '.');
						if(p && (strcmp(p, ".zcode") == 0))
						{
							int l = strlen(de->d_name);
							//*p = 0;
							//printf("%s\n", de->d_name);
							ilist[items] = malloc(l+1);
							strcpy(ilist[items], de->d_name);
							ilist[items++][l-6] = 0;
						}
						else
						{
							_dprintf("ignoring %s\n", de->d_name);
						}
					}

				}
				closedir(dir);
				i = menu(ilist, items);
				items = 0;
				if(i < 0)
				{
					char *p = strrchr(current, '/');
					if(p)
						*p = 0;
				}
				else
				if(ilist[i][0] == '[')
				{
					strcat(current, "/");
					strcat(current, &ilist[i][1]);
					current[strlen(current)-1] = 0;
				} else
					done = 1;
			}
			else
				done = 1;

		}	
		strcat(current, "/");
		strcat(current, ilist[i]);
		strcat(current, ".zcode");
	}

	while(getchar() != EOF)
		Halt();

	i = strlen(current);

	/* Show BMP */
	strcpy(&current[i-6], ".bmp");
	if(show_bmp(current))
		while(getchar() == EOF) Halt();

	/* Parse config files */
	strcpy(&current[i-6], ".cfg");
	parse_config(current);

	if((l = strlen(italname)))
	{
		l--;
		while(italname[l] == 10 || italname[l] == 13)
			italname[l--] = 0;
		font = font_load(italname);
		if(font)
			ioctl(1, CC_SETFONT, font, 3);
	}
	if((l = strlen(fixname)))
	{	
		l--;
		while(fixname[l] == 10 || fixname[l] == 13)
			fixname[l--] = 0;
		font = font_load(fixname);
		if(font)
			ioctl(1, CC_SETFONT, font, 1);
	}
	if((l = strlen(normname)))
	{
		l--;
		while(normname[l] == 10 || normname[l] == 13)
			normname[l--] = 0;
		font = font_load(normname);
		if(font)
			ioctl(1, CC_SETFONT, font, 0);
	}
	ioctl(1, CC_SETFONT, NULL, 0);

	strcpy(&current[i-6], ".zcode");
	strcpy(story, current);
	story_name = story;
	printf("\0330f");
	fd = open(story_name, 0);
 	story_rom_ptr = (zchar *)lseek(fd, 0, SEEK_MEM);
	_dprintf("open(\"%s\") => %d %p\n", story_name, fd, story_rom_ptr);
	close(fd);

	return 0;
}

FILE *os_path_open(const char *name, const char *mode)
{
	return fopen(name, mode);
}

void os_process_arguments (int argc, char *argv[])
{
	char *vram = (char *)0x06000000;
	char tmp[32];
	int fd,i;
	Font *font;

	filesys_init();

	/* Only init devices we need */
	deb_init();
	screen_init();
	vkey_init();
	console_init();
	sram_init();

	/* Try to set keyboard image */
	fd = open("/rom/keyboard.bm", 0);
	if(fd < 0)
		fd = open("/rom/.keyboard.bm", 0);
	if(fd < 0)
		fd = open("/rom/.shell/keyboard.bm", 0);
	if(fd >= 0)
	{
		uint16 *ptr = (uint16*)lseek(fd, 0, SEEK_MEM);
		ioctl(0, KC_SETIMAGE, ptr);
		close(fd);
	}


	/* Set console fonts */
	font = font_load("fonts/fixed5.font");
	ioctl(1, CC_SETFONT, font, 1);

	font = font_load("fonts/fixed5.font");
	ioctl(1, CC_SETFONT, font, 2);

	font = font_load("fonts/fixed5.font");
	ioctl(1, CC_SETFONT, font, 3);

	font = font_load("fonts/nokia.font");
	ioctl(1, CC_SETFONT, font, 0);

	option_save_quetzal = 0;
	gba_mainmenu(argc, argv);
}
