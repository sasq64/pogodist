# Microsoft Developer Studio Project File - Name="gbafrotz" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=gbafrotz - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "gbafrotz.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "gbafrotz.mak" CFG="gbafrotz - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "gbafrotz - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "gbafrotz - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "gbafrotz - Win32 Release"

# PROP BASE Use_MFC
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f gbafrotz.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "gbafrotz.exe"
# PROP BASE Bsc_Name "gbafrotz.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "NMAKE /f gbafrotz.mak"
# PROP Rebuild_Opt "/a"
# PROP Target_File "gbafrotz.exe"
# PROP Bsc_Name "gbafrotz.bsc"
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "gbafrotz - Win32 Debug"

# PROP BASE Use_MFC
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f gbafrotz.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "gbafrotz.exe"
# PROP BASE Bsc_Name "gbafrotz.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "make"
# PROP Rebuild_Opt "rebuild"
# PROP Target_File "gbafrotz.exe"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "gbafrotz - Win32 Release"
# Name "gbafrotz - Win32 Debug"

!IF  "$(CFG)" == "gbafrotz - Win32 Release"

!ELSEIF  "$(CFG)" == "gbafrotz - Win32 Debug"

!ENDIF 

# Begin Group "Source"

# PROP Default_Filter "C"
# Begin Source File

SOURCE=.\buffer.c
# End Source File
# Begin Source File

SOURCE=.\core_init.c
# End Source File
# Begin Source File

SOURCE=.\err.c
# End Source File
# Begin Source File

SOURCE=.\fastmem.c
# End Source File
# Begin Source File

SOURCE=.\files.c
# End Source File
# Begin Source File

SOURCE=".\gba-init.c"
# End Source File
# Begin Source File

SOURCE=".\gba-input.c"
# End Source File
# Begin Source File

SOURCE=".\gba-output.c"
# End Source File
# Begin Source File

SOURCE=".\gba-pic.c"
# End Source File
# Begin Source File

SOURCE=".\gba-stubs.c"
# End Source File
# Begin Source File

SOURCE=.\getopt.c
# End Source File
# Begin Source File

SOURCE=.\hotkey.c
# End Source File
# Begin Source File

SOURCE=.\input.c
# End Source File
# Begin Source File

SOURCE=.\main.c
# End Source File
# Begin Source File

SOURCE=.\math.c
# End Source File
# Begin Source File

SOURCE=.\object.c
# End Source File
# Begin Source File

SOURCE=.\process.c
# End Source File
# Begin Source File

SOURCE=.\quetzal.c
# End Source File
# Begin Source File

SOURCE=.\random.c
# End Source File
# Begin Source File

SOURCE=.\redirect.c
# End Source File
# Begin Source File

SOURCE=.\screen.c
# End Source File
# Begin Source File

SOURCE=.\sound.c
# End Source File
# Begin Source File

SOURCE=.\stream.c
# End Source File
# Begin Source File

SOURCE=.\table.c
# End Source File
# Begin Source File

SOURCE=.\text.c
# End Source File
# Begin Source File

SOURCE=.\variable.c
# End Source File
# End Group
# Begin Group "Include"

# PROP Default_Filter "H"
# Begin Source File

SOURCE=.\frotz.h
# End Source File
# Begin Source File

SOURCE=.\getopt.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\crt0.S
# End Source File
# Begin Source File

SOURCE=.\lnkscript
# End Source File
# Begin Source File

SOURCE=.\Makefile
# End Source File
# End Target
# End Project
