#include "agbTypes.h"

#define CLOCK_DEVICE	0x080000C8
#define CLOCK_CONTROL	0x080000C6
#define CLOCK_DATA		0x080000C4
#define	CLOCK_CSOUT		0x4
#define	CLOCK_SIOOUT	0x2
#define	CLOCK_SCKOUT	0x1
#define CLOCK_SCK		0x1
#define CLOCK_SIO		0x2
#define CLOCK_CS 		0x4		

#define		SETW(adr, val) (*((volatile u16*)adr) = val)

typedef struct _Time
{
	u8 year;
	u8 month;
	u8 data;
	u8 week;
	u8 hour;
	u8 minute;
	u8 second;
	u8 res;
}CLOCK_TIME;

//get the time from chip , has been changed to total time :
void		GetTime(CLOCK_TIME *cs) ;
void		SetTime(CLOCK_TIME *cs) ;
//get  the time from chip . data not toutched 
//**************************************************
//sprintf(time,"20%d%d.%d%d.%d%d %d%d:%d%d:%d%d",
//	(clocktime.year>>4),(clocktime.year&0xF),
//	(clocktime.month>>4),(clocktime.month&0xF),
//	(clocktime.data>>4),(clocktime.data&0xF),
//	(clocktime.hour>>4),(clocktime.hour&0xF),
//	(clocktime.minute>>4),(clocktime.minute&0xF),
//	(clocktime.second>>4),(clocktime.second&0xF));
void 		GetTime_Orignal(CLOCK_TIME *cs);
void		Set24Hour();
//must enable befor get time
void 		Clock_Enable();
void 		Clock_Disable();

//enable write
void		OpenWrite();
void		CloseWrite();
