#ifndef __INRAM_H
#define __INRAM_H

#include "agb.h"

void SetRampage(u16 page);
void SetRompage(u16 page);
void SetRompageWithSoftReset(u16 page);
void SetRompageWithHardReset(u16 page);
void SoftRest();
void HardRest();

//����
//void AATEST() ;

#endif
