/*fat16.h	define some structure of fat16*/
#ifndef __FAT16_H
#define __FAT16_H

#ifndef BYTE
typedef unsigned char BYTE;
#endif

#ifndef WORD
typedef unsigned short WORD;
#endif

#ifndef DWORD
typedef unsigned long DWORD;
#endif

struct _BPB
{
	BYTE NumFATs;	//The count of FAT data structures on the volume.

	BYTE SecPerClus;	//Number of sectors per allocation unit.

	WORD BytsPerSec;	//Count of bytes per sector.

	WORD RsvdSecCnt;	//Number of reserved sectors in the Reserved region of the volume starting at the first sector of the volume.

	WORD RootEntCnt;	//this field contains the count of 32-byte directory entries in the root directory.

	DWORD TotSec;	//This field is the total count of sectors on the volume.

	DWORD FATSz;	//This field is the FAT12/FAT16 16-bit count of sectors occupied by ONE FAT.
};

#define ERCHAR	0xe5

struct _DIR
{
	BYTE Name[11];
	BYTE Attr;
	BYTE NTRes;
	BYTE CrtTimeTenth;
	WORD CrtTime;
	WORD CrtDate;
	WORD LstAccDate;
	WORD FstClusHI;
	WORD WrtTime;
	WORD WrtDate;
	WORD FstClusLO;
	DWORD FileSize;
};

#define ATTR_READ_ONLY	0x01
#define ATTR_HIDDEN		0x02
#define ATTR_SYSTEM		0x04
#define ATTR_VOLUME_ID	0x08
#define ATTR_DIRECTORY	0x10
#define ATTR_ARCHIVE	0x20

//The sector number of the first sector of that cluster.
//FirstSectorofCluster = ((N – 2) * BPB_SecPerClus) + FirstDataSector;
#define FirstSectorofCluster(N)	(((N – 2) * Bpb.SecPerClus) + FirstDataSector)

//
//FAT16 Apis
//

struct _stat
{
	BYTE Attr;
	BYTE CrtTimeTenth;
	WORD CrtTime;
	WORD CrtDate;
	WORD LstAccDate;
	WORD WrtTime;
	WORD WrtDate;
	DWORD FileSize;
};

struct _file
{
	int valid; // 1 valid, 0 free.

	DWORD DirSectorNum;
	int DirIndex;

	DWORD StartSectorNum;
	DWORD CurrentSectorNum;
	DWORD SectorOffset;

	struct _DIR dir;

	unsigned long offset;
};

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

struct FatDate
{
	WORD Day : 5;
	WORD Month : 4;
	WORD Year : 7;
};

struct FatTime
{
	WORD Second_2s : 5;
	WORD Minutes : 6;
	WORD Hours : 5;
};

struct FatDateTime
{
	union
	{
		struct FatDate fatdate;
		WORD Date;
	}Date;

	union
	{
		struct FatTime fattime;
		WORD Time;
	}Time;

	BYTE TimeTenth;
};

struct FatGet
{
	DWORD DirSectorNum;
	int DirIndex;

	int IsRootDir;

	char filename[13];
};

extern struct _file handles[16];
extern struct FatGet fat_get;
////////////////////////////////////////////////////////////////////////////////
//most important thing only 8+3 is supported
///////////////////////////////////////////////////////////////////////////////

//int fat_init();
//useEram is a indicator that use whitch ram .
//useEram = 1  , use the 0x2000000 external ram for fat table and data. 
//		so you should not use the external ram.
//useEram = 0 , use the psram 0x9000000. 2Mbit totally .
int fat_init(int useEram);
// fat_deinit will write back to nand file system .so you need not call it if you do nothing just read .
void fat_deinit();


int fat_mkdir( const char *dirname );
int fat_rmdir( const char *dirname );
//***********************************************************
//*get first 
//path : path name of you want to get file , begin with '\' , such as "\\files"
//file name is the output name .
int fat_getfirst(const char *path, char* filename);
int fat_getnext(char* filename);

//samething as the fat_deinit . but here  add one fat_close_withouwrit.
int fat_close_withouwrit(int handle);
int fat_close(int handle);

// wrong when return blow zero .
// file name must contains the path name such as "\\files\\readme.txt"
int fat_creat(const char* filename, BYTE attribute);
int fat_open(const char* filename);

long fat_lseek(int handle, long offset, int origin);
unsigned int fat_read(int handle, void* buffer, unsigned int bytes);
unsigned int fat_write(int handle, const char* buffer, unsigned int bytes);

int fat_remove( const char *filename);
int fat_get_stat( const char *filename, struct _stat* stat);
int fat_set_stat( const char *filename, struct _stat* stat);
int fat_rename( const char *oldname, const char *newname );
/*
//长文件名函数
wchar_t *vfat_unistrchr(const wchar_t *s, const wchar_t c);
int vfat_is_used_badchars(const wchar_t *s, int len);
int vfat_valid_longname(const unsigned char *name, unsigned int len);
*/

#endif //__FAT16_H
