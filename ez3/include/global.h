
#ifndef GLOBAL_H
#define GLOBAL_H

//定义的Ez3的一些配置。
#define	_Ez3PsRAM 			0x08400000
#define	_Ez3NorList			0x083c0000
#define _Ez3NorRom			0x09400000
#define _Ez3NorRomEnd		0x09C00000
#define	_UnusedVram 		0x06012c00
#define	_UnusedVramSize 	0x00005400
#define _UnusedEram			0x02013c00
#define _ramFatCache		0x02000000
#define _ramFatBlockCache	0x02020000
#define _psFatCache			(_Ez3PsRAM+0xC00000)
#define _psFatBlockCache	(_Ez3PsRAM+0xC20000)


#define MAXDISKNAME			32
#define _GoldenEnable		0x0E000400
#define _GoldenSaver		0x0E000200
#define _FileInfoSaver		0x0E000100
#define _SRAMSaver			0x0E000000

//
#define GAMEPAKMem	   (u8*) 0x8000000
#define GAMEPAKRES	GAMEPAKMem  //图像资源位置
#define GBAPAKLINE	(GAMEPAKMem + 200*1024)  //字库和文件资源位置

#define SAVEPOINT	0xE000000				//存盘点
#define SAVEHEADVAL	0x1616
#define SAVEHEAD	SAVEPOINT
#define SAVEISSET	(SAVEPOINT+4)
#define SAVETXTPOS	(SAVEPOINT+8)

#define WORDSAVEPOINT	0x800				//单词学习存盘点

#endif



