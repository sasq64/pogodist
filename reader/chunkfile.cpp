#ifdef GBA

#include <pogo.h>

#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#endif

#include "chunkfile.h"

ChunkFile::ChunkFile(const char *name, int flags)
{
	m_write = (flags == WRITE);
	
	if(m_write)
		m_fp = fopen(name, "wb");
	else
		m_fp = fopen(name, "rb");
	
	m_chunkPos = &m_chunkStack[0];
	
	m_chunkName[4] = 0;
	
}

bool ChunkFile::OpenChunk(const char *chunk)
{
	m_chunkPos++;
	
	if(m_write)
	{
		int dummy = -1;
		fwrite(chunk, 1, 4, m_fp);
		m_chunkPos->pos = ftell(m_fp);
		m_chunkPos->size = 0;
		fwrite(&dummy, 1, 4, m_fp);
		
		return true;
	}
	else
	{
		int rc;
		int offset = 0;
		do
		{
			fseek(m_fp, offset, SEEK_CUR);
			
			rc = fread(m_chunkName, 1, 4, m_fp);
			rc += fread(&m_chunkSize, 1, 4, m_fp);
			if(rc != 8)
				return false;
			
			offset = m_chunkSize;
		}
		while(chunk && (strcmp(chunk, m_chunkName) != 0));
		
		m_chunkStart = ftell(m_fp);
		m_chunkPos->pos  = m_chunkStart + offset;

		return true;
	}
}

int ChunkFile::Seek(int offset)
{
	if(offset > m_chunkSize)
		offset = m_chunkSize;

	return fseek(m_fp, m_chunkStart + offset, SEEK_SET) - m_chunkStart;
}

int ChunkFile::Tell()
{
	return ftell(m_fp) - m_chunkStart;
}

void ChunkFile::CloseChunk()
{
	if(m_write)
	{
		int pos = ftell(m_fp);
		int size = pos - m_chunkPos->pos - 4;
		fseek(m_fp, m_chunkPos->pos, SEEK_SET);
		fwrite(&size, 1, 4, m_fp);
		fseek(m_fp, pos, SEEK_SET);
	}
	else
	{
		fseek(m_fp, m_chunkPos->pos, SEEK_SET);
	}

	m_chunkPos--;

}

unsigned int ChunkFile::ReadInt()
{
	unsigned int t;
	fread(&t, 1, 4, m_fp);
	return t;
}

int ChunkFile::Read(char *dest, int size)
{
	if(!size)
		size = m_chunkSize;
	
	int pos = ftell(m_fp);
	int end = m_chunkStart + m_chunkSize;
	if(pos + size > end)
		size = end - pos;
	
	if(size < 0)
		return 0;
	
	return fread(dest, 1, size, m_fp);
	
}

char *ChunkFile::ReadLine(char *dest, int size)
{
	int end = m_chunkStart + m_chunkSize;

	int start = ftell(m_fp);
	
	if(start >= end)
		return 0;
	
	char *rc = fgets(dest, size, m_fp);
	
	int pos = ftell(m_fp);
	
	if(pos > end)
	{
		fseek(m_fp, end, SEEK_SET);		
		dest[size - (end-pos)] = 0;
	}
	
	return rc;
}

/*
	int pos = m_file->Tell();
	int end = pos - m_start + size;
	
	if( end > m_size)
		size -= (end - m_size);
	
	if(size <= 0)
		return NULL;
*/
	
int ChunkFile::Write(unsigned int v)
{
	unsigned int t = v;
	return fwrite(&t, 1, 4, m_fp) / sizeof(unsigned int);
}

int ChunkFile::Write(const char *str)
{
	return fwrite(str, 1, strlen(str)+1, m_fp);
}

void ChunkFile::Close()
{
	if(m_fp)
	{
		while(m_chunkPos != &m_chunkStack[0])
			CloseChunk();
		fclose(m_fp);
		m_fp = NULL;
	}
}

ChunkFile::~ChunkFile()
{
	Close();
}
