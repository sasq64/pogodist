
class TextBar
{
public:
	TextBar(char *text, Display *display, PogoFont **fonts, int y);
	void SetSource(char letter, char *string);
	void SetSource(char letter, int number);
	void Render();
private:

	bool Parse(char *dest, int section);
	
	enum {STRING, NUMBER};
	
	struct Source
	{
		char *string;
		int number;
		char letter;
		char type;
	};
	
	Source m_source[32];
	int m_sourceCount;
	Display *m_display;
	int m_y;
	char *m_text;
	
	PogoFont **m_fonts;
	
};
