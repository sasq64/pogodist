
#include "types.h"

//#define SCREEN_WIDTH 240
//#define SCREEN_HEIGHT 160
//#define XY(x,y) ((y)*240+(x))
//#define SCREENBASE ((uint16*)0x06000000)


int g_screenWidth;
int g_screenHeight;
void *g_screenPtr;

#define SCREEN_WIDTH g_screenWidth
#define SCREEN_HEIGHT g_screenHeight

#ifdef GBA

#define DIRECT
typedef uchar Pixel;

#else

typedef uint16 Pixel;

#endif


// NORMAL (FROM TOP-LEFT)
#define XY(x,y) ((y)*g_screenWidth+(x))
#define SCREENBASE ((Pixel*)g_screenPtr)

// FROM BOTTOM-RIGHT
//#define XY(x,y) (-(y)*g_screenWidth-(x))
//#define SCREENBASE (((uint16*)g_screenPtr)+g_screenWidth*g_screenHeight-1)

// FROM BOTTOM-LEFT
//#define XY(x,y) ((y) - (x) * g_screenWidth)
//#define SCREENBASE (((uint16*)g_screenPtr)+g_screenWidth*(g_screenHeight-1))

// FROM TOP-RIGHT
//#define XY(x,y) ((x) * g_screenWidth - (y))
//#define SCREENBASE (((uint16*)g_screenPtr)+g_screenWidth-1)


#define MAX_COLOR 256

//#if (XY(0,1) - XY(0,0)) == 1
#define LINEAR_X
//#endif

#ifdef GBA

#define UINT2PIXEL(x) (((x>>19)&0x1F)<<10) | (((x>>11)&0x3F)<<5) | ((x>>3)&0x1F)

#else

#define UINT2PIXEL(x) (((x>>19)&0x1F)<<11) | (((x>>10)&0x3F)<<5) | ((x>>3)&0x1F)

#endif

#ifdef DIRECT
#define COPY_PIXEL(dst, src) *(dst) = *(src)
#else
#define COPY_PIXEL(dst, src) *(dst) = colors[*(src)]
#endif


static Pixel colors[MAX_COLOR];

int screen_setcolors(unsigned int *pal, int start, int size)
{
	int i;
	unsigned int col;
	
	if((start+size) > MAX_COLOR)
		size = (MAX_COLOR - start);
	
	for(i=0; i<size; i++)
	{
		col = pal[i];
		colors[i+start] = UINT2PIXEL(col);
	}
	
	/*for(i=0; i<256; i++)
	{
		int j = 255-i;
		col = (j | j<<8 | j<<16);
		colors[i] = UINT2PIXEL(col);
	}*/
	
	return size;
}

void screen_copy(int x, int y, uchar *src, int width, int height, int sw, int solid)
{
	int w;

	Pixel *dst = SCREENBASE + XY(x,y);

	int smod = (sw - width);
	int dmod = XY(-width, 1);

	while(height--)
	{
		w = width;
		if(solid)
		{
			while(w--)
#ifdef LINEAR_X
			COPY_PIXEL(dst++, src++);
#else
			{
				COPY_PIXEL(dst, src++);
				dst += XY(1,0);
			}
#endif
		}
		else
		{
			while(w--)
			{
				if(*src)//;// < *dst)
					COPY_PIXEL(dst, src);
					//*dst = colors[*src];
#ifdef LINEAR_X
				dst++;
#else
				dst += XY(1,0);
#endif
				src++;
			}
		}
		dst += dmod;
		src += smod;
	}
}


void screen_set(int x, int y, int width, int height, int col)
{
	int w;
	
#ifdef DIRECT
	Pixel color = col;
#else
	Pixel color = colors[col];
#endif
	
	Pixel *dst = SCREENBASE + XY(x,y);
	
	int dmod = XY(-width, 1);

	while(height--)
	{
		w = width;
		while(w--)
#ifdef LINEAR_X
			*dst++ = color;
#else
			{
				*dst = color;
				dst += XY(1,0);
			}
#endif
		dst += dmod;
	}
}
