#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL.h>

//#include <SDL_ttf.h>

#include "types.h"
#include "pogofont.h"

#include "display.h"

extern "C" {

	void screen_set(int x, int y, int width, int height, int col);

	extern int g_screenWidth;
	extern int g_screenHeight;
	extern void *g_screenPtr;

};

Display::Display(int w, int h)
{
	int bpp = 16;
	int video_flags = 0;
	
	//m_fontSize = 0;
	
	//m_currentState = 0;

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_NOPARACHUTE) < 0)
	{
		fprintf(stderr,"Couldn't initialize SDL: %s\n",SDL_GetError());
		exit(1);
	}

	video_flags = SDL_RESIZABLE;//SDL_HWSURFACE | SDL_DOUBLEBUF;

	if((m_screen = (void *)SDL_SetVideoMode(w, h, bpp, video_flags)) == NULL)
	{
		fprintf(stderr, "Couldn't set mode: %s\n", SDL_GetError());
		SDL_Quit();
		exit(1);
	}
	
	g_screenWidth = m_width = w;
	g_screenHeight = m_height = h;

	SDL_WM_SetCaption("Reader v1.0", "reader.icon");
}

void Display::Lock()
{
	SDL_LockSurface((SDL_Surface*)m_screen);
	g_screenPtr = (uint16 *)((SDL_Surface*)m_screen)->pixels;
}

void Display::Unlock()
{
	SDL_UnlockSurface((SDL_Surface*)m_screen);
	SDL_UpdateRect((SDL_Surface*)m_screen, 0, 0, m_width, m_height);
}
	
int Display::Text(int x, int y, char *text, PogoFont **fonts, int *font)
{
	//Lock();
	int rc = pogofont_text_multi(fonts, font, text, x, y);
	//Unlock();
	return rc;
}

void Display::Fill(int col, int x, int y, int w, int h)
{
	
	if(w == -1)
		w = m_width;
	if(h == -1)
		h = m_height;
	
	screen_set(x, y, w, h, col);
}

static bool resize = false;
int new_width;
int new_height;

int Display::WaitEvent()
{
	SDL_Event event;

	if(resize)
	{
		int bpp = 16;
		int video_flags = SDL_RESIZABLE;

		if((m_screen = (void *)SDL_SetVideoMode(new_width, new_height, bpp, video_flags)) == NULL)
		{
			fprintf(stderr, "Couldn't set mode: %s\n", SDL_GetError());
			SDL_Quit();
			exit(1);
		}
		g_screenWidth = m_width = new_width;
		g_screenHeight = m_height = new_height;
		resize = false;
		return 9;
	}

	SDL_PumpEvents();
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
		case SDL_VIDEORESIZE:
			new_width = event.resize.w & 0xFFFFFFFC;
			new_height = event.resize.h;
			resize = true;
			break;
		case SDL_KEYUP:
			//keys[event.key.keysym.sym] = 0;
			break;
			
		case SDL_KEYDOWN:
			//keys[event.key.keysym.sym] = 1;
			//evt.type = BUTTON;
			switch(event.key.keysym.sym)
			{
			case SDLK_DOWN:
				return 1;
				break;
			case SDLK_UP:
				return 2;
				break;
			case SDLK_LEFT:
			case SDLK_BACKSPACE:
				return 4;
				break;
			case SDLK_RIGHT:
			case SDLK_SPACE:
				return 3;
				break;				
			case SDLK_END:
				return 5;
				break;
			case SDLK_HOME:
				return 6;
				break;


		
			case SDLK_ESCAPE:
			case SDLK_q:
				SDL_Quit();
				return 0;
			default:
				break;
			}
		}
	}
	return -1;
}
