#ifndef DISPLAY_H
#define DISPLAY_H

#include "pogofont.h"
#include "types.h"

class Display
{
public:
	Display(int w, int h);
	void Lock();
	void Unlock();
	int Text(int x, int y, char *text, PogoFont **fonts, int *font);
	void Fill(int col = 0, int x = 0, int y = 0, int w = -1, int h = -1);
	int WaitEvent();

	inline int Width() const { return m_width; }
	inline int Height() const { return m_height; }
	
private:
	void *m_screen;
	int m_width;
	int m_height;
};

#endif
