#ifdef GBA

#include <pogo.h>

#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"

#endif

//#define ASSERT(x) (x)

#include "types.h"
#include "util.h"
#include "display.h"
#include "itextstream.h"
#include "textrenderer.h"

#include "bookformat.h"

int TextRenderer::TextLength(char *text, int *rcfont)
{
	PogoFont **fonts = state->m_currentFontSet;
	int font = state->m_currentFont;
	
	if(!rcfont)
		rcfont = &font;
	return pogofont_text_multi(fonts, rcfont, text, 0, -1);
}

int TextRenderer::Text(int x, int y, char *text)
{
	return pogofont_text_multi(state->m_currentFontSet, &state->m_currentFont, text, x, y);
}

int TextRenderer::TextHeight(char *text)
{
	unsigned char *ptr = (unsigned char *)text;
	
	PogoFont **fonts = state->m_currentFontSet;
	int font = state->m_currentFont;
		
	int height =  fonts[font]->height;
	
	if(*ptr <= 8)
		height = 0;
	
	while(*ptr)
	{
		if(*ptr <= 8)
		{
			int h = fonts[*ptr-1]->height;
			if(h > height)
				height > h;
		}
		ptr++;
	}

	return height;
}

bool verify = false;

bool TextRenderer::LoadIndex(char *fname)
{
	int i;	
	char tmp[128];
	char name[32];
	int width, height, linespace,avgsize;
	

	FILE *fp = fopen(fname, "rb");
	if(fp)
	{
		for(i=0; i<12; i++)
		{
			ReadString(fp, name);
			if(!m_fonts[i])
			{
				if(!strlen(name))
					continue;
				else
				{
					fprintf(stderr, "empty font\n");
					fclose(fp);
					return false;
				}
			}
			if(strcmp(name, m_fonts[i]->name) != 0)
			{
				fprintf(stderr, "font mismatch\n");
				fclose(fp);
				return false;
			}
		}
		
		width = ReadInt(fp);
		height = ReadInt(fp);
		linespace = ReadInt(fp);
		
		//fprintf(stderr, "dimensions %d %d %d\n", width, height, linespace);

		if(width != (m_width - m_margins[LEFT] - m_margins[RIGHT]) ||
		   height != (m_height - m_margins[TOP] - m_margins[BOTTOM]) ||
		   linespace != m_lineSpace)
		{
			printf("dimensions failed\n", width, height, linespace);
			
			fclose(fp);
			return false;
		}
		
		m_page = ReadInt(fp);
		m_pageCount = ReadInt(fp);

		m_chapterCount = ReadInt(fp);
		for(i=0; i<m_chapterCount; i++)
			m_chapters[i] = ReadInt(fp);

		if(m_pageOffsets)
			free(m_pageOffsets);
		m_allocCount = m_pageCount+2;
		m_pageOffsets = (unsigned int *)malloc(m_allocCount * sizeof(int));
		
		uint32 style = 0;
		uint32 val = 0;
		
		m_pageOffsets[0] = val;
		
		for(i=1; i<m_pageCount; i++)
		{
			uint32 c = ReadWord(fp);
			if((c & 0xFF00) == 0xFF00)
			{			
				//printf("New style (%x)\n", c&0xFF);
				style = (c << 24);				
				c = ReadWord(fp);
			}
			val = val + c;
			
			/*if(verify)
			{
				if(m_pageOffsets[i] != (style | val))
				{
					printf("%d: saved %08x, loaded %08x\n", i, m_pageOffsets[i], (style | val));
				}
			}
			else*/
				m_pageOffsets[i] = (style | val);
		}
				
		fclose(fp);
		m_done = true;
		
		return true;
	}
	else
		fprintf(stderr, "%s not found\n", fname);
	
	return false;

}

bool TextRenderer::SaveIndex(char *fname)
{
	int i;
	char name[32];
	int width, height, avgsize;


	FILE *fp = fopen(fname, "wb");
	if(fp)
	{
		for(i=0; i<12; i++)
			WriteString(fp, m_fonts[i] ? m_fonts[i]->name : "");
		
		int width = (m_width - m_margins[LEFT] - m_margins[RIGHT]);
		int height = (m_height - m_margins[TOP] - m_margins[BOTTOM]);
		
		WriteInt(fp, width);
		WriteInt(fp, height);
		WriteInt(fp, m_lineSpace);

		WriteInt(fp, m_page);
		WriteInt(fp, m_pageCount);
		
		//fprintf(stderr, "SAVE %d %d %d %d %d\n", width, height, m_lineSpace, m_page, m_pageCount);
	
		uint32 style = 0;
		uint32 lastl = m_pageOffsets[0];

		WriteInt(fp, m_chapterCount);
		for(i=0; i<m_chapterCount; i++)
			WriteInt(fp, m_chapters[i]);

		for(i=1; i<m_pageCount; i++)
		{
			uint32 l = m_pageOffsets[i];
			
			if((l & 0xFF000000) != style)
			{
				//printf("%d: Big %d (%x)\n", i, l, l);
				style = l & 0xFF000000;
				WriteWord(fp, 0xFF00 | (style>>24));
			}
			l &= 0x00FFFFFF;
			WriteWord(fp, l-lastl);	
			lastl = l;
		}
		
		fclose(fp);
	}
	
	//verify = true;
	//LoadIndex(fname);
	//verify = false;
	return true;
}

int TextRenderer::GetChapterPage(int c)
{
	if((c < 1) || (c > m_chapterCount))
		return -1;
	else
		return m_chapters[c-1];
}

int TextRenderer::GetCurrentChapter()
{
	int i;
	for(i=0; i<m_chapterCount; i++)
		if(m_page < m_chapters[i])
			break;
	return i;
}


void TextRenderer::SetMargins(int left, int top, int right, int bottom)
{
	m_margins[LEFT] = left;
	m_margins[TOP] = top;
	
	if(right != -1)
		m_margins[RIGHT] = right;
	else
		m_margins[RIGHT] = left;

	if(bottom != -1)
		m_margins[BOTTOM] = bottom;
	else
		m_margins[BOTTOM] = top;
}


TextRenderer::TextRenderer(Display *display, ITextStream *stream, PogoFont **fonts) :
m_pageCount(1),
m_allocCount(32),
m_done(false),
//m_statusBar(0),
//m_titleBar(0),
m_fgCol(0x000000),
m_bgCol(0xFFFFFF),
m_defaultAlign(LEFT),
m_lineSpace(100)
{
	m_width = display->Width(); //page_width;
	m_height = display->Height(); //page_height;
	m_display = display;
	//m_font = font;
	m_stream = stream;
		
	m_page = 1;
	
	m_chapterCount = 0;
	m_bookCount = 0;	

	//m_savePath="";	
	
	m_pageOffsets = (unsigned int *)malloc(m_allocCount * sizeof(int));	
	m_pageOffsets[0] = 0;
	
	m_margins[LEFT] = m_margins[RIGHT] = 0;
	m_margins[TOP] = m_margins[BOTTOM] = 0;
	
	m_fonts = fonts;

	renderState.m_currentFontSet = &m_fonts[0];
	renderState.m_currentFont = 0;
	renderState.m_paragraphAlign = LEFT;
	renderState.m_indent = 0;

	processState.m_currentFontSet = &m_fonts[0];
	processState.m_currentFont = 0;
	processState.m_paragraphAlign = LEFT;
	processState.m_indent = 0;

	pogofont_setcolor(m_fgCol, m_bgCol);
}

void TextRenderer::SetFontSet(int n)
{
	
	state->m_currentFontSet = &m_fonts[n*4];
}

int TextRenderer::GetFontSet()
{
	return (state->m_currentFontSet - m_fonts) / 4;
}

void TextRenderer::SetState(int page, TextState *state)
{
	unsigned int l = m_pageOffsets[page-1];
	
	state->m_currentFont = (l>>28)&0x03;
	
	state->m_indent = (l>>30)&0x3;
	state->m_paragraphAlign = (l>>26)&0x3;
	state->m_currentFontSet = &m_fonts[(l>>24)&0x03];
}

void TextRenderer::PushWord(char *word, int len)
{
	m_lineWLenghts[m_lineWCount] = len;
	m_lineWords[m_lineWCount] = word;
	m_lineLength += len;
	m_lineWCount++;
	
	ASSERT(m_lineWCount < 128);
}

void TextRenderer::RenderLine()
{
	if(!m_lineWCount)
		return;
	int i;
	int x = 0;
	int y = m_y;
	int align = state->m_paragraphAlign;

	int space_size = state->m_currentFontSet[0]->charwidth;
	int width = m_width + state->m_currentFontSet[0]->spacing - m_leftPMargin - m_rightPMargin - m_x;

	if((m_defaultAlign == JUSTIFIED) && (align == NORMAL))
	{
		int extra;
		
		if(m_lineWCount > 1)
			extra = ((width - m_lineLength)<<8) / (m_lineWCount-1);
		else
			extra = 0;
		
		x = 0;

		for(i=0; i<m_lineWCount; i++)
		{
			Text(m_x + (x>>8) + m_leftPMargin, y, m_lineWords[i]);
			x += ((m_lineWLenghts[i]<<8) + extra);
		}
	}
	else
	{
		int len = m_lineLength + (m_lineWCount-1) * space_size;
	
		if(align == CENTER)
			x = (width - len) / 2;
		else
		if(align == RIGHT)
			x = (width - len);
	
		for(int i=0; i<m_lineWCount; i++)
		{
			Text(m_x + x + m_leftPMargin, y, m_lineWords[i]);
			x += (m_lineWLenghts[i] + space_size);
		}
	}
	
	m_lineWCount = 0;
	m_lineLength = 0;
}



char *TextRenderer::ParseParagraphInfo(char *line, int *page)
{
	char *ptr = line;
	
	m_x = 0;
	
	while(*ptr == 11)
	{
		switch(ptr[1])
		{
		case BC_LEFTALIGN:
			state->m_paragraphAlign = LEFT;
			break;
		case BC_CENTERALIGN:
			state->m_paragraphAlign = CENTER;
			break;
		case BC_RIGHTALIGN:
			state->m_paragraphAlign = RIGHT;
			break;
		case BC_BIGFONT:
			//m_paragraphFontSize = BIG;
			SetFontSet(BIG);
			break;
		case BC_MEDIUMFONT:
			//m_paragraphFontSize = MEDIUM;
			SetFontSet(MEDIUM);
			break;
		case BC_NORMALFONT:
			//m_paragraphFontSize = NORMAL;
			SetFontSet(NORMAL);
			break;
		case BC_CHAPTER:
			*page = BC_CHAPTER;
			return ptr+2;			
			break;
		case BC_BOOK:
			*page = BC_BOOK;
			return ptr+2;			
			break;
		case BC_INDENT1:
			state->m_indent = 1;//
			break;
		case BC_NOINDENT:
			state->m_indent = 0;
			break;
		}
		ptr += 2;
	}

	return ptr;
}

static char *seplist[16];
static int sep = 0;

char *NextWord2(char *ptr)
{
	sep = 0;
	while(*ptr && *ptr != ' ') { if(*ptr == '-') seplist[sep++] = ptr; ptr++; }
	while(*ptr && *ptr == ' ') ptr++;
	return ptr;
}

char *TextRenderer::RenderParagraph(char *line, bool render)
{
	char *ptr;

	char *oldptr = line;
	
		
	int fh = state->m_currentFontSet[0]->height + m_lineSpace;
	int space_size = state->m_currentFontSet[0]->charwidth;

	m_leftPMargin = m_margins[LEFT] + state->m_indent * space_size * 4;
	m_rightPMargin = m_margins[RIGHT] + state->m_indent * space_size * 4;
	
	int width = m_width + state->m_currentFontSet[0]->spacing - m_leftPMargin - m_rightPMargin;
	int bottom = m_height - m_margins[BOTTOM];

	//m_x = 0;
	int x = 0;
	m_x = 0; // state->m_indent * state->m_currentFontSet[0]->charwidth*3;
	
	if(*oldptr == 9)
	{
		if(state->m_paragraphAlign == LEFT)// || state->m_paragraphAlign == JUSTIFIED)
		{
			PushWord("   ", space_size*3);
			//x += space_size*3;
			x = space_size*4;
		}
		oldptr++;
		
	}
	
//	int x = m_x;

	//m_display->SetFontSize(m_paragraphFontSize);
	
	int f = state->m_currentFont;

	do
	{
		if(m_y+fh > bottom)
			return oldptr;

		ptr = NextWord2(oldptr);		
		ptr[-1] = 0;

		int len = TextLength(oldptr, &f);
		if(!render)
			state->m_currentFont = f;
		
		if((x+len+space_size) < width)
		{
			if(render)
				PushWord(oldptr, len);
			x += (len+space_size);				
		}
		else
		if((x+len) < width)
		{
			if(render)
			{
				PushWord(oldptr, len);
				RenderLine();
			}
			x = width;//m_x = 0;
			//m_y += fh;
			
		}
		else
		if(len > width)
		{
			if(render)
				RenderLine();
			//m_x = 0;
			x = m_x;

			int c = ' ';
			// We need to cut word
			while((x+len) > width)
			{
				ptr--;					
				*ptr = c;					
				c = ptr[-1];
				ptr[-1] = 0;
				len = TextLength(oldptr);
			}
			
			if(render)
			{
				PushWord(oldptr, len);
				RenderLine();
			}
			//x = m_x = 0;
			x = m_x;

			ptr[-1] = 0;
			
			m_y += fh;
			
		}
		else
		{
			// If word contains separators may be able to print part of the word
			// on the current line			
			while(sep)
			{
				int l2;
				sep--;
				char c = seplist[sep][1];
				seplist[sep][1] = 0;
				l2 = TextLength(oldptr);
				//if(render)
				//	printf("'%s' (%d+%d) < %d\n", oldptr, x, l2, width);

				if((x+l2) < width)
				{
					if(render)
					{
						//printf("Push '%s'\n", oldptr);
						PushWord(oldptr, l2);
						RenderLine();
					}
					seplist[sep][1] = c;
					oldptr = seplist[sep]+1;
					len -= l2;
					break;
				}
				
				seplist[sep][1] = c;
			}

			// This will do nothing if we already rendered in loop above
			if(render)
				RenderLine();

			//x = m_x = 0;
			x = m_x;

			// Word must be written on new line
			m_y += fh;
			
			if(m_y+fh > bottom)
				return oldptr;

			x = m_x + len + space_size;
			
			if(render)
				PushWord(oldptr, len);
		}
		
		oldptr = ptr;
	}
	while(*ptr);
	
	// We rendered entire paragraph

	//if(state->m_paragraphAlign == JUSTIFIED)
	//	state->m_paragraphAlign = LEFT;

	int a = m_defaultAlign;
	
	if(m_defaultAlign == JUSTIFIED)
		m_defaultAlign = LEFT;

	if(render)
		RenderLine();
	
	m_defaultAlign = a;

	//m_x = 0;
	m_y += fh;

	return NULL;

}

bool TextRenderer::RenderPage(int page, bool render)
{
	int linepos = 0;
	char *line;
	//int space_size = m_display->GetFont(0)->charwidth;

	//m_display->SetFontSize(0);
	
	//if(render)
	//	m_display->Lock();
	
	if(render)
		state = &renderState;
	else
		state = &processState;

	int fh = state->m_currentFontSet[0]->height + m_lineSpace;//m_font->Height();

	m_x = 0;
	m_y = 0;
	m_lineWCount = 0;
	m_lineLength = 0;

	if(page < 1)
		page = 1;

	if(page > m_pageCount)
		page = m_pageCount;
	
	if(render)
		m_page = page;
	
	m_width = m_display->Width(); //page_width;
	m_height = m_display->Height(); //page_height;
	
	m_y += m_margins[TOP];
	
	//int width = m_width + state->m_currentFontSet[0]->spacing - m_margins[LEFT] - m_margins[RIGHT];
	
	
	//if(page)
	m_stream->Seek(Offset(page));
	//printf("Seeking to %d\n", Offset(page));
	SetState(page, state);	
	//state->m_currentFont = State(page);

	char *endp;
	
	bool first = true;
	
	while(true)
	{
		linepos = m_stream->Tell();
		line = m_stream->ReadLine();

		if(!line)
			break;
				
		int pagebreak = 0;

		//if(!first)
		/*{
			state->m_paragraphAlign = LEFT;
			SetFontSet(NORMAL);
			first = false;
		}*/

		endp = ParseParagraphInfo(line, &pagebreak);
		
		if(pagebreak)
		{
			if(!render)
			{
				if(pagebreak == BC_CHAPTER)
					m_chapters[m_chapterCount++] = page+1;
				else
				if(pagebreak == BC_BOOK)
					m_books[m_bookCount++] = page+1;
			}			
			break;
		}
		
		endp = RenderParagraph(endp, render);
		
		if(!endp)
		{
			state->m_paragraphAlign = LEFT;
			SetFontSet(NORMAL);
		}
		
		if((m_y + state->m_currentFontSet[0]->height + m_lineSpace)  > (m_height - m_margins[BOTTOM]))
			break;
		
		//if(endp)
		//	break;
	}

	if(line && endp)
	{
		m_stream->Seek(linepos + (endp-line));
		//state->m_paragraphAlign = LEFT;
		//SetFontSet(NORMAL);
	}

	//if(render)
	//	m_display->Unlock();
	
	return !(line == 0);
}

void TextRenderer::Reset()
{
	m_stream->Seek(0);
	m_pageCount = 1;
	m_done = false;
	
	m_chapterCount = 0;
	m_bookCount = 0;
}

void TextRenderer::Reindex()
{
	int pos = Offset(m_page); //m_stream->Tell();
	Reset();
	
	
	//printf("page:%d offset:%d\n", m_page, pos);
	
	while(true)
	{
		//printf("cmp %d\n", Offset(m_pageCount));
		if(Offset(m_pageCount) >= pos)
		{
			m_page = m_pageCount-1;
			m_stream->Seek(Offset(m_pageCount));
			return;
		}
		ProcessPage();
	}	
}

int TextRenderer::ProcessPage()
{	
	if(m_done)
		return m_pageCount;
	
	if(m_pageCount >= m_allocCount)
	{
		m_allocCount = m_allocCount * 2;
		m_pageOffsets = (unsigned int *)realloc(m_pageOffsets, m_allocCount * sizeof(int));
		
		ASSERT(m_pageOffsets != 0);
	}
	
	if(!RenderPage(m_pageCount, false))
	{
		m_done = true;
	}
	else
	{
		m_pageOffsets[m_pageCount++] = (m_stream->Tell()) | 
		(state->m_currentFont<<28) |
		(state->m_indent<<30) |
		(state->m_paragraphAlign<<26) |
		(GetFontSet() << 24);
		
		
		//printf("%d %d %d %d\n", state->m_currentFont,
		//state->m_indent,
		//state->m_paragraphAlign,
		//GetFontSet());
	}
	
	return m_pageCount;

}
