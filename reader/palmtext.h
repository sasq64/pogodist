#ifndef PALMTEXT_H
#define PALMTEXT_H

#ifdef GBA
#include <pogo.h>
#else
#include <stdio.h>
#endif

class PalmText : public ITextStream
{
public:

	PalmText(char *fname);
	char *ReadLine(char *dest = 0, int size = -1);
	int Tell();
	int Size();
	int Seek(int pos);

//private:
	void UnpackIfNeeded(int n);
	void ReadRecord(int i);
	void UnpackNext();
	int UnpackToBuffer(int i);
	void SwapBuffers(int a, int b);
	
	struct Record
	{
		int offset;
		int size;
		int unpacked;
	};

	int m_version;
	int m_recordSize;
	
	int m_numRecords;
	Record *m_records;
	
	int m_currRecord;  // Current read & unpacked record

	int m_currPos; // Current "seek" position ( < m_unpackedSize)
	int m_seekRecord;
	int m_seekOffset;
	
	// We always read records to here
	unsigned char m_readBuffer[4096];

	int m_readSize;
	int m_readRecord;
	
	// How many unpackbuffers we have allocated
	int m_unpackBufCount;
	
	struct Buffer
	{
		unsigned char *ptr;
		int age;
		int rec;
	};
	
	// Buffers for unpacked records
	Buffer m_unpackBuffer[16];
	
	// Which record is in each buffer
//	int m_unpackRecord[16];

//	int m_age[16];
	
	// Sizes (in unpacked space) for all records
	// When we seek we need to find which record matches the filepos.
	// If that record is not mapped to unpacked space yet we need to
	// unpack records until we get there
	//int m_unpackSize[256];
	//int m_unpackStart[256];
	
	// How many records we have unpacked so far
	int m_maxUnpack;
	
	int m_unpackTotalSize;
	
	FILE *m_fp;
	int m_fileSize;
	
	char m_linebuf[4096];
};

#endif
