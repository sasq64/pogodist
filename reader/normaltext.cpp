
#ifdef GBA

#include <pogo.h>

#else

#include <stdio.h>
#include <string.h>

#endif

#include "normaltext.h"

NormalText::NormalText(char *fname)
{
	m_fp = fopen(fname, "rb");
	if(m_fp)
	{
		fseek(m_fp, 0, SEEK_END);
		m_size = ftell(m_fp);
		fseek(m_fp, 0, SEEK_SET);
	}
}

int NormalText::Seek(int offset)
{
	return fseek(m_fp, offset, SEEK_SET);
}

int NormalText::Tell()
{
	return ftell(m_fp);
}

int NormalText::Size()
{
	return m_size;
}

char *NormalText::ReadLine(char *dest, int size)
{
	if(!dest)
	{
		dest = m_linebuf;
		size = sizeof(m_linebuf);
	}

	return fgets(dest, size, m_fp);		
}
/*
int NormalText::Read(char *dest, int size)
{
	return fread(dest, 1, size, m_fp);
}
*/
