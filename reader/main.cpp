/*
 * PIXIE READER - part of the Pogo/Pixe project
 * (C) Jonas Minnberg 2004
 */

#ifdef GBA

int __gba_multiboot;
#include <pogo.h>

void abort(void)
{
	while(1);
}

#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL.h>

#endif

#ifdef WIN32
#include <windows.h>
#endif
	

//#include <assert.h>
//#define ASSERT(x) assert(x)

#define ASSERT(x) (x)

#include "types.h"
#include "util.h"
#include "display.h"
#include "itextstream.h"
#include "textrenderer.h"
#include "config.h"
#include "textbar.h"
#include "book.h"

extern "C" {
void memory_setarea(int area);
}


PogoFont *loadfont(char *dir, char *font)
{
	char tmp[128];
	char *ext = strrchr(font, '.');
		
	strcpy(tmp, dir);
	char *ptr = &tmp[strlen(tmp)];
	if(ptr[-1] != '/')
		*ptr++ = '/';
	strcpy(ptr, font);
	if(!ext)
		strcat(ptr, ".font");
	
	printf("%s %s => %s\n", dir, font, tmp);

	return pogofont_load(tmp);
}

void set_defaults(Config &cfg)
{

#ifdef GBA
	cfg["FONT_DIR"] = ".shell/fonts/";
#else
	cfg["FONT_DIR"] = "fonts/";
#endif
		
	cfg["FONT0_NORM"] = "arial10";
	cfg["FONT0_ITAL"] = "arial10i";
	cfg["FONT0_BOLD"] = "arial10b";
	cfg["FONT0_FIXED"] = "cour10";
	cfg["FONT1_NORM"] = "arial18";
	cfg["FONT1_ITAL"] = "arial18i";
	cfg["FONT1_BOLD"] = "arial18b";
	cfg["FONT1_FIXED"] = "cour10";
	cfg["FONT2_NORM"] = "arial24";
	cfg["FONT2_ITAL"] = "arial24i";
	cfg["FONT2_BOLD"] = "arial24b";
	cfg["FONT2_FIXED"] = "cour10";

	cfg["ALIGN"] = "left";
	cfg["MARGIN_LEFT"] = "2";
	cfg["MARGIN_RIGHT"] = "2";
	cfg["MARGIN_TOP"] = "14";
	cfg["MARGIN_BOTTOM"] = "1";
	cfg["LINESPACE"] = "-1";
	cfg["TITLEBAR"] = "\002%t\t%03p / %03P ";

	cfg.Load();
#ifndef GBA
	cfg.Save();
#endif

}

int main(int argc, char **argv)
{
	Display *display = new Display(600, 680);

	char *bookname;
	Book *book = 0;
	if(argc > 1)
		bookname = argv[1];
	else
		bookname = "default.pdb";
	book = new Book(bookname);

	if(!book->Ok())
	{
		fprintf(stderr, "BOOK ERROR\n");
		return 0;
	}
	
	bool notSaved = true;
	char savename[80];	
#ifdef GBA
	strcpy(savename, "/sram/");
	makeBaseName(bookname, &savename[6]);
#else
	makeBaseName(bookname, savename);
#endif

	strcat(savename, ".ind");
	//fprintf(stderr, "SAVENAME:%s\n", savename);

	PogoFont *font[12];
	
#ifdef GBA
	Config cfg(".shell/reader.cfg");
#else
	Config cfg("reader.cfg");
#endif

	set_defaults(cfg);

	char *fontdir = cfg["FONT_DIR"];

	font[0] = loadfont(fontdir, cfg["FONT0_NORM"]);	
	font[2] = loadfont(fontdir, cfg["FONT0_BOLD"]);
	font[3] = loadfont(fontdir, cfg["FONT0_FIXED"]);
	font[1] = loadfont(fontdir, cfg["FONT0_ITAL"]);
	
	
	printf("%d %d\n", font[0]->charwidth, font[3]->charwidth);

	font[4] = loadfont(fontdir, cfg["FONT1_NORM"]);	
	font[6] = loadfont(fontdir, cfg["FONT1_BOLD"]);
	font[7] = loadfont(fontdir, cfg["FONT1_FIXED"]);
	font[5] = loadfont(fontdir, cfg["FONT1_ITAL"]);
	
	font[8] = loadfont(fontdir, cfg["FONT2_NORM"]);	
	font[10] = loadfont(fontdir, cfg["FONT2_BOLD"]);
	font[11] = loadfont(fontdir, cfg["FONT2_FIXED"]);
	font[9] = loadfont(fontdir, cfg["FONT2_ITAL"]);	
	//font[3] = font[7] = font[11] = 0;

	ITextStream *stream = book->ReadSection(0);
	
	TextRenderer *tr = new TextRenderer(display, stream, font);
	
	TextBar *titleBar = new TextBar(cfg["TITLEBAR"], display, &font[0], 0);
	titleBar->SetSource('t', book->Title());
	
	tr->SetMargins(atoi(cfg["MARGIN_LEFT"]), atoi(cfg["MARGIN_TOP"]), atoi(cfg["MARGIN_RIGHT"]), atoi(cfg["MARGIN_BOTTOM"]));//2, 16, 2, 2);
	tr->SetLineSpace(atoi(cfg["LINESPACE"]));
	
	if(strcmp(cfg["ALIGN"], "justified") == 0)
		tr->SetDefaultAlignment(TextRenderer::JUSTIFIED);
	else
		tr->SetDefaultAlignment(TextRenderer::LEFT);
	
#ifdef GBA
	int ch;	
	ch = getchar();		
	if(ch == EOF)
		tr->LoadIndex(savename);
#else
	
	if(tr->LoadIndex(savename))
		printf("Could load index!\n");
	else
		printf("No index!\n");
#endif

	display->Lock();
	display->Fill();
	display->Unlock();

	
	
	int c = 1;
	int p = tr->GetPage();
	int e = 0;

	tr->ProcessPage();

	do
	{
		if(e != -1)
		{
			int oldp = p;
			switch(e)
			{
			case 1:
				c = tr->GetCurrentChapter();
				p = tr->GetChapterPage(c+1);
				if(p < 0)
					p = oldp;
				break;
			case 2:
				c = tr->GetCurrentChapter();
				p = tr->GetChapterPage(c);
				if(p == oldp)
					p = tr->GetChapterPage(c-1);
				if(p < 0)
					p = oldp;
				break;
			case 3:
				p++;
				break;
			case 4:
				p--;
				break;
			case 6:
				p = 1;
				break;
			case 5:
				p = tr->PageCount();
				break;
			case 9:				
				{
					tr->Reindex();
					p = tr->GetPage();
					while(!tr->Done())
						tr->ProcessPage();
				}
				break;
			}
			
			if(p < 1)
				p = 1;
			if(p > tr->PageCount())
				p = tr->PageCount();

			display->Lock();
			display->Fill();
			//display->Fill(0,0,16,240, 160-16);
			tr->RenderPage(p);
			titleBar->SetSource('p', p);
			titleBar->SetSource('P', tr->PageCount());
			//display->Fill(0,0,0,240,10);
			titleBar->Render();
			display->Unlock();
		}
		else
		if(!tr->Done())
			tr->ProcessPage();
		else
		if(notSaved)
		{
			tr->SaveIndex(savename);
			notSaved = false;
		}
		
	}
	while((e = display->WaitEvent()));

	tr->SaveIndex(savename);

	return 0;
}
