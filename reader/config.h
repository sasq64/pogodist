#ifndef CONFIG_H
#define CONFIG_H

class Hashtable
{
public:
	Hashtable();
	~Hashtable();
	char*& operator[](const char *s);
	int size();

protected:
	struct HashEntry
	{
		const char *name;
		char *val;
		struct HashEntry *next;
	};
	
	HashEntry *symbols[41];
	int m_size;
	
	unsigned int hash(const char *str);

};

class Config : public Hashtable
{
public:
	Config(char *name);
	bool Load();
	bool Save();
	
	char *m_name;
};

#endif
