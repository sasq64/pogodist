#ifndef CHUNKFILE_H
#define CHUNKFILE_H

#ifdef GBA

#include <pogo.h>

#else

#include <stdio.h>
#include <string.h>

#endif

class ChunkFile
{
public:
	enum
	{
		READ = 1,
		WRITE
	};
	
	ChunkFile(const char *name, int flags);
	~ChunkFile();
	
	bool Ok() { return (m_fp != NULL); }

	bool OpenChunk(const char *chunk = 0);
	void CloseChunk();
	
	int Write(unsigned int v);
	int Write(const char *str);
	
	unsigned int ReadInt();
	int Read(char *dest, int size = 0);
	char *ReadLine(char *dest, int size = 0);

	int ChunkSize() const { return m_chunkSize; }
	const char *ChunkName() const { return m_chunkName; }
	bool IsChunk(const char *name) const { return (strcmp(name, m_chunkName) == 0); }
	
	void Close();
	
	int Seek(int offset);
	int Tell();

private:
	FILE *m_fp;
	
	struct Chunk
	{
		int pos;
		int size;
	};
	
	Chunk m_chunkStack[32];	
	Chunk *m_chunkPos;
	
	bool m_write;
	int m_chunkSize;
	int m_chunkStart;
	char m_chunkName[5];
};

#endif
