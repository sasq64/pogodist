#ifndef BOOK_H
#define BOOK_H

#define EBOOK_MAGIC 0xDA0EB00C

class ChunkFile;
class ITextStream;

class Book
{
public:
	Book(char *fname);
	int Sections() { return m_textCount; }
	ITextStream *ReadSection(int i);
	
	char *Title() { return (strlen(m_title) ? m_title : m_fileName); }
	
	bool Ok() { return m_valid; }
	
private:
	bool m_valid;
	int m_textCount;
	ChunkFile *m_file;
	int m_textPos[32];
	int m_textSize[32];	
	char m_title[80];
	
	char *m_fileName;
	
	enum
	{
		NONE,
		TEXT,
		PALM,
		BOOK,
	};
			
	
	int m_type;
};

#endif
