#ifdef GBA
	
#include <pogo.h>
	
#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char uchar;
typedef unsigned int uint32;

#endif
	
#include "itextstream.h"
#include "palmtext.h"
//
//// PALM DOC PARSER


typedef unsigned char Word[2];
typedef unsigned char DWord[4];

struct pdb_header {       // 78 bytes total
	char	 name[32];
	Word	 attributes;
	Word	 version;
	DWord	 create_time;
	DWord	 modify_time;
	DWord	 backup_time;
	DWord	 modificationNumber;
	DWord	 appInfoID;
	DWord	 sortInfoID;
	char	 type[4];
	char	 creator[4];
	DWord	 id_seed;
	DWord	 nextRecordList;
	Word	 numRecords;
};


struct pdb_rec_header {   // 8 bytes total
	DWord	 offset;
	char     attributes;
	char	 uniqueID[3];
};

struct doc_record {      // 16 bytes total
	Word	 version;      // 1 = plain text, 2 = compressed text
	Word	 reserved1;
	DWord	 doc_size;     // uncompressed size in bytes
	Word	 num_recs;     // not counting itself
	Word	 rec_size;     // in bytes: usually 4096 (4K)
	DWord	 reserved2;
};

static int GetWord(unsigned char *p)
{
//	fprintf(stderr,"%02x %02x\n", p[0] & 0xff, p[1] & 0xff);
	return (p[0] << 8) | p[1];
}

static int GetDWord(unsigned char *p)
{
	return (p[0] << 24) | (p[1]<<16) | (p[2] << 8) | p[3];
}

/*bool PalmText::Require(int offset)
{
	int i;
	
	for(i=0; i<m_maxUnpack; i++)
	{
		if(offset >= m_unpackStart[i] && offset <= (m_unpackStart[i] + m_unpackSize[i]))
		{
			UnpackIfNeeded(i);
			return true;
		}
	}
	
	for(;i<m_numRecords;i++)
	{
		UnpackNext();
		if(offset >= m_unpackStart[i] && offset <= (m_unpackStart[i] + m_unpackSize[i]))
		{
			return true;
		}

	}
	
	return false;
	
}
*/

void PalmText::SwapBuffers(int a, int b)
{
	Buffer buf = m_unpackBuffer[a];
	m_unpackBuffer[a] = m_unpackBuffer[b];
	m_unpackBuffer[b] = buf;
}

// Make sure m_unpackBuffer[0] is record n
void PalmText::UnpackIfNeeded(int n)
{
	int max_age = 0;
	int oldest = -1;
	int i;

	//fprintf(stderr,"---Want record %d\n", n);
	
	/*for(i=0; i<m_unpackBufCount; i++)
	{
		fprintf(stderr,"%d: [%08p] RECORD %03d, AGE %d\n", i, 
			   m_unpackBuffer[i].ptr, m_unpackBuffer[i].rec, m_unpackBuffer[i].age);
	}*/

	
	for(i=0; i<m_unpackBufCount; i++)
	{
		Buffer *b = &m_unpackBuffer[i];
		
		if(b->rec == n)
		{
			//fprintf(stderr,"Record %d already unpacked\n", n);			
			SwapBuffers(0, i);
		}
		else
			b->age++;

		if(b->age >= max_age)
		{
			max_age = b->age;
			oldest = i;
		}
	}
	
	//fprintf(stderr,"Oldest record is %d (buffer %d)\n", m_unpackBuffer[oldest].age, oldest);
	
	if(m_unpackBuffer[0].rec != n)
	{
		//fprintf(stderr,"Throwing it out\n");
		
		SwapBuffers(0, oldest);		
		ReadRecord(n);
		m_records[n].unpacked = UnpackToBuffer(0);
	}
	
	m_unpackBuffer[0].rec = n;
	m_unpackBuffer[0].age = 0;
	
}

void PalmText::ReadRecord(int i)
{
	int offset = m_records[i].offset;
	int size = m_records[i+1].offset - offset;
	fprintf(stderr, "Reading from %d, %d bytes\n", offset, size);
	fseek(m_fp, offset, SEEK_SET);
	m_readSize = fread(m_readBuffer, 1, size, m_fp);
	m_readRecord = i;

}

void PalmText::UnpackNext()
{
/*	ReadRecord(m_maxUnpack+1);
	int size = UnpackToBuffer(0);
	m_unpackSize[m_maxUnpack++] = size;*/
}

static void memcpy2(uchar *dst, uchar *src, int len)
{
	while(len--)
		*dst++ = *src++;
}

int PalmText::UnpackToBuffer(int i)
{
	int len;
	int offs;
	unsigned char c;
	unsigned char *ptr = m_readBuffer;
	unsigned char *dst = m_unpackBuffer[i].ptr;
	unsigned char *end = &m_readBuffer[m_readSize];
								
	while(ptr < end)
	{
		c = *ptr++;

		if(((c > 8) && (c < 0x80)) || (!c))
		{
			//fprintf(stderr, "Literal '%c'\n", c);
			*dst++ = c;
		}
		else
		if(c <= 8)
		{
			//char *s = (char *)dst;
			//fprintf(stderr, "\nCopy %d bytes", c);
			while(c--)
			{
				*dst++ = *ptr++;
			}
			//*dst = 0;
			//fprintf(stderr, " '%s'\n", s); 
			//memcpy(dst, ptr, c);
			//dst += c;
			//ptr += c;
		}
		else
		if(c < 0xC0)
		{
			//fprintf(stderr,"%02x %02x   ", c, *ptr);
			offs = ((c & 0x3f)<<8) | (*ptr++);
			len = (offs & 0x7) + 3;
			offs = -(offs >> 3);			
			while(len--)
				*dst++ = dst[offs];
		}
		else
		{
			*dst++ = ' ';
			*dst++ = c & 0x7F;				
		}
	}
	*dst = 0;

	return dst - m_unpackBuffer[i].ptr;
}


PalmText::PalmText(char *fname)
{
	int i;
	
	fprintf(stderr, "PALMTEXT\n");
	
	m_fp = fopen(fname, "rb");
	if(m_fp)
	{		
		struct pdb_header header;
		struct pdb_rec_header record;

		m_maxUnpack = 0;
		m_seekRecord = 1;
		m_seekOffset = 0;
		m_currPos = 0;
		
		fseek(m_fp, 0, SEEK_END);
		m_fileSize = ftell(m_fp);
		fseek(m_fp, 0, SEEK_SET);

		//fprintf(stderr,"SIZE %d %d %d\n", sizeof(struct pdb_header), (char *)&header.type - (char *)&header, (char *)&header.numRecords - (char *)&header);
	
		fread(&header, 1, 78, m_fp);
		//fseek(m_fp, 80, SEEK_SET);
		
		m_numRecords = GetWord(header.numRecords);
		
		fprintf(stderr,"RECORDS %d  %d %d %d\n", m_numRecords,
				GetDWord(header.version),
				GetWord(header.attributes),
				GetDWord(header.appInfoID)
				);
		
		m_records = (Record *)malloc(m_numRecords * sizeof(Record));
		
		
		for(i=0; i<m_numRecords; i++)
		{
			//uchar *ptr = (uchar *)&record;
			//fread(&ptr[0], 1 ,1, m_fp);
			//fread(&ptr[1], 1 ,1, m_fp);
			//fread(&ptr[2], 1 ,1, m_fp);
			//fread(&ptr[3], 1 ,1, m_fp);
			fread(&record, 1, 7, m_fp);
			fseek(m_fp, 1, SEEK_CUR);

			fprintf(stderr, "%02x %02x %02x %02x\n", record.offset[0], record.offset[1], record.offset[2], record.offset[3]);
			m_records[i].offset = GetDWord(record.offset);
		}
		
		ReadRecord(0);
		
		struct doc_record *rec = (struct doc_record *)m_readBuffer;
		m_unpackTotalSize = GetDWord(rec->doc_size);
		m_recordSize = GetWord(rec->rec_size);
		m_version = GetWord(rec->version);

		fprintf(stderr,"%sompressed text, %d bytes (%d per record)\n", m_version == 2 ? "C" : "Unc", m_unpackTotalSize, m_recordSize);
		
		m_unpackBufCount = 3;
		for(i=0; i<m_unpackBufCount; i++)
		{
			m_unpackBuffer[i].ptr = (uchar *)malloc(12*1024);
			m_unpackBuffer[i].rec = -1;
			m_unpackBuffer[i].age = 0;
		}	
	}
}

int PalmText::Seek(int offset)
{
	int i;
	int pos = 0;
	
	for(i=1; i<m_numRecords; i++)
	{
		Record *r = &m_records[i];
		if(!r->unpacked)
			UnpackIfNeeded(i);

		if(offset >= pos && (offset < (pos + r->unpacked)))
		{
			m_seekRecord = i;
			m_seekOffset = offset-pos;
			m_currPos = offset;
			
			//fprintf(stderr,"Seek to %d/%d\n", i, m_seekOffset);
			
			return offset;
		}
		pos += r->unpacked;
	}

	m_currPos = m_unpackTotalSize;
	return m_currPos;
}

int PalmText::Tell()
{
	return m_currPos;
}

int PalmText::Size()
{
	return m_unpackTotalSize;
}

char *PalmText::ReadLine(char *dst, int size)
{
	uchar *ptr, *end;
	
	if(!dst)
	{
		dst = m_linebuf;
		size = sizeof(m_linebuf);
	}
	
	uchar *dest = (uchar *)dst;
	int recsize;
	
	if((m_currPos >= m_unpackTotalSize) || (m_seekRecord >= m_numRecords))
	{
		//fprintf(stderr,"DONE\n");
		return NULL;
	}

	do
	{
		UnpackIfNeeded(m_seekRecord);

		recsize =  m_records[m_seekRecord].unpacked;
		ptr = m_unpackBuffer[0].ptr + m_seekOffset;
		end = m_unpackBuffer[0].ptr + recsize;		

		//fprintf(stderr,"Reading from %d %d\n", m_seekRecord, recsize);

		while((ptr < end) && (*ptr != 10) && (size))
		{
			*dest++ = *ptr++;
	
			size--;
			m_seekOffset++;
			m_currPos++;
		}
		
		if(size && (*ptr == 10))
		{
			*dest++ = *ptr++;
			m_currPos++;
			m_seekOffset++;
			size = 0;
		}

		if(ptr >= end)
		{
			m_seekRecord++;
			m_seekOffset = 0;

			if(m_seekRecord >= m_numRecords)
				size = 0;
		}	
	}
	while(size);
	
	*dest++ = 0;
	
	return dst;

}
/*
int PalmText::Read(char *dest, int size)
{
	return fread(dest, 1, size, m_fp);
}
*/

/*
int main(int argc, char **argv)
{
	PalmText *pt = new PalmText("test.pdb");
		
	char tmp[512];
	
	pt->Seek(128000);
				 
	int i = 4;
	
	while(pt->ReadLine(tmp, sizeof(tmp)) && i)
	{		
		printf("-%s-", tmp);
		i--;
	}
	
	
	
	//while(m_maxUnpack < m_numRecords)
	//	pt->UnpackNext();
	
	//pt->ReadRecord(1);
	//pt->UnpackToBuffer(0);
	
//	printf("%s", pt->m_unpackBuffer[0]);
	
	
	return 0;
}
*/
