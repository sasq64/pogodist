
#ifndef NORMALTEXT_H
#define NORMALTEXT_H

#include "itextstream.h"

class NormalText : public ITextStream
{
public:
	NormalText(char *fname);
	int Size();
	int Seek(int offset);
	int Tell();
	//int Read(char *dest, int size);
	char *ReadLine(char *dest = 0, int size = -1);

private:
	FILE *m_fp;
	int m_size;
	char m_linebuf[8192];

};

#endif
