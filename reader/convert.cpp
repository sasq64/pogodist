#include <stdio.h>
#include <stdlib.h>
//#include <string.h>

#include <string>
#include <iostream>

using namespace std;

#include "bookformat.h"

#include "chunkfile.h"

class Hashtable
{
public:
	Hashtable();
	~Hashtable();
	int& operator[](string s);
	int size();

private:
	struct HashEntry
	{
		int val;
		struct HashEntry *next;
		char str[0];
	};
	
	HashEntry *symbols[41];
	int m_size;
	
	unsigned int hash(const char *str);

};

unsigned int Hashtable::hash(const char *str)
{
	unsigned int hash = 0;
	while(*str)
		hash = ((hash<<5)^(hash>>27))^*str++;

	return hash;
}

Hashtable::Hashtable()
{
	m_size = 41;
	memset(symbols, 0, m_size * sizeof(void *));
}

Hashtable::~Hashtable()
{
	HashEntry *he, *he2;
	int i;

	for(i=0; i<m_size; i++)
	{
		he = symbols[i];
		while(he)
		{
			he2 = he->next;			
			free(he);			
			he = he2;
		}
	}
}

int Hashtable::size()
{
	HashEntry *he;
	int i;
	int size = 0;

	for(i=0; i<m_size; i++)
	{
		he = symbols[i];
		while(he)
		{
			size++;
			he = he->next;			
		}
	}
	return size;
}

int& Hashtable::operator[](string name)
{
	HashEntry **he;
	int i = hash(name.c_str()) % m_size;

	he = &symbols[i];
	while(*he && strcmp((*he)->str, name.c_str()) != 0)
		he = &((*he)->next);

	if(!(*he))
	{
		*he = (HashEntry *)malloc(sizeof(HashEntry) + name.size() + 1);
		strcpy((*he)->str, name.c_str());
		(*he)->val = 0;
		(*he)->next = 0;
	}
	
	return (*he)->val;

}

static Hashtable symbols;

class Tag
{
public:
	Tag(string text);
	
	bool is(string s) { return (name.compare(s) == 0); }	
	int var(string s) { return hash[s]; }
	
	enum
	{
		LEFT = 1,
		RIGHT,
		CENTER
	};
			

private:
	Hashtable hash;
	string name;
};



int getval(string name)
{
	//cout << "GETVAL " << name << endl;

	if(!symbols.size())
	{
		printf("SYMBOL INIT\n");
		symbols["left"] = Tag::LEFT;
		symbols["right"] = Tag::RIGHT;
		symbols["center"] = Tag::CENTER;
	}
	
	
	int rc = symbols[name];
	
	//printf("%s => %d\n", name.c_str(), rc);
	
	if(!rc)
		return atoi(name.c_str());
	else
		return rc;
}

Tag::Tag(string text)
{
	int i = 0;
	int c;
	string tmp;
	char dummy[2];
	
	dummy[1] = 0;
	
	//cout << text.substr(0, 20) << endl;

	while(text[i] != '<')
		i++;
	
	i++;
	
	c = 0;
	
	while(c != '>')
	{
		c = text[i++];
		
		tmp.resize(0);

		while(c != '>' && c != ' ')
		{
			*dummy = tolower(c);
			tmp.append(dummy);
			c = text[i++];
		}

		//printf("tag '%s'\n", tmp.c_str());

		if(name.empty())
		{
			if((tmp.length() == 2) && (tmp[0] == 'h') && (isdigit(tmp[1])))
			{
				name = string("h");
				hash["size"] = tmp[1] - 0x30;
			}
			else
			if((tmp.length() == 3) && (tmp.substr(0,2).compare("/h") == 0))
			{
				name = string("/h");
				hash["size"] = tmp[1] - 0x30;
			}
			else
				name = tmp;

			//printf("tag '%s'\n", name.c_str());			
		}
		else
		{
			//printf("Checking %s\n", tmp.c_str());
			int v = tmp.find('=');
			if(v != string::npos)
			{
				int a = getval(tmp.substr(v+1));
				hash[tmp.substr(0, v)] = a;
				//printf("%s = %s : %d\n", tmp.substr(v+1).c_str(), tmp.substr(0, v).c_str(), a);
			}
				
		}
	}
}

char *loadfile(char *name)
{
	int size;
	char *ptr = NULL;
	FILE *fp = fopen(name, "rb");
	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		size = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		ptr = (char *)malloc(size+1);
		fread(ptr, 1, size, fp);
		fclose(fp);
		ptr[size] = 0;
	}
	return ptr;
}

int savefile(const char *name, const char *ptr, int size = 0)
{
	FILE *fp = fopen(name, "wb");
		
	if(fp)
	{
		if(!size)
			size = strlen(ptr);
		fwrite(ptr, 1, size, fp);
		fclose(fp);
		return size;
	}
	return 0;
}


Tag *gettag(string &html, int &i)
{
	Tag *tag = 0;
	
	if(html[i] == '<')
	{
		int start = i;
		while(html[i] != '>')
			i++;
			
		tag = new Tag(html.substr(start, i-start+1));
	
		while(html[i] != '>')
			i++;
		//i++;
	}

	return tag;

}

#define ITALIC 1
#define BOLD 2
#define FIXED 4

static int spaces;
static int flags = 0;
static int oldflags = 0;

static void dospace(char *&dest)
{
	if(spaces)
		*dest++ = ' ';
	spaces = 0;
}

#define BOOK_LEFT 16
#define BOOK_RIGHT 18
#define BOOK_CENTER 17

#define SIZE_NORMAL 22
#define SIZE_MEDIUM 23
#define SIZE_LARGE 24

static int align = BOOK_LEFT;
static int font_size = SIZE_NORMAL;
static bool do_lf = false;

char title[128];
char author[128];

string html2book(string html)
{
	int c,i;
	Tag *tag;
	bool inhead = false;
	bool intitle = false;
	bool inauth = false;
	bool inParagraph = false;
	
	char *titleptr = title;
	char *authptr = author;
	
	char *outbuf = (char *)malloc(html.size() + 32);
	char *dest = outbuf;
	
	cout << "Converting " << html.size() << "bytes" << endl;

	int indention = 0;
	
	spaces = 0;
	flags = 0;
	
	for(i=0; i<html.size(); i++)
	{
		if(tag = gettag(html, i))
		{
			int oldflags = flags;
			if(tag->is("head"))
			{
				inhead = true;
			}
			else
			if(tag->is("/head"))
			{
				inhead = false;
			}
			else
			if(tag->is("title"))
			{
				intitle = true;
				printf("Found TITLE\n");
			}
			else
			if(tag->is("/title"))
			{
				intitle = false;
			}
			else
			if(tag->is("author"))
			{
				inauth = true;
				printf("Found AUTHOR\n");
			}
			else
			if(tag->is("/author"))
			{
				inauth = false;
			}
			else
			if(tag->is("blockquote"))
			{
				if(inParagraph)
					*dest++ = 10;
				*dest++ = 10;
				indention++;
				*dest++ = 11;
				*dest++ = indention+1;
			}
			else
			if(tag->is("/blockquote"))
			{
				*dest++ = 10;
				indention--;
				*dest++ = 11;
				*dest++ = indention+1;
			}
			else
			if(tag->is("i") || tag->is("em"))
				flags |= ITALIC;
			else
			if(tag->is("/i") || tag->is("/em"))
				flags &= ~ITALIC;
			else
			if(tag->is("b"))
				flags |= BOLD;
			else
			if(tag->is("/b"))
				flags &= ~BOLD;
			else
			if(tag->is("tt"))
				flags |= FIXED;
			else
			if(tag->is("dl"))
			{
				if(inParagraph)
					*dest++ = 10;
			}
			else
			if(tag->is("/dl"))
			{
			}
			else			
			if(tag->is("p"))
			{
				if(inParagraph)
					*dest++ = 10;
				//dospace();				
				if(tag->var("align") == Tag::CENTER)
				{
					align = BOOK_CENTER;
					printf("CENTER\n");
					*dest++ = 11;
					*dest++ = align;
				}
				else
				if(tag->var("align") == Tag::RIGHT)
				{
					align = BOOK_RIGHT;
					printf("RIGHT\n");
					*dest++ = 11;
					*dest++ = align;
				}
				else
					align = BOOK_LEFT;

				*dest++ = 9;
				
				spaces = 0;
				
				inParagraph = true;
			}
			else
			if(tag->is("/p"))
			{
				*dest++ = 10;
				spaces = 0;
				inParagraph = false;
			}
			else
			if(tag->is("br"))
			{
				//dospace();
				*dest++ = 10;
				if(align != BOOK_LEFT)					
				{
					*dest++ = 11;
					*dest++ = align;
				}
				if(font_size != SIZE_NORMAL)
				{
					*dest++ = 11;
					*dest++ = font_size;
				}
			}
			else
			if(tag->is("hr"))
			{
				*dest++ = 10;
				*dest++ = 11;
				*dest++ = BOOK_CENTER;
				strcpy(dest, "* * *");
				dest += 5;
				*dest++ = 10;
				
				if(align != BOOK_LEFT)		
				{
					*dest++ = 11;
					*dest++ = align;
				}
				if(font_size != SIZE_NORMAL)
				{
					*dest++ = 11;
					*dest++ = font_size;
				}
				
				spaces = 0;
			}
			else
			if(tag->is("h"))
			{	
				*dest++ = 10;

				int size = tag->var("size");
				
				if(size == 1)
				{
					*dest++ = 11;
					*dest++ = BC_CHAPTER;
					*dest++ = 10;
					*dest++ = 10;
				}
				else
				if(size == 4)
				{
					*dest++ = 11;
					*dest++ = BC_BOOK;
					*dest++ = 10;
					*dest++ = 10;
				}
					
				
				//printf("H%d\n", size);

				if(size <= 2)
				{
					font_size = SIZE_LARGE;
					*dest++ = 11;
					*dest++ = font_size;
				}
				else
				//if(size <= 3)
				{
					font_size = SIZE_MEDIUM;
					*dest++ = 11;
					*dest++= font_size;
				}
				/*else
				//if(size <= 3)
				{
					flags |= BOLD;
					font_size = SIZE_NORMAL;
				}*/

				if(tag->var("align") == Tag::CENTER)
				{
					align = BOOK_CENTER;
					printf("CENTER\n");
					*dest++ = 11;
					*dest++ = align;
				}
				else
				if(tag->var("align") == Tag::RIGHT)
				{
					align = BOOK_RIGHT;
					printf("RIGHT\n");
					*dest++ = 11;
					*dest++ = align;
				}
				else
					align = BOOK_LEFT;
				
				spaces = 0;
			}
			else
			if(tag->is("/h"))
			{
				flags &= ~BOLD;
				do_lf = true;
				font_size = SIZE_NORMAL;
				align = BOOK_LEFT;
				
				spaces = 0;
				
				//*dest++ = 10;
				//*dest++ = 1;
			}
			
			int diff = flags^oldflags;
			if(diff & BOLD)
			{
				if(flags & BOLD)
					*dest++ = 3;
				else
					*dest++ = 1;
			}
			if(diff & ITALIC)
			{
				if(flags & ITALIC)
					*dest++ = 2;
				else
					*dest++ = 1;
			}
			if(diff & FIXED)
			{
				if(flags & FIXED)
					*dest++ = 4;
				else
					*dest++ = 1;
			}
			
			if(do_lf)
			{
				do_lf = false;
				*dest++ = 10;
			}
			
			delete tag;

		}
		else
		switch(html[i])
		{
		case '&':
			{
				dospace(dest);
				int start = i+1;
				while(html[i] != ';')
					i++;
				string t = html.substr(start, i-start);
				cout << t << endl;
				if(t.compare("quot") == 0)
					*dest++ = '\"';
				else
				if(t.compare("nbsp") == 0)
					*dest++ = ' ';
				break;
			}
		case 0xA0:
			dospace(dest);
			*dest++ = '-';
			break;
		case ' ':
			spaces++;
			break;
		case 10:
		case 13:
			spaces++;
			break;
		default:
			if(inhead)
			{
				if(intitle)
				{
					dospace(titleptr);
					*titleptr++ = html[i];
				}
				else
				if(inauth)
				{
					dospace(authptr);
					*authptr++ = html[i];
				}
			}
			else
			{
				if((html[i] >= 0x20) && (html[i] <= 0x7F))
				{
					dospace(dest);
					*dest++ = html[i];
				}
			}
			break;
		}
	}
	
	
	cout << "done" << endl;
	
	*titleptr = 0;
	*authptr = 0;
	*dest = 0;
	string s = string(outbuf);
	free(outbuf);
	return s;
}



#define EBOOK_MAGIC 0xDA0EB00C

int main(int argc, char **argv)
{
	//Hashtable hash;
	//hash["TEST"] = 5;
	//printf("%d %d\n", hash["TEST"], hash["BAJS"]);
	//return 0;
	
	char *html = loadfile(argv[1]);
	
	cout << "HTML read to " << (unsigned int)html << endl;

	strcpy(title, "");
	strcpy(author, "");
	
	string dest = html2book(string(html));
	
	//printf(dest.c_str());
	//savefile("dump.txt", dest.c_str());
	
	ChunkFile *f = new ChunkFile(argv[2], ChunkFile::WRITE);
	
	f->OpenChunk("BOOK");

	f->OpenChunk("MAGI");
	f->Write(EBOOK_MAGIC);
	f->CloseChunk();

	f->OpenChunk("HEAD");
	
	f->OpenChunk("INFO");	
	f->Write(title);
	f->Write(author);
	f->CloseChunk();
	
	f->CloseChunk();

	f->OpenChunk("TEXT");
	//f->Write("This is the actual contents of\nthe document!\n\n--Sasq\n");

	f->Write(dest.c_str());
	
	f->CloseChunk();

	f->CloseChunk();
	
	delete f;
	
	return 0;
}
