
#ifdef GBA

#include <pogo.h>

#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"

#endif

#include "display.h"
#include "pogofont.h"
#include "textbar.h"


TextBar::TextBar(char *text, Display *display, PogoFont **fonts, int y)
{
	m_text = text;
	m_display = display;
	m_y = y;
	m_sourceCount = 0;
	
	m_fonts = fonts;
	
	SetSource('%', "%");
	
}

void TextBar::SetSource(char letter, char *string)
{
	int i;
	for(i=0; i<m_sourceCount; i++)
	{
		if(letter == m_source[i].letter)
		{
			m_source[i].string = string;
			m_source[i].type = STRING;

			return;
		}
	}
	
	m_source[i].letter = letter;
	m_source[i].string = string;
	m_source[i].type = STRING;
	m_sourceCount++;
}

void TextBar::SetSource(char letter, int number)
{
	int i;
	for(i=0; i<m_sourceCount; i++)
	{
		if(letter == m_source[i].letter)
		{
			m_source[i].number = number;
			m_source[i].type = NUMBER;
			return;
		}
	}
	
	m_source[i].letter = letter;
	m_source[i].number = number;
	m_source[i].type = NUMBER;
	m_sourceCount++;
}



bool TextBar::Parse(char *dest, int section)
{
	char number[8];
	char *num;

	char *dst = dest;
	char *ptr = m_text;
	
	while(section)
	{
		while(*ptr && (*ptr != 9)) ptr++;
		section--;
		if(*ptr)
			ptr++;
		else
			return false;
	}
	
	while(*ptr && (*ptr != 9))
	{
		if(*ptr == '%')
		{
			num = number;
			*num++ = *ptr++;
			
			while((*ptr >= 0x30) && (*ptr <= 0x39))
				*num++ = *ptr++;
			*num++ = 'd';
			*num = 0;
			
			for(int i=0; i<m_sourceCount; i++)
			{
				if(*ptr == m_source[i].letter)
				{
					if(m_source[i].type == STRING)
					{
						strcpy(dst, m_source[i].string);
						dst += strlen(dst);
					}
					else
					{
						sprintf(dst, number, m_source[i].number);
						dst += strlen(dst);
					}

				}
			}

			ptr++;
		}
		else
			*dst++ = *ptr++;
	}
	*dst = 0;
	
	return true;
}

void TextBar::Render()
{
			//pogofont_setcolor(m_bgCol, m_fgCol);	
	//m_display->Fill(m_margins[LEFT], fh+2, m_width - m_margins[LEFT] - m_margins[RIGHT], 1, 255);

	char tmp[128];
	int state = 0;
	
	m_display->Lock();
			
	if(Parse(tmp, 0))
	{
		m_display->Text(0, m_y, tmp, m_fonts, &state);
	}

	if(Parse(tmp, 1))
	{
		int dstate = state;
		int len = pogofont_text_multi(m_fonts, &dstate,  tmp, -1, -1);
		m_display->Text(m_display->Width() - len + m_fonts[1]->spacing, m_y, tmp, m_fonts, &state);
	}
	
	m_display->Fill(255, 0, m_y + m_fonts[0]->height, m_display->Width(), 1);
	
	m_display->Unlock();
			
}
