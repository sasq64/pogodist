
#ifdef GBA
#include <pogo.h>
#else
#include <stdio.h>
#include <string.h>
#endif

#include "util.h"

void WriteInt(FILE *fp, int v)
{
	int v2 = v;
	fwrite(&v2, 1, sizeof(int), fp);
}

void WriteWord(FILE *fp, int v)
{
	unsigned short v2 = v;
	fwrite(&v2, 1, sizeof(short), fp);
}

void WriteByte(FILE *fp, int v)
{
	unsigned char v2 = v;
	fwrite(&v2, 1, 1, fp);
}

int ReadInt(FILE *fp)
{
	int v;
	fread(&v, 1, sizeof(int), fp);	
	return v;
}

int ReadWord(FILE *fp)
{
	unsigned short v;
	fread(&v, 1, sizeof(short), fp);	
	return v;
}

int ReadByte(FILE *fp)
{
	unsigned char v;
	fread(&v, 1, 1, fp);
	return v;
}

void ReadString(FILE *fp, char *str)
{
	char c = ReadByte(fp);
	fread(str, 1, c, fp);
	str[c] = 0;
}

void WriteString(FILE *fp, const char *str)
{
	int l = strlen(str);
	WriteByte(fp, l);
	fwrite(str, 1, l, fp);
}

char *makeBaseName(char *name, char *dest)
{
	char *start, *end, *p;
	if(p = strrchr(name, '/'))
		start = p+1;
	else
		start = name;
		
	if(p = strrchr(name, '.'))
		end = p;
	else
		end = &name[strlen(name)];
	
	strncpy(dest, start, end-start);
	dest[end-start] = 0;
	return dest;
}

char *NextWord(char *ptr)
{
	while(*ptr && *ptr != ' ') ptr++;
	while(*ptr && *ptr == ' ') ptr++;
	return ptr;
}
