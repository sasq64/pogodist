#ifdef GBA

#include <pogo.h>

#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"

#endif

#include "pogofont.h"

int screen_setcolors(unsigned int *pal, int start, int size);
void screen_copy(int x, int y, uchar *src, int width, int height, int sw, int solid);
void screen_set(int x, int y, int width, int height, int color);


//enum {BG, FG};
//
#define BG 0x00

//uint32 colors[2];

void pogofont_setcolor(uint32 fg, uint32 bg)
{
	int i;
	uint32 color;

	int red = (bg & 0xff0000);
	int green = (bg & 0x00ff00)<<8;
	int blue = (bg & 0xff)<<16;

	int ra = ( (fg & 0xff0000) - red)>>8;
	int ga = ( ((fg & 0xff00)<<8) - green)>>8;
	int ba = ( ((fg & 0xff)<<16) - blue)>>8;

	//printf("%x %x %x + %x %x %x\n", red, green, blue, ra, ga, ba);

	for(i=0; i<256; i++)
	{
		//int k = (255-i);
		color = (red&0xff0000) | ((green>>8)&0xff00) | ((blue>>16)&0xff);

		red += ra;
		green += ga;
		blue += ba;

		screen_setcolors(&color, i, 1);
	}
	
//	uint32 colors[2] = {0x00FFFFFF, 0x00000000};
//	screen_setcolors(colors, 0, 2);
	
}

static uchar pogofont_putmono(PogoFont *font, int c, int x, int y)
{
	register int w;
	int solid = !(font->flags & FFLG_TRANSP);
	if(c < font->first || c >= font->last)
		return 0;

	w = font->charwidth;
	if(y >= 0)
	{
		if(c == ' ')
		{
			if(solid)
				screen_set(x, y, w, font->height, BG);
		}
		else
		{
			if(solid)
				screen_copy(x, y, &font->pixels[(c - font->first) * w], w, font->height, font->width, 1);
			else
				screen_copy(x, y, &font->pixels[(c - font->first) * w], w, font->height, font->width, 0);
		}
	}
	return w;
}

static uchar pogofont_putprop(PogoFont *font, int c, int x, int y)
{
	register int offset, w, index;
	register int ff = font->first;
	int solid = !(font->flags & FFLG_TRANSP);

	if(c == ' ')
	{
		w = font->charwidth;
		if((y >= 0) && solid)
			screen_set(x, y, w, font->height, BG);
	} 
	else
	{
		if(font->flags & FFLG_UNICODE)
		{
			int *seq = font->sequences;
			index = 0;
		
			for(; *seq >= 0; seq += 2)
			{
				if((c >= seq[0]) && (c <= seq[1]))
				{
					index += (c - seq[0]);
					break;
				}
				index += (seq[1] - seq[0] + 1);				
			}
		
			if(*seq < 0)			
				return 0;
		}
		else
		{
			if(c < font->first || c >= font->last)
				return 0;
			index = c - font->first;
		}

		offset =  font->offsets[index];
		w = font->offsets[index + 1] - offset;
		if(y >= 0)
		{

			if(solid)
				screen_copy(x, y, &font->pixels[offset], w, font->height, font->width, 1);
			else
				screen_copy(x, y, &font->pixels[offset], w, font->height, font->width, 0);
		}
		w += font->spacing;
		if(w <= 0) w = 1;
	}
	return w;
}

uchar pogofont_putchar(PogoFont *font, int c, int x, int y)
{
	int rc, fl;

	//if(font->flags & FFLG_COLOR)
	//	colors = font->colors;
	//else
	//	colors = pogofont_colors;
	
	if(font->offsets)
	{
		if(font->flags & FFLG_BOLD)
		{
			pogofont_putprop(font, c, x, y);
			fl = font->flags;
			font->flags |= FFLG_TRANSP;
			rc = pogofont_putprop(font, c, x+1, y);
			font->flags = fl;
			return rc+1;
		}
		else
			return pogofont_putprop(font, c, x, y);
	}
	else
	{
		if(font->flags & FFLG_BOLD)
		{
			pogofont_putmono(font, c, x, y);
			fl = font->flags;
			font->flags |= FFLG_TRANSP;
			rc = pogofont_putmono(font, c, x+1, y);
			font->flags = fl;
			return rc+1;
		}
		else
			return pogofont_putmono(font, c, x, y);
	}
}

int pogofont_text(PogoFont *font, char *str, int x, int y)
{
	int l = 0;

	while(*str)
		l += pogofont_putchar(font, *str++, x+l, y);

	return l;
}

int pogofont_text_multi(PogoFont **fontlist, int *current, char *txt, int x, int y)
{
	PogoFont *font;
	int l = 0;
	int c;
	unsigned char *str = (unsigned char *)txt;

	font = fontlist[*current];

	while(*str)
	{
		c = *str++;
		if(c <= 8)
			font = fontlist[*current = (c - 1)];
		else
		if(c <= 0x7F)
			l += pogofont_putchar(font, c, x+l, y);
		else
		{
			int mask = 0x20;
			int i = 1;
			// UTF-8
			while(mask & c)
			{
				mask = mask>>1;
				i++;
			}
			
			//printf("unicode %x has %d chars\n", c, i);
			
			c &= (0x3F>>i);
			// l = 1-5
			while(i--)
			{
				c = c << 6;
				c |= ((*str++)&0x3F);
			}
			//printf("unicode %x has %d chars\n", c, i);

			l += pogofont_putchar(font, c, x+l, y);
		}			
	}

	return l;
}

PogoFont *pogofont_dup(PogoFont *font)
{
	PogoFont *f = (PogoFont*)malloc(sizeof(PogoFont));
	memcpy(f, font, sizeof(PogoFont));
	return f;
}

static char *filename(char *name)
{
	char *p;
	if(p = strrchr(name, '/'))
		return p+1;
	else
		return name;
}


PogoFont *pogofont_memload(uchar *mem, char *name)
{
	int size, pix_size, l;
	PogoFont font;
	PogoFont *rfont;

	memcpy(&font, mem, 8);
	//font.width = (mem[6]<<8) | mem[7];

	pix_size = font.width * font.height;
	if(pix_size & 1) pix_size++;

	l = strlen(name);
	if(!(l & 1))
		l++;

	size = sizeof(PogoFont) + l + 1;

	rfont = (PogoFont *)malloc(size);
	memcpy(rfont, &font, 8);

	//fprintf(stderr, "FONT %p %d\n", rfont, size);

	rfont->name = (char *)&rfont[1];
	strcpy(rfont->name, name);

	mem += 8;
		
	if(rfont->flags & FFLG_COLOR)
	{
		rfont->colors = (uint16 *)&mem[2];
		l = *((uint16*)mem);
		printf("%d colors in font\n", l);
		mem += (l*2+4);
	}
	else
		rfont->colors = NULL;
		
	rfont->pixels = mem;
	mem += pix_size;
	
	if(rfont->flags & FFLG_UNICODE)
	{
		int *s = (int *)mem;
		fprintf(stderr, "sequences at %x", mem);
		while(*s >= 0) s++;
		
		rfont->sequences = malloc((int)((uchar*)s - mem + 8));
		memcpy(rfont->sequences, mem, (int)((uchar*)s - mem + 8) );
		
		mem = (uchar *)(s+2);
	}
	
	fprintf(stderr, "offsets at %x", mem);
	if(rfont->flags & FFLG_PROPORTIONAL)
	{
		rfont->offsets = (uint16 *)mem;
		fprintf(stderr, "%d %d %d %d\n", rfont->offsets[0], rfont->offsets[1], rfont->offsets[2], rfont->offsets[3]);
	}
	else
		rfont->offsets = NULL;

	return rfont;
}

static int fsize(FILE *fp)
{
	int rc;
	int pos = ftell(fp);
	fseek(fp, 0, SEEK_END);
	rc = ftell(fp);
	fseek(fp, pos, SEEK_SET);
	return rc;
}

PogoFont *pogofont_load(char *name)
{
	FILE *fp;
	uchar *mem = NULL;
	PogoFont *font = NULL;

	fprintf(stderr, "font %s\n", name);
	
	fp = fopen(name, "rb");
	if(fp)
	{		
#ifdef GBA		
		mem = fseek(fp, 0, SEEK_MEM);
#endif
		fprintf(stderr, "at %p\n", mem);
		if(!mem)
		{
			int len = fsize(fp);
			mem = (uchar *)malloc(len);
			fread(mem, 1, len, fp);
		}
		font = pogofont_memload(mem, filename(name));
		fclose(fp);
		
	}
	return font;
}
