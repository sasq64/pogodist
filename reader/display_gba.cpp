
#include <pogo.h>

#include "pogofont.h"
#include "display.h"

extern "C" {

	void screen_set(int x, int y, int width, int height, int col);
	int screen_setcolors(unsigned int *pal, int start, int size);

	extern int g_screenWidth;
	extern int g_screenHeight;
	extern void *g_screenPtr;

	void filesys_init();
	void deb_init();
	void mbvdeb_init();
	void screen_init();
	void key_init();
	void ssram_init();
	void nand_init();
};



Display::Display(int w, int h)
{
	filesys_init();
	deb_init();
	//mbvdeb_init();

	key_init();
	screen_init();
	ssram_init();
	//nand_init();
	
	w = 240;
	h = 160;
	
	int fd = open("/dev/screen", 0);	
	ioctl(fd, SC_SETMODE, 1);

	int i;
	for(i=0; i<256; i++)
	{
		uchar col[3];		
		col[0] = col[1] = col[2] = 255-i;
		ioctl(fd, SC_SETPAL, &col , i, 1);
	}

	g_screenWidth = m_width = w;
	g_screenHeight = m_height = h;
	g_screenPtr = (void *)malloc(240*160);
	
	//uint32 color = 0xFF0000;
	//screen_setcolors(&color, 0, 1);
	font_setcolor(0xFFFFFF, 0);
	
	
}

void Display::Lock()
{
}

void Display::Unlock()
{
	uint32 *vram = (uint32*)0x06000000;
	uint32 *ptr = (uint32*)g_screenPtr;
	int size = 240*160/4;
	
	while(size--)
		*vram++ = *ptr++;
	
}
	
int Display::Text(int x, int y, char *text, PogoFont **fonts, int *font)
{
	int rc = pogofont_text_multi(fonts, font, text, x, y);
	return rc;
}

void Display::Fill(int col, int x, int y, int w, int h)
{
	if(w == -1)
		w = m_width;
	if(h == -1)
		h = m_height;

	screen_set(x, y, w, h, col);
}

//static bool resize = false;
//int new_width;
//int new_height;

int Display::WaitEvent()
{
	int c = getchar();
	
	if(c == EOF)
		return -1;
	
	switch(c)
	{
	case RAWKEY_UP:
		return 2;
		break;
	case RAWKEY_DOWN:
		return 1;
		break;
	case RAWKEY_RIGHT:
		return 3;
		break;
	case RAWKEY_LEFT:
		return 4;
		break;
	case RAWKEY_L:
		return 5;
		break;
	case RAWKEY_R:
		return 6;
		break;
	case RAWKEY_START:
		return 9;
	case RAWKEY_SELECT:
		return 0;
		break;
	}
	
	return -1;
}
