#ifndef UTIL_H
#define UTIL_H

void WriteInt(FILE *fp, int v);
void WriteWord(FILE *fp, int v);
void WriteByte(FILE *fp, int v);
int ReadInt(FILE *fp);
int ReadWord(FILE *fp);
int ReadByte(FILE *fp);
void ReadString(FILE *fp, char *str);
void WriteString(FILE *fp, const char *str);
char *makeBaseName(char *name, char *dest);
char *NextWord(char *ptr);

#define ASSERT(x) if(!(x)) { fprintf(stderr, "\n***ASSERT: '%s' failed in %s:%d\n", #x, __FILE__, __LINE__); exit(0); }

#endif
