#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL.h>
#include <SDL_ttf.h>

#include "types.h"
#include "pogofont.h"

//Settings

#define BOLD 1
#define ITALIC 2
#define SOLID 4


static int min_minx;
static int max_maxx;
static int min_miny;
static int max_maxy;
static int *old_seq = 0;
static TTF_Font *old_font = 0;

static void GetLimits()
{
	int minx, maxx, miny, maxy;
	int i, advance;
	min_minx = 99;
	max_maxx = 0;
	min_miny = 99;
	max_maxy = 0;
	

	for(i=0; old_seq[i] > 0; i++)
	{
		TTF_GlyphMetrics(old_font, old_seq[i], &minx, &maxx, &miny, &maxy, &advance);
		if(minx < min_minx)
			min_minx = minx;
		
		if((maxx-advance) > max_maxx)
			max_maxx = (maxx-advance);
		
		if(miny < min_miny)
			min_miny = miny;
		if(maxy > max_maxy)
			max_maxy = maxy;
		
	}
}

// Return the lowest x of any character in the sequence
// (Usually a negative number indicating how many pixels that letter
// sticks out over the left border).
int GetFontMinX(TTF_Font *font, int *seq)
{
	if((font != old_font) || (seq != old_seq))
	{
		old_font = font;
		old_seq = seq;
		GetLimits();
	}
	
	return min_minx;
}

// Return the highest x-advance of any character in the sequence
// This indicates how many pixels the letter sticks out over the
// right border
int GetFontMaxAdvanceX(TTF_Font *font, int *seq)
{
	if((font != old_font) || (seq != old_seq))
	{
		old_font = font;
		old_seq = seq;
		GetLimits();
	}
	
	return max_maxx;
}

int GetFontHeight(TTF_Font *font, int *seq)
{
	if((font != old_font) || (seq != old_seq))
	{
		old_font = font;
		old_seq = seq;
		GetLimits();
	}
	
	return max_maxy - min_miny;
}

	
int write32(FILE *fp, int v)
{
	uchar t[4];
	t[0] = v & 0xff;
	t[1] = (v>>8) & 0xff;
	t[2] = (v>>16) & 0xff;
	t[3] = v>>24;
	return fwrite(t, 1, 4, fp);
}

int write16(FILE *fp, int v)
{
	uchar t[2];
	t[0] = v & 0xff;
	t[1] = v>>8;
	return fwrite(t, 1, 2, fp);
}

int write8(FILE *fp, int v)
{
	uchar t = v;
	return fwrite(&t, 1, 1, fp);
}


void ConvertFont(char *fontname, int size, int flags, int *sequence)
{
	if(TTF_Init() >= 0)
	{
		atexit(TTF_Quit);
		
		
		SDL_Color fg = { 0x00, 0x00, 0x00, 0 };
		SDL_Color bg = { 0xFF, 0xFF, 0xFF, 0 };
		
		char ttfname[32];
		sprintf(ttfname, "%s.ttf", fontname);

		printf("%s\n", ttfname);
		
		TTF_Font *font = TTF_OpenFont(ttfname, size);
		
		int style = 0;
		if(flags & BOLD)
		{
			style |= TTF_STYLE_BOLD;
		}
		if(flags & ITALIC)
		{
			style |= TTF_STYLE_ITALIC;
		}
		
		
		TTF_SetFontStyle(font, style);
		
		//SDL_Surface *text = TTF_RenderText_Shaded(font, "anything", fg, bg);
		
		SDL_Rect dst;
		
		int i;
		int minx, maxx, miny, maxy, advance;
		int total = 0;
		//int start = 0x21;
		//int stop = 0x7E;
		
		//SDL_Rect r = {0, 0, m_width, m_height};
		//SDL_FillRect(m_screen, &r, 0xFFFF);
		
		uint16 offsets[256];
		
		int spacing;// = 3;
		int topcut = 0;
		int bottomcut = 0;
		int addx;// = 2;
		//int min_minx = 0;
		//int max_maxx = 0;
		//int min_miny = 99;
		//int max_maxy = 0;
						
		//printf("%d %d %d\n", min_miny, max_maxy, TTF_FontHeight(font));

		addx = -GetFontMinX(font, sequence);
		spacing = addx+GetFontMaxAdvanceX(font, sequence);
			
		// Spacing is how many x pixels extra is needed to store the letters
		// because crossing over left & right borders
			
		for(total=0, i=0; sequence[i] > 0; i++)
		{
			TTF_GlyphMetrics(font, sequence[i], &minx, &maxx, &miny, &maxy, &advance);
			offsets[i] = total;
			//printf("%d\n", offsets[i-start]);
			total += (advance+spacing);			
		}
		
		int count = i;

		printf("%d %d\n", addx, spacing);
		
		offsets[i] = total;
		
		total = ((total+3) & 0xFFFFFFFC);
		
		int fontheight = GetFontHeight(font, sequence);//max_maxy - min_miny; //TTF_FontHeight(font) - topcut - bottomcut;
						
		SDL_Surface *fontsurf = SDL_CreateRGBSurface(SDL_SWSURFACE, total, fontheight, 8, 0, 0, 0, 0);
		
		//SDL_Rect r = {0, 0, m_width, m_height};
		SDL_FillRect(fontsurf, NULL, 0x00);

		SDL_Color col;
		
		for(i=0; i<256; i++)
		{
			col.r = col.b = col.g = (255-i);			
			SDL_SetColors(fontsurf, &col, i, 1);
		}
		
		int xpos = 0;
		//int ypos = TTF_FontAscent(font) - topcut;
		
		for(i=0; sequence[i] > 0; i++)
		{
			TTF_GlyphMetrics(font, sequence[i], &minx, &maxx, &miny, &maxy, &advance);

			SDL_Surface *letter;
			if(flags & SOLID)
				letter = TTF_RenderGlyph_Solid(font, sequence[i], fg);
			else
				letter = TTF_RenderGlyph_Shaded(font, sequence[i], fg, bg);

			//printf("%02x %c  X:%d->%d/%d, Y:%d->%d/%d  A:%d\n", i, i, minx, maxx, letter->w, miny, maxy, letter->h, advance);
			
			dst.x = xpos + minx + addx;
			dst.y = max_maxy - maxy;
			
			if(dst.y < 0)
			{
				printf("%d %d !!!!!\n", maxy, max_maxy);
			}
	
			dst.w = letter->w;
			dst.h = letter->h;

			xpos += (advance+spacing);
			
			SDL_BlitSurface(letter, NULL, fontsurf, &dst);
			SDL_FreeSurface(letter);
		}
				
		//SDL_SaveBMP(fontsurf, "font.bmp");
		
		PogoFont pfont;
		
		TTF_GlyphMetrics(font, 32, &minx, &maxx, &miny, &maxy, &advance);

		pfont.width = total;
		pfont.charwidth = advance;
		pfont.height = fontheight;
		pfont.spacing = -spacing;

		pfont.first = sequence[0];
		pfont.last = sequence[count-1];
		
		printf("From %d->%d\n", pfont.first, pfont.last);

		pfont.flags = FFLG_PROPORTIONAL | FFLG_COLOR | FFLG_TRANSP | FFLG_UNICODE;
		
		char outname[64];
		
		sprintf(outname, "%s%d%s.bmp", fontname, size);
		SDL_SaveBMP(fontsurf, outname);

		sprintf(outname, "%s%d%s%s.font", fontname, size,
				flags & BOLD ? "b" : "",
				flags & ITALIC ? "i" : ""
				);
		
		FILE *fp = fopen(outname, "wb");
		
		//uint16 cc = 0;
		
		fwrite(&pfont, 1, 6, fp);
		write16(fp, pfont.width);
		write16(fp, 0);
		write16(fp, 0);

		SDL_LockSurface(fontsurf);
		fwrite(fontsurf->pixels, total, fontheight, fp);
		SDL_UnlockSurface(fontsurf);
		
		//int newseq[48];
		//int *nsp = newseq;
		
		for(i=0; sequence[i] > 0;)
		{
			//*nsp++ = sequence[i];
			write32(fp, sequence[i]);
			while(sequence[i+1] - sequence[i] == 1) i++;
			//*nsp++ = sequence[i++];
			write32(fp, sequence[i++]);
		}
		
		write32(fp, -1);
		write32(fp, -1);
		//*nsp++ = -1;
		//*nsp++ = -1;


		int j;
		//fwrite(newseq, sizeof(int), nsp-newseq, fp);		
		//fwrite(offsets, 1, (i+1)*2, fp);
		for(j=0; j<i+1; j++)
		{
			write16(fp, offsets[j]);
		}
		
		fclose(fp);
/*		
		TTF_SetFontStyle(font, TTF_STYLE_ITALIC);
		SDL_Surface *text = TTF_RenderText_Shaded(font, "anything", fg, bg);
		dst.x = 0;
		dst.y = yyy;
		yyy += 16;
		dst.w = text->w;
		dst.h = text->h;
		SDL_BlitSurface(text, NULL, m_screen, &dst);
		SDL_FreeSurface(text);
		SDL_UpdateRect(m_screen, 0, 0, m_width, m_height);
*/
	}
}

int main(int argc, char **argv)
{
	int bpp = 16;
	int video_flags = 0;	

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_NOPARACHUTE) < 0)
	//if(SDL_Init(SDL_INIT_NOPARACHUTE) < 0)
	{
		fprintf(stderr,"Couldn't initialize SDL: %s\n",SDL_GetError());
		exit(1);
	}
/*
	video_flags = 0;//SDL_HWSURFACE | SDL_DOUBLEBUF;

	if((m_screen = SDL_SetVideoMode(w, h, bpp, video_flags)) == NULL)
	{
		fprintf(stderr, "Couldn't set mode: %s\n", SDL_GetError());
		SDL_Quit();
		exit(1);
	}
	
	g_screenWidth = m_width = w;
	g_screenHeight = m_height = h;

	font_setcolor(0, 0xFFFFFF);
*/	
	int i;
	int flags = 0;
	int size = 12;

	if(argc < 3)
	{
		printf("Usage: %s <ttfname> <size> [-s] [-b] [-i]\n", argv[0]);
		return 0;
	}
	
	char *name = argv[1];
	size = atoi(argv[2]);

	for(i=3; i<argc; i++)
	{
		if(argv[i][0] == '-')
		{
			switch(argv[i][1])
			{
			case 's':
				flags |= SOLID;
				break;
			case 'b':
				flags |= BOLD;
				break;
			case 'i':
				flags |= ITALIC;
				break;
			}
		}
	}
	
	int sequence[1024];
	int *s = sequence;
	
	for(i=0x20; i<=0x7F; i++)
		*s++ = i;
	for(i=0xC0; i<=0xFF; i++)
		*s++ = i;
	for(i=0x2013; i<=0x2014; i++)
		*s++ = i;
	
	*s++ = -1;
	*s++ = -1;
	
	ConvertFont(name, size, flags, sequence);
	
	return 0;	
}
