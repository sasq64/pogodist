#ifndef POGOFONT_H
#define POGOFONT_H

typedef struct {
	uchar flags;
	signed char spacing;
	uchar first;
	uchar last;
	uchar charwidth;	// Only for space in proportional fonts
	uchar height;		// Height in pixels
	uint16 width;		// Real width of entire font in pixels - actual planar data is aligned up to nearest 32

	/* Non-disk part */
	char *name;
	uint16 *colors;
	uchar *pixels;		// Chunky data, width*height bytes
	uint16 *offsets;	// NULL for monospaced font
	int *sequences;
} PogoFont;

/* Disk format

	- Font structure (First part)
	- width*height/8 bytes planar pixels
	- (last-first) 16bit offsets
*/

#define FFLG_PROPORTIONAL 1
#define FFLG_HIFONT 2
#define FFLG_COLOR 4
#define FFLG_TRANSP 8
#define FFLG_BOLD 16
#define FFLG_ITALIC 32
#define FFLG_UNICODE 64

#ifdef __cplusplus
extern "C" {
#endif

uchar pogofont_putchar(PogoFont *font, int c, int x, int y);
int pogofont_text(PogoFont *font, char *str, int x, int y);
int pogofont_text_multi(PogoFont **fontlist, int *current, char *str, int x, int y);

//uchar pogofont_putchar(PogoFont *font, char c, uchar *dest, int width);
//int pogofont_text(PogoFont *font, char *str, uchar *dest, int width);
//int pogofont_text_multi(PogoFont **fontlist, int *current, char *str, uchar *dest, int width);
void pogofont_setcolor(uint32 fg, uint32 bg);
PogoFont *pogofont_load(char *name);
PogoFont *pogofont_memload(uchar *mem, char *name);
PogoFont *pogofont_dup(PogoFont *font);

#ifdef __cplusplus
}
#endif
	
#endif
