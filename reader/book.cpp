
#ifdef GBA

#include <pogo.h>

#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#endif

#include "types.h"
#include "util.h"

#include "normaltext.h"
#include "palmtext.h"
#include "chunkfile.h"
#include "itextstream.h"
#include "textrenderer.h"

#include "book.h"

class BookStream : public ITextStream
{
public:
	BookStream(ChunkFile *file, int size);

	int Size();
	int Seek(int offset);
	int Tell();
	//int Read(char *dest, int size);
	char *ReadLine(char *dest = 0, int size = -1);

private:
	ChunkFile *m_file;
	int m_size;
	int m_start;
	char m_linebuf[8192];
};

BookStream::BookStream(ChunkFile *file, int size)
{
	m_size = size;
	m_file = file;
}

int BookStream::Seek(int offset)
{
	return m_file->Seek(offset);
}

int BookStream::Tell()
{
	return m_file->Tell();
}

int BookStream::Size()
{
	return m_size;
}

char *BookStream::ReadLine(char *dest, int size)
{

	if(!dest)
	{
		dest = m_linebuf;
		size = sizeof(m_linebuf);
	}
		
	return m_file->ReadLine(dest, size);
}


ITextStream *Book::ReadSection(int i)
{
	if(m_type == BOOK)
	{
		m_file->Seek(m_textPos[i]);
		return new BookStream(m_file, m_textSize[i]);
	}
	else
	if(m_type == TEXT)
	{
		return new NormalText(m_fileName);
	}
	else
	if(m_type == PALM)
	{
		return new PalmText(m_fileName);
	}
};


Book::Book(char *fname)
{	
	m_fileName = fname;
	strcpy(m_title, "");
	
	char *ext = strrchr(fname, '.');
	
	if(strcmp(ext, ".txt") == 0)
	{
		m_type = TEXT;
		m_valid = true;		
		return;
	}

	if(strcmp(ext, ".pdb") == 0)
	{
		m_type = PALM;
		m_valid = true;		
		return;
	}

	m_file = new ChunkFile(fname, ChunkFile::READ);
	
	m_textCount = 0;
	m_valid = false;
	
	if(!m_file->Ok())
		return;
	
	m_type = BOOK;
	
	if(m_file->OpenChunk("BOOK"))
	{
		unsigned int magic = 0;
		if(m_file->OpenChunk("MAGI"))
		{
			magic = m_file->ReadInt();
			m_file->CloseChunk();
		}
		
		if(magic == EBOOK_MAGIC)
		{
			m_valid = true;
			if(m_file->OpenChunk("HEAD"))
			{
				if(m_file->OpenChunk("INFO"))
				{
					m_file->Read(m_title);
					m_file->CloseChunk();
				}
				m_file->CloseChunk();
			}
			
			while(m_file->OpenChunk("TEXT"))
			{
				//m_text[m_textCount] = (char *)malloc(m_file->ChunkSize()+1);
				//m_file->Read(m_text);
				//m_text[m_file->ChunkSize()] = 0;
				//m_file->CloseChunk();
				
				m_textPos[m_textCount] = m_file->Tell();
				m_textSize[m_textCount] = m_file->ChunkSize();
				m_textCount++;
			}
		}
		
		m_file->CloseChunk();
	}
}
