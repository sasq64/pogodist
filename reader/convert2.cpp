#include <stdio.h>
#include <stdlib.h>
//#include <string.h>

#include <string>
#include <iostream>

using namespace std;

#include "bookformat.h"
#include "chunkfile.h"


void utf8(int c, string &output)
{	
	if(c <= 0x7F)
		output += (char)c;
	else
	if(c <= 0x7ff)
	{
		output += (char)(0xC0 | (c>>6));
		output += (char)(0x80 | (c&0x3f));
		//printf("unicode %x %c\n", c, c);
	}
	else
	if(c <= 0xffff)
	{
		output += (char)(0xE0 | (c>>12));
		output += (char)(0x80 | ((c>>6)&0x3f));
		output += (char)(0x80 | (c&0x3f));
		//printf("unicode %x %c\n", c, c);
	} 
}


char *loadfile(char *name)
{
	int size;
	char *ptr = NULL;
	FILE *fp = fopen(name, "rb");
	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		size = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		ptr = (char *)malloc(size+1);
		fread(ptr, 1, size, fp);
		fclose(fp);
		ptr[size] = 0;
	}
	return ptr;
}

int savefile(const char *name, const char *ptr, int size = 0)
{
	FILE *fp = fopen(name, "wb");
		
	if(fp)
	{
		if(!size)
			size = strlen(ptr);
		fwrite(ptr, 1, size, fp);
		fclose(fp);
		return size;
	}
	return 0;
}





class Hashtable
{
public:
	Hashtable();
	~Hashtable();
	int& operator[](string s);
	int size();
	int first();
	int next();

private:
	struct HashEntry
	{
		int val;
		struct HashEntry *next;
		char str[0];
	};
	
	HashEntry *symbols[41];
	int m_size;
	
	HashEntry *current;
	int cindex;
	
	unsigned int hash(const char *str);

};

unsigned int Hashtable::hash(const char *str)
{
	unsigned int hash = 0;
	while(*str)
		hash = ((hash<<5)^(hash>>27))^*str++;

	return hash;
}

Hashtable::Hashtable()
{
	m_size = 41;
	memset(symbols, 0, m_size * sizeof(void *));
}

Hashtable::~Hashtable()
{
	HashEntry *he, *he2;
	int i;

	for(i=0; i<m_size; i++)
	{
		he = symbols[i];
		while(he)
		{
			he2 = he->next;			
			free(he);			
			he = he2;
		}
	}
}

int Hashtable::next()
{
	if(current)
		current = current->next;
	
	while(cindex < m_size && !current)
		current = symbols[cindex++];
	
	if(current)
		return current->val;
	else
		return 0;
}

int Hashtable::first()
{
	current = 0;
	cindex = 0;
	return next();
}


int Hashtable::size()
{
	HashEntry *he;
	int i;
	int size = 0;

	for(i=0; i<m_size; i++)
	{
		he = symbols[i];
		while(he)
		{
			size++;
			he = he->next;			
		}
	}
	return size;
}

int& Hashtable::operator[](string name)
{
	HashEntry **he;
	int i = hash(name.c_str()) % m_size;

	he = &symbols[i];
	while(*he && strcmp((*he)->str, name.c_str()) != 0)
		he = &((*he)->next);

	if(!(*he))
	{
		*he = (HashEntry *)malloc(sizeof(HashEntry) + name.size() + 1);
		strcpy((*he)->str, name.c_str());
		(*he)->val = 0;
		(*he)->next = 0;
	}
	
	return (*he)->val;

}

////////////////////////////////////////////////////////////////////////

class Tag
{
public:
	
	Tag(string);
	~Tag();
	
	string Name() { return name; }
	bool IsDual() { return m_dual; }
	
	bool ArgExists(string arg) { return (hash[arg] =! 0); }	
	string *GetStringArg(string arg) { return (string *)hash[arg]; }
	int GetIntArg(string arg) 
	{ 
		return atoi( ((string *)hash[arg])->c_str() );
	}

private:
	Hashtable hash;
	string name;
	bool m_dual;
};

Tag::Tag(string text)
{
	int i = 0;
	int c;
	string tmp;
	char dummy[2];
	char endchar;
	
	dummy[1] = 0;
			
	c = 0;
	
	m_dual = false;
	
	if(text[text.size()-1] == '/')
	{
		text.resize(text.size()-1);
		m_dual = true;
	}
	
	for(i=0; i<text.size();)
	{
		c = text[i++];
		
		tmp.resize(0);
		
		/*if(c == '"')
		{
			endchar = '"';
			c = text[i++];
		}
		else*/
			endchar = ' ';

		while(i<=text.size() && c != endchar)
		{
			*dummy = tolower(c);
			tmp.append(dummy);
			c = text[i++];
		}

		//printf("tag '%s'\n", tmp.c_str());

		if(name.empty())
		{
			if((tmp.length() == 2) && (tmp[0] == 'h') && (isdigit(tmp[1])))
			{
				name = string("h");
				hash["size"] = (int)new string(tmp.substr(1,1));
			}
			else
			if((tmp.length() == 3) && (tmp.substr(0,2).compare("/h") == 0))
			{
				name = string("/h");
				hash["size"] = (int)new string(tmp.substr(1,1));
			}
			else
				name = tmp;

			printf("tag '%s'\n", name.c_str());			
		}
		else		
		{
			int v = tmp.find('=');
			if(v != string::npos)
			{
				int start = v+1;
				int len = tmp.size()-start-1;
				
				//printf("-%c-\n", tmp[start+len]);
				
				if(tmp[start] == '\"') {start++; len--;}
				if(tmp[start+len] == '\"') len--;
				
				printf("Got '%s'='%s'\n", tmp.substr(0,v).c_str(), tmp.substr(start, len+1).c_str());
				hash[tmp.substr(0, v)] = (int)new string(tmp.substr(start, len+1));
			}
			else
			{
				hash[tmp] = (int)new string("__DEFINED__");
			}
				
		}
	}
	
}

Tag::~Tag()
{
	string *s = (string *)hash.first();
	
	while(s)
	{
		delete s;
		s = (string *)hash.next();
	}
}


////////////////////////////////////////////////////////////////////////
class TagHandler
{
public:
	virtual void HandleTag(Tag &tag, string &output) = 0;
};

class CharHandler
{
public:
	virtual bool HandleChar(int c, string &output) = 0;
};

class SpecialHandler
{
public:
	virtual bool HandleSpecial(string name, string &output) = 0;
};

class HtmlParser
{
public:
	HtmlParser();
	bool Parse(string);
	string *GetOutput() { return &output;	}
	void RegisterTagHandler(string tagname, TagHandler *handler);
	void SetCharHandler(CharHandler *handler) { m_defaultHandler = handler; }
	void SetSpecialHandler(SpecialHandler *handler) { m_specialHandler = handler; }
	
private:
	bool HandleTag(string tagname);	
	bool HandleSpecial(string name);
	Hashtable m_handlerHash;
	string output;
	CharHandler *m_defaultHandler;
	SpecialHandler *m_specialHandler;
	
};

/////////////////////



HtmlParser::HtmlParser() : 
m_defaultHandler(0),
m_specialHandler(0)
{
}


void HtmlParser::RegisterTagHandler(string tagname, TagHandler *handler)
{
	m_handlerHash[tagname] = (int)handler;
}

bool HtmlParser::HandleSpecial(string name)
{
	//cout << "special " << name << endl;
	if(!m_specialHandler || m_specialHandler->HandleSpecial(name, output))
	{
	}
}

bool HtmlParser::HandleTag(string tagname)
{
	
	Tag tag(tagname);

	//cout << "Handling \"" << tagname << "\" (" << tag.Name() << ")" << endl;
	
	TagHandler *handler = (TagHandler *)m_handlerHash[tag.Name()];
	if(handler)
	{
		//cout << "Handler found" << endl;
		handler->HandleTag(tag, output);
		
		if(tag.IsDual())
		{
			string slashname = string("/").append(tag.Name());
			printf("Dual tag %s\n", slashname.c_str());
			Tag tag2(slashname);
			handler = (TagHandler *)m_handlerHash[slashname];
			if(handler)
				handler->HandleTag(tag2, output);
		}
		
		return true;
	}
	
	
	
	cout << "No handler found for " << tag.Name() << endl;
	return false;
}

bool HtmlParser::Parse(string html)
{	
	output = "";
	
	for(int i=0; i<html.size(); i++)
	{
		switch(html[i])
		{
		case '<':
			{
				int start = ++i;
				while(html[i] != '>') i++;
				HandleTag(html.substr(start, i-start));
			}
			break;
		case '&':
			{
				int start = ++i;
				while(html[i] != ';') i++;
				HandleSpecial(html.substr(start, i-start));
			}
			break;
			
		default:
			if(!m_defaultHandler || m_defaultHandler->HandleChar((unsigned char)html[i], output))
			{
				output += html[i];
				printf(">>%c<<\n", html[i]);
			}
		}
	}
	
	return true;
}





////////////////////////////////////////////////////
//**********************************************************************
////////////////////////////////////////////////////////////////////////////////////////////






class ParseState
{
public:
	int m_inParagraph;
	int m_flags;
	int m_oldFlags;
	int m_fontSize;
	bool m_newParagraph;
	bool m_lineFeed;
	int m_spaces;
	int m_alignment;
	int m_indent;
	int m_lastIndent;
	bool m_newChapter;
	bool m_newBook;
	bool m_noSpaces;
	bool m_killChars;
	int m_lf;
};

ParseState state;

void HandleFlags(string &output)
{
	if(state.m_flags != state.m_oldFlags)
	{
		char font;
		
		if(state.m_flags & FLG_FIXED)
			font = FIXED;
		else
		if(state.m_flags & FLG_BOLD)
			font = BOLD;
		else
		if(state.m_flags & FLG_ITALIC)
			font = ITALIC;
		else
			font = NORMAL;
		
		output += font;
		
		//cout << "Setting font to " << (int)font << endl;
		
		state.m_oldFlags = state.m_flags;
	}

}

void HandleState(string &output)
{
	
	if(state.m_lineFeed)
	{
		output += 10;
		
		if(state.m_newBook)
		{
			output += 11;
			output += BC_BOOK;
			state.m_newBook = false;
		}
		
		if(state.m_newChapter)
		{
			output += 11;
			output += BC_CHAPTER;
			state.m_newChapter = false;
		}

		
		if(state.m_alignment != BC_LEFTALIGN)
		{
			output += 11;
			output += state.m_alignment;
			cout << "Setting align to " << state.m_alignment << endl;
		}
		
		if(state.m_fontSize != BC_NORMALFONT)
		{
			output += 11;
			output += state.m_fontSize;
			cout << "Setting size to " << state.m_fontSize << endl;
		}
		
		if(state.m_indent != state.m_lastIndent)
		{
			output += 11;
			output += state.m_indent+1;
			state.m_lastIndent = state.m_indent;
		}
		
		if(state.m_newParagraph)
			output += 9;
		
		state.m_newParagraph = state.m_lineFeed = false;
		
	}
	else
	{
		if(state.m_spaces)
			output += ' ';
	}
	
	HandleFlags(output);
	
	state.m_spaces = 0;
}


class MySpecialHandler : public SpecialHandler
{
public:
	virtual bool HandleSpecial(string name, string &output)
	{
		HandleState(output);
		if(name[0] == '#')
		{
			int l = atoi(&(name.c_str())[1]);
			if(l > 0)
			{
				if(l == 151)
					l = 0x2014;
				if(l == 146)
					l = '\'';

				//printf("Adding %d as unicode\n", l);
				if(l == 169)
					output.append("(C)");
				else
					utf8(l, output);
			}
		}
		else
		if(name.compare("nbsp") == 0)
			output += ' ';
		else
		if(name.compare("rsquo") == 0)
			output += '\'';
		else
		if(name.compare("rdquo") == 0)
			output += '\"';
		else
		if(name.compare("ldquo") == 0)
			output += '\"';
		else
		if(name.compare("mdash") == 0)
			utf8(0x2014, output);
		else
		if(name.compare("amp") == 0)
			output += '&';
		else
		if(name.compare("gt") == 0)
			output += '>';
		else
		if(name.compare("lt") == 0)
			output += '<';
		else
		if(name.compare("trade") == 0)
			utf8(0x2122, output);
		
		state.m_noSpaces = false;
	}
};


class MyCharHandler : public CharHandler
{
public:
	virtual bool HandleChar(int c, string &output)
	{
		switch(c)
		{
		case 9:
		case 10:
			if(state.m_flags & FLG_FIXED)
			{
				//state.m_lf++;
				//if(state.m_lf == 2)
				//{
				//	state.m_lf = 0;
					output += 10;
					state.m_spaces = -1;
				//}
				//break;
			}
		case 13:
		case ' ':
			if(!state.m_noSpaces)
				state.m_spaces++;
			break;
		default:
			if(!state.m_killChars)
			{
				HandleState(output);
				
				if(c == 151)
				{
					printf("FOUND HYPHEN\n");
					c = 0x2014;
				}
				if(c == 146)
					c = '\'';

				utf8(c, output);
				state.m_noSpaces = false;
			}
			break;
		}
		
		return false;
	}
};


class ParagraphHandler : public TagHandler
{
public:
	virtual void HandleTag(Tag &tag, string &output)
	{
		state.m_lineFeed = true;
		state.m_spaces = 0;
		state.m_alignment = BC_LEFTALIGN;
		state.m_fontSize = BC_NORMALFONT;		
		state.m_noSpaces = true;

		if(tag.Name()[0] == '/')
		{
			if(tag.Name().compare("/blockquote") == 0)
			{
				output += 10;
				state.m_indent--;
			}
			else
			if(tag.Name().compare("/pre") == 0)
			{
				output += 10;
				state.m_flags &= ~FLG_FIXED;
			}
			state.m_inParagraph--;
		}
		else
		{
			state.m_inParagraph++;
		
			if(tag.Name().compare("pre") == 0)
			{
				HandleFlags(output);
				output += 10;
				state.m_flags |= FLG_FIXED;
			}
			else
			if(tag.Name().compare("blockquote") == 0)
			{
				HandleFlags(output);
				output += 10;
				state.m_indent++;
			}
			else
			if(tag.Name().compare("p") == 0)
			{	
				state.m_newParagraph = true;
			}
			else
			if(tag.Name().compare("h") == 0)
			{
				int size = tag.GetIntArg("size");
				
				cout << "Header size " << size << endl;
				
				if(size == 2)
					state.m_newChapter = true;
				/*else
				if(size == 4)
					state.m_newBook = true;*/
				
				if(size <= 2)
					state.m_fontSize = BC_BIGFONT;
				else
					state.m_fontSize = BC_MEDIUMFONT;
			}
		

			string *a = tag.GetStringArg("align");
		
			if(a)
			{
				if(a->compare("right") == 0)
					state.m_alignment = BC_RIGHTALIGN;
				else
					if(a->compare("center") == 0)
						state.m_alignment = BC_CENTERALIGN;
			}

		}
	}
};

class FontStyleHandler : public TagHandler
{
public:
	virtual void HandleTag(Tag &tag, string &output)
	{
		string name = tag.Name();
		
		if((name.compare("b") == 0) || (name.compare("strong") == 0))
			state.m_flags |= FLG_BOLD;
		else
		if((name.compare("/b") == 0) || (name.compare("/strong") == 0))
			state.m_flags &= ~FLG_BOLD;
		else
		if((name.compare("tt") == 0) || (name.compare("kbd") == 0))
			state.m_flags |= FLG_FIXED;
		else
		if((name.compare("/tt") == 0) || (name.compare("/kbd") == 0))
			state.m_flags &= ~FLG_FIXED;
		else
		if((name.compare("i") == 0) || (name.compare("em") == 0))
			state.m_flags |= FLG_ITALIC;
		else
		if((name.compare("/i") == 0) || (name.compare("/em") == 0))
			state.m_flags &= ~FLG_ITALIC;
	}
};

class MarkerHandler : public TagHandler
{
public:
	virtual void HandleTag(Tag &tag, string &output)
	{
		string name = tag.Name();
		
		if(name.compare("br") == 0)
		{
			state.m_lineFeed = true;
			//state.m_newParagraph = true;
			//output += 10;
			state.m_noSpaces = true;
		}
		else
		if(name.compare("li") == 0)
		{
			output += 10;
			output.append("o ");
		}
		else
		if(name.compare("hr") == 0)
		{
			HandleFlags(output);
			
			/*if(tag.GetStringArg("noshade"))
			{
				output += 10;
				output += 11;
				output += BC_CENTERALIGN;
				output.append("* * *");
				state.m_lineFeed = true;
				state.m_noSpaces = true;
			}
			else*/
			{
				output += 10;
				state.m_lineFeed = true;
				state.m_noSpaces = true;
			}
		}
		
	}
};

class KillHandler : public TagHandler
{
public:
	virtual void HandleTag(Tag &tag, string &output)
	{
		if(tag.Name()[0] == '/')
			state.m_killChars = false;
		else
			state.m_killChars = true;
	}
};



bool html2book(string &input, string &output)
{
	
	memset(&state, 0, sizeof(state));
	
	state.m_alignment = BC_LEFTALIGN;
	state.m_fontSize = BC_NORMALFONT;
	
	HtmlParser parser;

	ParagraphHandler ph;
	FontStyleHandler fh;
	MarkerHandler mh;
	KillHandler kh;
	MyCharHandler ch;
	MySpecialHandler sh;

	cout << "Registering handlers" << endl;

	parser.SetSpecialHandler(&sh);
	parser.SetCharHandler(&ch);
	parser.RegisterTagHandler("p", &ph);
	parser.RegisterTagHandler("/p", &ph);
	parser.RegisterTagHandler("blockquote", &ph);
	parser.RegisterTagHandler("/blockquote", &ph);
	parser.RegisterTagHandler("h", &ph);
	parser.RegisterTagHandler("/h", &ph);
	parser.RegisterTagHandler("ul", &ph);
	parser.RegisterTagHandler("/ul", &ph);
	parser.RegisterTagHandler("dl", &ph);
	parser.RegisterTagHandler("/dl", &ph);
	parser.RegisterTagHandler("pre", &ph);
	parser.RegisterTagHandler("dt", &ph);
	parser.RegisterTagHandler("/dt", &ph);
	parser.RegisterTagHandler("/pre", &ph);

	parser.RegisterTagHandler("b", &fh);
	parser.RegisterTagHandler("/b", &fh);
	parser.RegisterTagHandler("strong", &fh);
	parser.RegisterTagHandler("/strong", &fh);
	parser.RegisterTagHandler("i", &fh);
	parser.RegisterTagHandler("/i", &fh);
	parser.RegisterTagHandler("tt", &fh);
	parser.RegisterTagHandler("/tt", &fh);
	parser.RegisterTagHandler("kbd", &fh);
	parser.RegisterTagHandler("/kbd", &fh);
	parser.RegisterTagHandler("em", &fh);
	parser.RegisterTagHandler("/em", &fh);

	parser.RegisterTagHandler("br", &mh);
	parser.RegisterTagHandler("hr", &mh);
	parser.RegisterTagHandler("li", &mh);

	//parser.RegisterTagHandler("a", &kh);
	//parser.RegisterTagHandler("/a", &kh);
	parser.RegisterTagHandler("title", &kh);
	parser.RegisterTagHandler("/title", &kh);

	
	parser.Parse(input);
	
	output = *parser.GetOutput();
	
	return true;

}


#define EBOOK_MAGIC 0xDA0EB00C

int main(int argc, char **argv)
{
	char *title = "";
	char *author = "";
	//Hashtable hash;
	//hash["TEST"] = 5;
	//printf("%d %d\n", hash["TEST"], hash["BAJS"]);
	//return 0;
	
	char *html = loadfile(argv[1]);
	
	string input = string(html);
	string output;
	
	html2book(input, output);
	
	ChunkFile *f = new ChunkFile(argv[2], ChunkFile::WRITE);
	
	f->OpenChunk("BOOK");

	f->OpenChunk("MAGI");
	f->Write(EBOOK_MAGIC);
	f->CloseChunk();

	f->OpenChunk("HEAD");
	
	f->OpenChunk("INFO");	
	f->Write(title);
	f->Write(author);
	f->CloseChunk();
	
	f->CloseChunk();

	f->OpenChunk("TEXT");
	//f->Write("This is the actual contents of\nthe document!\n\n--Sasq\n");

	f->Write(output.c_str());
	
	f->CloseChunk();

	f->CloseChunk();
	
	delete f;
	
	return 0;
}
