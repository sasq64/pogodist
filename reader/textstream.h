
class ITextStream
{
public:
	//ITextStream(char *fname);
	virtual int Size() = 0;
	virtual int Seek(int offset) = 0;
	virtual int Tell() = 0;
	virtual char *ReadLine(char *dest = 0, int size = -1) = 0;
	
};


class NormalText : public ITextStream
{
public:
	NormalText(char *fname);
	int Size();
	int Seek(int offset);
	int Tell();
	char *ReadLine(char *dest = 0, int size = -1);

private:
	FILE *m_fp;
	int m_size;
	char m_linebuf[8192];

};

NormalText::NormalText(char *fname)
{
	m_fp = fopen(fname, "rb");
	if(m_fp)
	{
		fseek(m_fp, 0, SEEK_END);
		m_size = ftell(m_fp);
		fseek(m_fp, 0, SEEK_SET);
	}
}

void NormalText::Seek(int offset)
{
	fseek(m_fp, offset, SEEK_SET);
}

int NormalText::Tell()
{
	return ftell(m_fp);
}

int NormalText::Size()
{
	return m_size;
}

char *NormalText::ReadLine(char *dest, int size)
{
	if(!dest)
	{
		dest = m_linebuf;
		size = sizeof(m_linebuf);
	}
	fgets(dest, size, m_fp);
	return dest;
}
