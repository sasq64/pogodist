#include <stdio.h>
#include <stdlib.h>
//#include <string.h>

#include <string>
#include <iostream>

using namespace std;


char *loadfile(char *name)
{
	int size;
	char *ptr = NULL;
	FILE *fp = fopen(name, "rb");
	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		size = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		ptr = (char *)malloc(size+1);
		fread(ptr, 1, size, fp);
		fclose(fp);
		ptr[size] = 0;
	}
	return ptr;
}

int savefile(const char *name, const char *ptr, int size = 0)
{
	FILE *fp = fopen(name, "wb");
		
	if(fp)
	{
		if(!size)
			size = strlen(ptr);
		fwrite(ptr, 1, size, fp);
		fclose(fp);
		return size;
	}
	return 0;
}





class Hashtable
{
public:
	Hashtable();
	~Hashtable();
	int& operator[](string s);
	int size();
	int first();
	int next();

private:
	struct HashEntry
	{
		int val;
		struct HashEntry *next;
		char str[0];
	};
	
	HashEntry *symbols[41];
	int m_size;
	
	HashEntry *current;
	int cindex;
	
	unsigned int hash(const char *str);

};

unsigned int Hashtable::hash(const char *str)
{
	unsigned int hash = 0;
	while(*str)
		hash = ((hash<<5)^(hash>>27))^*str++;

	return hash;
}

Hashtable::Hashtable()
{
	m_size = 41;
	memset(symbols, 0, m_size * sizeof(void *));
}

Hashtable::~Hashtable()
{
	HashEntry *he, *he2;
	int i;

	for(i=0; i<m_size; i++)
	{
		he = symbols[i];
		while(he)
		{
			he2 = he->next;			
			free(he);			
			he = he2;
		}
	}
}

int Hashtable::next()
{
	if(current)
		current = current->next;
	
	while(cindex < m_size && !current)
		current = symbols[cindex++];
	
	if(current)
		return current->val;
	else
		return 0;
}

int Hashtable::first()
{
	current = 0;
	cindex = 0;
	return next();
}


int Hashtable::size()
{
	HashEntry *he;
	int i;
	int size = 0;

	for(i=0; i<m_size; i++)
	{
		he = symbols[i];
		while(he)
		{
			size++;
			he = he->next;			
		}
	}
	return size;
}

int& Hashtable::operator[](string name)
{
	HashEntry **he;
	int i = hash(name.c_str()) % m_size;

	he = &symbols[i];
	while(*he && strcmp((*he)->str, name.c_str()) != 0)
		he = &((*he)->next);

	if(!(*he))
	{
		*he = (HashEntry *)malloc(sizeof(HashEntry) + name.size() + 1);
		strcpy((*he)->str, name.c_str());
		(*he)->val = 0;
		(*he)->next = 0;
	}
	
	return (*he)->val;

}

////////////////////////////////////////////////////////////////////////

class TextParser
{
public:
	TextParser(string s)
	{
		m_ptr = s.c_str();
	}
		
	bool NextToken();

	bool IsUpper() { return m_isUpper; }
	bool IsUnderscored() { return m_isUnderScored; }
	bool IsWord() { return m_type == WORD; }
	bool IsDelimeter() { return m_type == DELIMITER; }
	bool IsLine() { return m_type == LINE; }
	bool IsSpace() { return m_type == SPACE; }
	bool Done() { return *m_ptr == 0; }
	
	string Text() { return m_word; }

private:
	
	enum { NONE, SPACE, LINE, WORD, DELIMITER };
	int m_type;
	bool m_isUpper;
	bool m_isUnderScored;
	const char *m_ptr;
	string m_word;
};

bool TextParser::NextToken()
{
	//char *m_ptr = &(text.c_str()[index]);
		
	if(!(*m_ptr))
		return false;
		
	char c = *m_ptr++;
	
/*	m_isUnderScored = false;
		
	if(c == '_')
	{
		printf("---\n");
		m_isUnderScored = true;
		c = *m_ptr++;
	}
*/	
	if(isalnum(c))
	{
		char tmp[128];
		m_type = WORD;
		const char *start = m_ptr-1;
		
		m_isUpper = isupper(c);
		
		while(isalpha(*m_ptr))
		{
			m_isUpper = m_isUpper & isupper(*m_ptr);
			m_ptr++;
		}
		strncpy(tmp, start, m_ptr-start);
		tmp[m_ptr-start] = 0;
		m_word = string(tmp);
	}
	else
	if(c == ' ' || c == 9)
	{
		m_type = SPACE;
		m_word = " ";
	}
	else
	if(c == 13)
	{
		if(*m_ptr == 10)
			*m_ptr++;
		m_type = LINE;
		m_word = "\n";
	}
	else		
	if(c == 10)
	{
		m_type = LINE;
		m_word = "\n";
	}
	else
	{						
		m_type = DELIMITER;
		m_word.clear();
		m_word += c;
	}
		
/*	if(*m_ptr == '_')
	{
		//m_isUnderScored = false;
		printf("Ends with\n");
		m_ptr++;
	}*/
	
	return true;
}


int lines = 0;
int space = 0;
bool inPara = false;
enum { NXX, EMPHASIS, BOLD };
int flags = 0;
int oldflags = 0;
int lefties = 0;
int righties = 0;

void HandleParagraph(string &output)
{
	if(lines > 0)
	{
		//output += 10;
		if(inPara)
		{
			output.append("</p>\n");
			if(lines == 2)
				output.append("<hr nospace>\n");
		}
		if(righties)
			output.append("<p align=right>");
		else
			output.append("<p>");
		inPara = true;
	}
	else
	if(space || lines)
		output += ' ';
	lines = space = 0;
	
	if(flags != oldflags)
	{
		if(flags & EMPHASIS)
			output.append("<i>");
		else
			output.append("</i>");
		
		oldflags = flags;
	}

	righties = 0;
	
}
			
	
bool text2html(string &input, string &output)
{
	TextParser tp(input);
	
	while(!tp.Done())
	{
		tp.NextToken();

		if(tp.IsWord())
		{
			/*if(tp.IsUnderscored())
			{
				cout << tp.Text() << " is underscored!" << endl;
				flags |= EMPHASIS;
			}
			else
				flags &= EMPHASIS;*/

			HandleParagraph(output);
			
			output.append(tp.Text());
		}
		else
		if(tp.IsSpace())
		{
			space++;
		}
		else
		if(tp.IsLine())
		{
			lines++;
		}
		else
		if(tp.IsDelimeter())
		{
			if(tp.Text().compare("_") == 0)
			{
				if(flags & EMPHASIS)
					flags &= ~EMPHASIS;
				else
					flags |= EMPHASIS;
				
				continue;
			}
			else
			if(tp.Text().compare("<") == 0)
			{
				lefties++;
				continue;
			}
			else
			if(tp.Text().compare(">") == 0)
			{
				righties++;
				continue;
			}
						
			HandleParagraph(output);
			
			if(tp.Text().compare("----") == 0)
			{
				//output += 3emdash;
			}
			else
				output.append(tp.Text());
				
		}
	}
		
}

int main(int argc, char **argv)
{
	char *text = loadfile(argv[1]);	
	string input = string(text);
	string output;	
	text2html(input, output);
	
	savefile(argv[2], output.c_str());
	
	return 0;
}
