#ifdef GBA

#include <pogo.h>

char *strdup(const char *s)
{
	char *d = (char *)malloc(strlen(s)+1);
	strcpy(d, s);
	return d;
}

#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#endif

#include "config.h"


unsigned int Hashtable::hash(const char *str)
{
	unsigned int hash = 0;
	while(*str)
		hash = ((hash<<5)^(hash>>27))^*str++;

	return hash;
}

Hashtable::Hashtable()
{
	m_size = 41;
	memset(symbols, 0, m_size * sizeof(void *));
}

Hashtable::~Hashtable()
{
	HashEntry *he, *he2;
	int i;

	for(i=0; i<m_size; i++)
	{
		he = symbols[i];
		while(he)
		{
			he2 = he->next;			
			free(he);			
			he = he2;
		}
	}
}

int Hashtable::size()
{
	HashEntry *he;
	int i;
	int size = 0;

	for(i=0; i<m_size; i++)
	{
		he = symbols[i];
		while(he)
		{
			size++;
			he = he->next;			
		}
	}
	return size;
}

char*& Hashtable::operator[](const char *name)
{
	HashEntry **he;
	int i = hash(name) % m_size;

	he = &symbols[i];
	while(*he && strcmp((*he)->name, name) != 0)
	{
		he = &((*he)->next);
	}

	if(!(*he))
	{
		*he = (HashEntry *)malloc(sizeof(HashEntry));
		(*he)->name = name;
		(*he)->val = 0;
		(*he)->next = 0;
	}
	
	return (*he)->val;

}

Config::Config(char *name)
{
	Hashtable();
	m_name = name;
}

bool Config::Save()
{
	HashEntry *he;
	FILE *fp = fopen(m_name, "wb");

	if(fp)
	{
		for(int i=0; i<m_size; i++)
		{
			he = symbols[i];
			while(he)
			{
				fprintf(fp, "%s=%s\n", he->name, he->val);			
				he = he->next;			
			}
		}
		
		fclose(fp);
		
		return true;
	}
	return false;
}

bool Config::Load()
{
	char line[80];
	char *val;
	char *ptr;

	FILE *fp = fopen(m_name, "rb");

	if(fp)
	{
		while(fgets(line, sizeof(line), fp))
		{
			ptr = line;
			while(*ptr && (*ptr != '=')) ptr++;
			if(*ptr)
			{
				*ptr = 0;
				val = ++ptr;
				while(*ptr && (*ptr != 10) && (*ptr != 13)) ptr++;
				*ptr = 0;
				
				(*this)[strdup(line)] = strdup(val);
				//if(p)
				//	free(p);
				//p = strdup(val);
			}
		}
		fclose(fp);
		return true;
	}
		
	return false;
}

/*
int main(int argc, char **argv)
{
	Config c("test.cfg");
	
	c["FONT0"] = "fonts/arial12.font";
	c["FONT1"] = "fonts/arial12i.font";
	c["FONT2"] = "fonts/arial12b.font";

//	c.Load();
	
	printf("%s\n", c["FONT0"]);
	printf("%s\n", c["FONT1"]);
	printf("%s\n", c["FONT2"]);

	c["FONT0"] = "fonts/arial10.font";
	c["FONT1"] = "fonts/arial10i.font";
	c["FONT2"] = "fonts/arial10b.font";
	
	c.Save();
	
}*/
