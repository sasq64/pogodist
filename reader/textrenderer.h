#ifndef TEXTRENDERER_H
#define TEXTRENDERER_H

#include "display.h"
#include "itextstream.h"

class TextRenderer
{
public:
	TextRenderer(Display *display, ITextStream *stream, PogoFont **fonts);
	void SetStream(ITextStream *stream) { m_stream = stream; }
	
	int  ProcessPage();
	bool Done() { return m_done; }
	
	//int GetTotalHeight() { return m_totalHeight; }
	int GetPageWidth() { return m_width; }
	int GetPageHeight(){ return m_height; }	
	int PageCount() { return m_pageCount; }
	
	// Render a specific page. Will block if page has not
	// been indexed yet
	bool RenderPage(int page = -1, bool render = true);
	
	int GetPage() { return m_page; }	
	int GetChapterPage(int c);
	int GetCurrentChapter();
	
	void SetMargins(int left, int top = 0, int right = -1, int bottom = -1);
	
	void SetLineSpace(int percent) { m_lineSpace = percent; };
	void SetDefaultAlignment(int align) {  m_defaultAlign = align; }
	
	void Reset();
	void Reindex();
	
	bool SaveIndex(char *fname);
	bool LoadIndex(char *fname);

	enum
	{	
		LEFT,
		RIGHT,
		CENTER,
		TOP,
		BOTTOM,
		JUSTIFIED
	};
	
private:
	
	// Sensitive State variagbles
	class TextState
	{
	public:
		int m_paragraphAlign;
		PogoFont **m_currentFontSet;
		int m_currentFont;
		int m_indent;
	};
	
	TextState renderState;
	TextState processState;	
	TextState *state;
	

	void SetFontSet(int n);

	int GetFontSet();

	int Offset(int page) { return m_pageOffsets[page-1] & 0xFFFFFF; }
	//int State(int page) { return m_pageOffsets[page-1] >> 28; }
	void SetState(int page, TextState *state);
	
	//bool ParseTitleBar(char *dest, int section);
	
	// Add a word with pixellength 'len' to be rendered as a line
	void PushWord(char *word, int len);
	
	// Render pushed words
	void RenderLine();
	
	// Renders the paragraph to the current position.
	// Return NULL if it fit, or ptr to first unrendered
	// char otherwise
	char *RenderParagraph(char *line, bool render);

	char *ParseParagraphInfo(char *line, int *page);

	int Text(int x, int y, char *str);
	int TextHeight(char *text);
	int TextLength(char *text, int *rcfont = 0);
	
	int m_width;
	int m_height;
	
	//int m_processedHeight;
	//int m_totalHeight;
	int m_pageCount;

	int m_allocCount;
	unsigned int *m_pageOffsets;
	
	bool m_done;
	

	
	// Global settings
	int m_margins[4];
	int m_lineDistance;
	int m_defaultAlign;
	int m_lineSpace;	
	int m_fgCol;
	int m_bgCol;
	
	// RenderPage temporary
	int m_x;
	int m_y;
	int m_lineWCount;
	int m_lineLength;
	char *m_lineWords[128];
	int m_lineWLenghts[128];
	int m_leftPMargin;
	int m_rightPMargin;

	int m_page;

	
	//int m_paragraphFontSize;

	//char m_title[80];
	
	//char *m_savePath;
	char *m_baseName;

	enum { NORMAL, MEDIUM, BIG };

	PogoFont **m_fonts;
	
	
	ITextStream *m_stream;	
	Display *m_display;
	
	int m_chapterCount;
	int m_chapters[48];
	int m_bookCount;
	int m_books[8];
	
};

#endif
