#ifndef ITEXTSTREAM_H
#define ITEXTSTREAM_H

class ITextStream
{
public:
	//ITextStream(char *fname);
	virtual int Size() = 0;
	virtual int Seek(int offset) = 0;
	virtual int Tell() = 0;
	//virtual int Read(char *dest, int size) = 0;
	virtual char *ReadLine(char *dest = NULL, int size = -1) = 0;
	
};

#endif
