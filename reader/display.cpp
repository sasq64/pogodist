#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "pogofont.h"

#include "display.h"

Display::Display(int w, int h)
{	
	m_width = w;
	m_height = h;
}

uint16 *Display::Lock()
{
}

void Display::Unlock()
{
}

void Display::Clear()
{
	
	SDL_Rect r = {0, 0, m_width, m_height};	
	SDL_FillRect((SDL_Surface*)m_screen, &r, 0xFFFFFF);
}

void Display::Refresh()
{
	SDL_UpdateRect((SDL_Surface*)m_screen, 0, 0, m_width, m_height);
}
	
int Display::Text(int x, int y, char *text)
{
	PogoFont **fonts = &m_font[m_fontSize * 4];
	Lock();
	//m_font[id]->Render(text, x, y);
	int rc = pogofont_text_multi(fonts, &m_currentState, text, x, y);
	Unlock();
	return rc;
}

void Display::Fill(int x, int y, int w, int h, int col)
{
	Lock();
	screen_set(x, y, w, h, col);
	Unlock();
}

int Display::TextLength(char *text)
{
	PogoFont **fonts = &m_font[m_fontSize * 4];
	int state = m_currentState;
	return pogofont_text_multi(fonts, &state, text, 0, -1);
}

int Display::TextHeight(char *text)
{
	unsigned char *ptr = (unsigned char *)text;
	
	PogoFont **fonts = &m_font[m_fontSize * 4];
	
	int height =  fonts[m_currentState]->height;
	
	if(*ptr <= 8)
		height = 0;
	
	while(*ptr)
	{
		if(*ptr <= 8)
		{
			int h = fonts[*ptr-1]->height;
			if(h > height)
				height > h;
		}
		ptr++;
	}

	return height;
}


static bool resize = false;
int new_width;
int new_height;

int Display::WaitEvent()
{
	SDL_Event event;

	if(resize)
	{
		int bpp = 16;
		int video_flags = SDL_RESIZABLE;

		if((m_screen = (void *)SDL_SetVideoMode(new_width, new_height, bpp, video_flags)) == NULL)
		{
			fprintf(stderr, "Couldn't set mode: %s\n", SDL_GetError());
			SDL_Quit();
			exit(1);
		}
		g_screenWidth = m_width = new_width;
		g_screenHeight = m_height = new_height;
		resize = false;
		return 9;
	}

	SDL_PumpEvents();
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
		case SDL_VIDEORESIZE:
			new_width = event.resize.w & 0xFFFFFFFC;
			new_height = event.resize.h;
			resize = true;
			break;
		case SDL_KEYUP:
			//keys[event.key.keysym.sym] = 0;
			break;
			
		case SDL_KEYDOWN:
			//keys[event.key.keysym.sym] = 1;
			//evt.type = BUTTON;
			switch(event.key.keysym.sym)
			{
			case SDLK_DOWN:
			case SDLK_SPACE:
				return 1;
				break;
			case SDLK_UP:
			case SDLK_BACKSPACE:
				return 2;
				break;

			case SDLK_ESCAPE:
			case SDLK_q:
				SDL_Quit();
				return 0;
			default:
				break;
			}
		}
	}
	return -1;
}
