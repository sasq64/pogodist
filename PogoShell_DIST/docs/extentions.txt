
FILE EXTENTIONS

.GBA

Normal GBA-roms



.BIN

Also gba-roms, and the recommended extention for pogoshell
plugins. BIN-files are treated somewhat differently by
MAKEFS (they are never patched in anyway and are sorted
so they appear before other files in the adress-space of
the rom-image).



.BM

Simple bitmap files used by pogoshell. The format is

HEADER (8 Bytes)
"BM".w bpp.w width.w height.w
Followed by width*height*(bpp/8) bytes of pixeldata

Normally bpp=16



.FONT

Potoshells font files, more later.



.MB

Multiboot files - executable files that are loaded into WRAM
to be executed. Can be run even on unsupported carts since
they don't require changing the romstart.



.MBZ

Compressed Multiboot files (Use DCMP to compress). Both MB and
MBZ can be used as plugins (A good idea since it both saves
space and makes those plugins work on non-supported carts).



.THEME

Describes the layout and look of PogoShells gui. Pogoshell always
loads "default.theme" from the Theme-dir defined by pogo.cfg.



.NSF

Nintendo 8bit music-files, check http://zophar.net for a lot of
thses. Can be played by the nsf.bin plugin (but that plugin only
works reliably on Turbo FA like carts).



.TXT

Textfiles, can be read by PogoShells internal text-reader. The
textreader also handles text-files with extra formatting
characters in them, which can be produced using the HTML2POGO
tool.



.MOD

Amiga music modules, playable by PogoShell's internal module-
player.



.BMP

Windows Bitmap files, can be displayed by PogoShells BMP-
viewer.
