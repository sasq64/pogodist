AGB_Hack 1.1
For the GameBoy Advance
Copyright � 2004, Donnie Russell
All rights reserved.

Based on RevivedHack 1.1,
an extensively rewritten adaptation of
Don G. Kneller's PC Hack 3.61.

Level compression was implemented with
Markus Franz Xaver Johannes Oberhumer's
miniLZO 1.08, a mini subset of the LZO
real-time data compression library.



History
=======

Hack has a long and interesting history. Here is part of its lineage:

Hack by Jay Fenlason with help from Kenny Woodland, Mike Thome and Jon Payne
(inspired by Rogue; twice as many monster types)
|
|--- Hack 1.0.3 by Andries E. Brouwer
     (greatly expanded; more than three times as large as the original)
     |
1986 |--- PC Hack 3.61 by Don G. Kneller
          (ported to MS-DOS, with many bugfixes)
          |
2004      |--- RevivedHack 1.1 by Donnie Russell
               (Windows/DOS/Linux port; color, more bugfixes)
               |
2004           |--- AGB_Hack 1.1 by Donnie Russell
                    (GameBoy Advance port; level compression)

Note: In 1987, Hack became the basis for the modern game NetHack.



License
=======

This game is freeware, and may not be sold in any form for any reason
whatsoever.

This game may be redistributed provided that its archive is whole and intact.
Only copies identical to the original distribution archive available from my
home page are authorized for redistribution. Proper credit must be given to
all people involved in the creation of this game near the point of
redistribution. Redistribution of this game on cartridge media is strictly
prohibited.



Getting Started
===============

When the initial screen appears you will be prompted to enter your name. You
can allow the game to pick a name for you by pressing the START button (Enter
key). Turn the key selector on by pressing the L button, then type your name
by moving the cursor around the alphabet with the directional keys, pressing
the B button on each letter. Press the L key to turn the selector off. When
the key selector is off, pressing the B button (Backspace key) moves the
cursor back one space. Press the START button to submit your name.

At this point, the game will seed a sequence of random numbers based upon the
amount of time you took to enter your name. Also, Hack is influenced by the
year, day of the year, and time of day, which are set to random values in the
GameBoy Advance version to simplify game set up.

Next, you will be asked if you want a character to be selected for you
randomly. If you are a beginning player, you may want to select your own
character, so turn the key selector on and type 'n' for no. Select your
character by typing the first letter of its name. Each character has its own
strengths and weaknesses, so choose carefully.

Now, the game begins.

Move your character with the directional keys. To execute a game command, turn
on the key selector and consult the chart below. There are two in-game help
screens that list available commands (the '?' key), and the meaning of various
screen symbols (the '/' key). As a beginning player, you should consult them
frequently; several useful hints can be found there.

While playing, some game messages are too numerous to be displayed at once. To
continue viewing the following message(s), press the A button (Space key).

To cancel a command in progress, press the R button (Escape key).

To quit the current game, type the 'Q' key with the key selector.

Due to the unsually large amount of random-access memory used by this game,
and the limited amount of battery-powered memory, no save command is
available. When the console power is turned off, the current game is lost
forever.



Reference
=========

Buttons (key selector off)
--------------------------

A        space
B        backspace
L        turn key selector on
R        escape (cancel)
START    enter
SELECT   definable, initially 's' (search)

Buttons (key selector on)
-------------------------

A        type space and keep key selector on
B        type key and keep key selector on
L        turn key selector off
R        type key and turn key selector off
START    type enter and turn key selector off
SELECT   define selected key

Game Commands
-------------

Issuing the '@' command toggles automatic picking up of
objects on and off. Gold is always picked up if it is the
only object at your current location.

?  Help.
!  Escape to a shell.
<  Go up a staircase (if you are standing on it).
>  Go down (just like up).
^  Print the type of a trap you found earlier.
)  Print your currently wielded weapon.
[  Print your currently worn armor.
=  Print your currently worn rings.
$  Count how many gold pieces you are carrying.
.  Rest. Do nothing.
,  Pick up some things.
:  Look at what is here.
/  Print what a symbol represents.
\  Print what has been discovered.
a  Again. Issue the previous command again.
A  Use or apply. A generic command for using a key to
   lock or unlock a door, using a rope, etc.
c  Call. Name a certain object or class of objects.
C  Call. Name an individual monster.
d  Drop something. Example: "d7a" drops seven "a"s.
D  Drop several things. In answer to the question "What
   kinds of things do you want to drop? [!%= au]" you
   should give zero or more object symbols, possibly
   followed by "a" and/or "u". "a" means: drop all such
   objects, without asking for confirmation. "u" means:
   drop only unpaid objects (when in a shop).
E  Engrave. Write a message in the dust on the floor.
   "E-" means: use fingers for writing.
e  Eat food.
g  Move until something interesting is found.
G  As previous, but forking of corridors is not
   considered interesting.
kjhlyubn  Go one step in the direction indicated.
          "k": north (i.e., to the top of the screen),
          "j": south, "h": west, "l": east, "y": nw,
          "u": ne, "b": sw, "n": se.
KJHLYUBN  Go in the direction indicated until you hit a
          wall or run into something.
i  Print your inventory.
I  Print selected parts of your inventory. "I*" means:
   print all gems. "Iu" means: print all unpaid items.
   "Ix" means: print all used up items that are on your
   shopping bill. "I$" means: Count your money.
m  Move without picking up any objects.
M  Move far, with no pickup.
o  Redraw the screen.
p  Pay your shopping bill.
P  Put on a ring.
q  Drink (quaff) a potion.
Q  Quit the game.
r  Read a scroll.
R  Remove a ring.
s  Search for secret doors and traps around you.
t  Throw an object or shoot an arrow.
T  Take off armor.
v  Print version number.
w  Wield weapon. "w-" means: wield nothing, or use your
   bare hands.
W  Wear armor.
X  Teleport.
z  Zap a wand.
Z  Repeat the last message (subsequent "Z"s repeat
   earlier messages).

You can put a number before a command to repeat it that
many times, as in "20s" or "40.".

When you see the prompt "[ynaq]", it means:

  y - yes for this item
  n - no for this item (skip to the next one)
  a - yes for all items starting with this one
  q - quit; stop asking about these items

If you use "D%", you will be asked whether you want to
drop each piece of food. If you answer "a", you will
drop all pieces of food. If you answer "q", you will not
drop any pieces. You can drop pieces selectively by
answering "y" or "n" for each piece.

Screen Symbols
--------------

Use the "/" command to find out what a symbol represents.
Monster symbols are not listed below.

@  human (or you)
-  a wall
|  a wall
+  a door
.  the floor of a room
#  a corridor
}  water filled area
<  the staircase to the previous level
>  the staircase to the next level
^  a trap
$  a pile, pot or chest of gold
%  a piece of food
!  a potion
*  a gem
?  a scroll
=  a ring
/  a wand
[  a suit of armor
)  a weapon
(  a useful item (camera, key, rope, etc.)
0  an iron ball
_  an iron chain
`  an enormous rock
"  an amulet



Status Line
===========

On the bottom line of the display, you can see what dungeon level you are on,
how much gold you are carrying, how many hit points you have now and will have
when fully recovered, what your armor class is (the lower the better), your
strength, experience level and the state of your stomach.



Hint
====

If you ever find yourself inside a room with no exits, use the 's' (search)
command repeatedly along the walls of the room. This command can also be used
to find hidden traps. By default, the SELECT button is defined as 's'.
