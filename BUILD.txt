
HOW TO BUILD libpogo AND PogoShell
----------------------------------



This is what you need:

- the gnuarm sdk (or devkitarm)

The old devkitadvance is no longer supported.
You can find gnuarm here: http://www.gnuarm.com/files.html


- make.exe, cp.exe, mkdir.exe rm.exe

Since the source is originally linux based, you need some unix command
line tools to build. Either install cygwin (www.cygwin.com) or just
grab the zipfile from http://unxutils.sourceforge.net/ and copy
the tools you need into your path.


- gbafix.exe

Darkfaders gbafix is also needed. The source is included in the tools
directory. Compile and copy to your path, or add tools to your path.


- Visual C++

To build the tools under windows (like makefs.exe) you also need the
Visual C++ commandline tools. If you have Visual C++ installed you
need to run VCVARS32.BAT from the commandline to set up paths and
environment variables for compilations.
You can also get the command line tools for free from Microsoft.



Building

o On linux (or similar) type make -f Makefile.unx
  On windows type make -f Makefile.win
  
o Go into PogoShell_DIST and run one of the build-scripts to make
  a rom that can be flashed to a cart or run in an emulator



ABOUT THE SOURCE

libpogo is built both in release and debug. The results are placed in
libpogo/lib. Normal source is in libpogo/source while include files that
is needed by programs using libpogo are in libpogo/include. The Makefile
is libpogo/Makefile

pogoshell is built by pogoshell/Makefile. It only builds for one specific
cart-type, defined by the CART-variable. The top-level makefile calls the
pogoshell makefile for each cart type. If you want to build only pogoshell
yourself you can do something like:

cd pogoshell
make CART=ez

to build for that cart-type - which means linking with
libfc_CART/libflashcart.a and outputting pogoshell_CART.gba

The different flashcart-libraries are built as necessary by pogoshell.

The resulting gba is placed directly under pogoshell. Only when you use
the toplevel makefile are the different gba-files actually copied to
PogoShell_DIST/root/.shell/plugins

Pogoshell now works in multiboot which is convinient if you have a MBV2-
cable. Just uncomment the #define MULTIBOOT in pogoshell/main.c for this.

Normally, debug-output goes to the emulator debug output system, but if
you compile MULTIBOOT it uses MB console output, which will make pogoshell
lock up unless there is an mb console running at the PC end.
So for multiboot you need to start the MB tool something like this:

mb -s pogoshell_xxx.gba -w 250 -c -x 400

This loads the gba-file as a multiboot-file and enters console-mode.

-- Sasq
