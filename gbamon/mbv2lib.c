
//
//  MB v1.40 is required to use this software.
// Possibly later versions of MB *may* work as well.
//
// The following library functions are supported:
//
// Library name   Standard Name      Function
//   dprintf        printf       Print a string on PC console.
//   dputchar       putchar      Print a char on PC console.
//   dgetch         getch        Get a char from PC keyboard.
//   dkbhit         kbhit        Return 1 if PC keyboard char is ready.
//
//   dfopen         fopen        Open PC file.
//   dfclose        fclose       Close PC file.
//   dfgetc         fgetc        Get char from PC file.
//   dfputc         fputc        Write a char to PC file.
//   drewind        rewind       Set file pointer to start of file.
//
// If you wish to use the standard naming conventions
// rather than the library names then change "__ANSI_NAMES 0"
// to "__ANSI_NAMES 1" instead.
//
// Notes:
//
//  Currently only ONE file may be open at a time.
//
//  If you are missing some .h files during compile than get
// 'arminc.zip' from http://www.devrs.com/gba in the
//  Apps / C Compilers section.
//
// Example command line:
//    mb -s file.mb -c -w 50 -x 255 -m
//
//  In this example, after transferring "file.mb" to the GBA,
// the PC goes into console/file server mode (-c) and also
// shows all of the file open/file close/fgetc/fputc commands
// (-m) on screen. The -w value should be a large enough value
// where the -s is reliable and the -x value should be a large
// enough value where the -c is reliable with the GBA.
//
//      [Sending a file & console mode each have
//       their own delay settings because they
//       each use a different method for transferring
//       data. Each method is about ideal for it's
//       application.]
//
// Example GBA Code:
//
//  #include "mbv2lib.c"
//
//  int main (void)
//    {
//    int i,j,k;
//    FILE fp;
//
//    dprintf("Hello world!");
//
//    // Get character from PC keyboard
//    i = dgetch();
//
//    // Write data to file
//    fp = dfopen("foo.bin","wb");
//    dfputc(0,fp);
//    dfputc(1,fp);
//    dfputc(2,fp);
//    dfclose(fp);
//
//    // Read data from file
//    fp = dfopen("foo.bin","rb");
//    i = dfgetc(fp);
//    j = dfgetc(fp);
//    k = dfgetc(fp);
//    dfclose(fp);
//
//    }

typedef unsigned short u16;
typedef volatile unsigned short vu16;
typedef unsigned int uint32;

#include "gba_defs.h"

//#include "vsprintf.c"

#define __DOUTBUFSIZE 256
#define __DINBUFSIZE 256  //Must be a multiple of 2! (ex: 32,64,128,256,512..)
#define __ESCCHR 27

#define __ESC_NADA   0
#define __ESC_ESCCHR 1
#define __ESC_FOPEN  2
#define __ESC_FCLOSE 3
#define __ESC_FGETC  4
#define __ESC_FPUTC  5
#define __ESC_REWIND 6
#define __ESC_KBHIT  7

unsigned char __outstr[__DOUTBUFSIZE];
unsigned char __instr[__DINBUFSIZE];
int inptr = 0;
int outptr = 0;

int __dputchar (int c)
   {
   int rcv;
   static int LastChar = 0;

   // Set non-general purpose comms mode
   *(u16 *)REG_RCNT = 0;

   // Init normal comms, 8 bit transfer, receive clocking
   *(u16 *)REG_SIODATA8 = c;
   *(u16 *)REG_SIOCNT = 0x80;

   // Wait until transfer is complete
   while (*(vu16 *)REG_SIOCNT & 0x80) {}

   // Wait until SC is low
   while (*(vu16 *)REG_RCNT & 1) {}

   // Force SD high
   *(u16 *)REG_RCNT = 0x8022;

   // Wait until SC is high
   while ((*(vu16 *)REG_RCNT & 1)==0) {}

   rcv = *(vu16 *)REG_SIODATA8;
   if (LastChar == __ESCCHR)
      {
      // Process escape character
      switch (rcv)
         {
         case 1:
            __instr[inptr++] = __ESCCHR;
            inptr &= (__DINBUFSIZE-1);
            break;
         }
      LastChar = 0;
      }
   else
      {
      if (rcv == __ESCCHR)
         LastChar = __ESCCHR;
      else
         {
         // If char received from PC then save in receive FIFO
         __instr[inptr++] = rcv;
         inptr &= (__DINBUFSIZE-1);
         }
      }
   return(1);
   }

int dputchar (int c)
   {
   (void) __dputchar(c);
   if (c == __ESCCHR)
      (void) __dputchar(__ESC_ESCCHR);
   return (1);
   }

/*void __PrintStr (char *str)
   {
   while (*str)
      (void) dputchar(*str++);
   }
*/
int dgetch (void)
   {
   int c;

   // If no character is in FIFO then wait for one.
   while (inptr == outptr)
      {
      __dputchar(__ESCCHR);
      __dputchar(__ESC_NADA);
      }

   c = __instr[outptr++];
   outptr &= (__DINBUFSIZE-1);

   return (c);
   }

int dkbhit (void)
   {
   return(inptr != outptr);
   }

#define FILE int

FILE dfopen (const char *file, const char *type)
   {
   __dputchar(__ESCCHR);
   __dputchar(__ESC_FOPEN);

   while (*file)
      (void) dputchar(*file++);
   dputchar(0);

   while (*type)
      (void) dputchar(*type++);
   dputchar(0);

   return(1);
   }

int dfclose (FILE fp)
   {
   __dputchar(__ESCCHR);
   __dputchar(__ESC_FCLOSE);

   return(1);
   }

int dfgetc (FILE fp)
   {
   __dputchar(__ESCCHR);
   __dputchar(__ESC_FGETC);

   return(dgetch());
   }

int dfputc (int ch, FILE fp)
   {
   __dputchar(__ESCCHR);
   __dputchar(__ESC_FPUTC);

   dputchar(ch);

   return(1);
   }

void drewind (FILE fp)
   {
   __dputchar(__ESCCHR);
   __dputchar(__ESC_REWIND);
   }

/*#define dprintf(x...) ({ dsprintf(__outstr, x); __PrintStr(__outstr); })

#ifdef __ANSI_NAMES
  #define printf  dprintf
  #define putchar dputchar
  #define getch   dgetch
  #define kbhit   dkbhit

  #define fopen   dfopen
  #define fclose  dfclose
  #define fgetc   dfgetc
  #define fputc   dfputc
  #define rewind  drewind
#endif
*/
