	@.SECTION	.iwram

	.ALIGN
	.ARM

	.GLOBAL		executeCart
	.GLOBAL		setRamStart
	.GLOBAL		setRamStart_VIS
	.GLOBAL		setRomStart_VIS
	.GLOBAL		setRamStart_XG
	.GLOBAL		setRomStart_XG

Setup_VIS:

	add	r1, pc, #1
	bx		r1

	.THUMB

	nop
	add		r0, pc, #flash_constants-.-2
	ldmia	r0!,{r2-r5}

	ldr		r1,[r0]
	strh	r2,[r1]

	ldr		r1,[r0,#4]
	mov		r7,#250
	add		r7,r7
	mov		r6,r7
0:
	strh	r3,[r1]
	add		r6,r6,#-1
	bne		0b

	ldr		r1,[r0,#4]
	strh	r2,[r1]

	mov		r6,r7
1:
	strh	r4,[r1]
	add		r6,r6,#-1
	bne		1b

	ldr		r1,[r0]
	strh	r2,[r1]

	ldr		r1,[r0,#4]
	strh	r2,[r1]

	ldr		r1,[r0,#12]
	strh	r4,[r1]

	ldr		r1,[r0,#16]
	strh	r3,[r1]

	ldr		r1,[r0,#4]
	mov		r6,r7
2:
	strh	r5,[r1]
	add		r6,r6,#-1
	bne		2b

	ldr		r1,[r0]
	strh	r2,[r1]

	bx		lr

	.ALIGN

flash_constants:
	.word	0x5354
	.word	0x1234
	.word	0x5678
	.word	0xabcd

	.word	(0x00987654 * 2) + 0x08000000
	.word	(0x00012345 * 2) + 0x08000000
	.word	(0x00007654 * 2) + 0x08000000
	.word	(0x00765400 * 2) + 0x08000000
	.word	(0x00013450 * 2) + 0x08000000

	.ARM


@ r0 = ROM start
setRomStart_VIS:

	stmfd	sp!, {lr}

	mov		r9,r0

	bl	Setup_VIS

	mov		r0,r9

	@ k = ((l>>12)&0x7F8) | ((l>>22)&0x7) | 0x4000;

	mov		r2, r0, lsr #22
	mov		r0, r0, lsr #12

	mov		r3, #0xff

	and		r2, r2, #7
	and		r0, r0, r3, lsl #3

	orr		r0, r0, r2

	mov		r3, #0x40
	orr		r0, r0, r3, lsl #8

	ldr		r1,=0x096B592E
	strh	r0,[r1]

	ldmfd	sp!,{lr}
	bx		lr


@ r0 = Rom start offset
setRomStart_XG:

	add	r1, pc, #1
	bx		r1

	.THUMB


	lsr		r0,r0,#15
	ldr		r1,=0xFFF
	and		r0,r1

	mov		r1,#0xd2
	lsl		r1,r1,#8
	mov		r2,#0x15
	lsl		r2,r2,#8

	add	r3, pc, #ez_constants-.-2

	ldmia	r3!,{r4-r7}

	strh	r1,[r4]
	strh	r2,[r5]
	strh	r1,[r6]
	strh	r2,[r7]

	ldmia	r3!,{r4-r6}
	strh	r0,[r4]
	strh	r2,[r6]

	bx		lr

	.ARM

@ r0 = RAM 64KB bank
setRamStart_VIS:

	stmfd	sp!, {lr}

	mov		r9,r0

	bl		Setup_VIS

	ldr		r1,=0x0942468A
	strh	r9,[r1]

	ldmfd	sp!,{lr}
	bx		lr


@ r0 = RAM 64KB bank
setRamStart_XG:

	add		r1, pc, #1
	bx		r1

	.THUMB

	lsl		r0,r0,#2

	mov		r1,#0xd2
	lsl		r1,r1,#8
	mov		r2,#0x15
	lsl		r2,r2,#8

	add	r3, pc, #ez_constants-.-2

	ldmia	r3!,{r4-r7}

	strh	r1,[r4]
	strh	r2,[r5]
	strh	r1,[r6]
	strh	r2,[r7]

	ldmia	r3!,{r4-r6}
	strh	r0,[r5]
	strh	r2,[r6]

	bx		lr


	.ALIGN
ez_constants:
	.word	0x09FE0000
	.word	0x08000000
	.word	0x08020000
	.word	0x08040000
	.word	0x09880000
	.word	0x09C00000
	.word	0x09FC0000


	.ALIGN
	.POOL
	.END
