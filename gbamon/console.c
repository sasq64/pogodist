
#include <pogo.h>
#include "fixed5.h"
#include "gba_defs.h"

static uint16 *vram;
static uchar *buffer;
static uchar *colors;

static int width;
static int height;
static Font *font;
static int linespace;

static int color;

#define TO_RGB16(r,g,b) ( ((b << 7) & 0x7C00) | ((g << 2) & 0x03E0) | (r >> 3) )

const static uint16 ansicol[16] =
{ 
	TO_RGB16(0x00,0x00,0x00),
	TO_RGB16(0xA0,0x00,0x00),
	TO_RGB16(0x00,0xA0,0x00),
	TO_RGB16(0xA0,0xA0,0x00),
	TO_RGB16(0x00,0x00,0xA0),
	TO_RGB16(0xA0,0x00,0xA0),
	TO_RGB16(0x00,0xA0,0xA0),
	TO_RGB16(0xA0,0xA0,0xA0),
	
	TO_RGB16(0x00,0x00,0x00),
	TO_RGB16(0xFF,0x00,0x00),
	TO_RGB16(0x00,0xFF,0x00),
	TO_RGB16(0xFF,0xFF,0x00),
	TO_RGB16(0x00,0x00,0xFF),
	TO_RGB16(0xFF,0x00,0xFF),
	TO_RGB16(0x00,0xFF,0xFF),
	TO_RGB16(0xFF,0xFF,0xFF)
};

void con_clear(int sol, int eol)
{
	int y;
	uint32 *ptr = vram;
	uint32 col = 0;ansicol[color>>4];
	int size;
	
	//col |= (col<<16);
	
	for(y=sol; y<=eol; y++)
	{
		size = (120*(font->height+linespace));
		ptr = (uint32 *)vram + size*y;
		while(size--) *ptr++ = col;
		memset(&buffer[width*y], 0, width);
		memset(&colors[width*y], 0, width);
	}

//	memset(buffer, 0, width*height);
//	memset(colors, color, width*height);
}

void con_init()
{
	dprint("blasd\n");
	
	width = 48;
	height = 16;
	vram = (uint16 *)0x06000000;
	
	if(!buffer)
	{
		buffer = malloc(width*height*2);
		colors = buffer + width*height;
	}

	linespace = 2;
	color = 0x0F;
	con_clear(0, height);

	font = font_memload(fixed5, "fixed5");

}

void con_setcolor(int fg, int bg)
{
	color = fg | (bg<<4);
}

void con_putcolorxy(int x, int y, int fg, int bg)
{
	colors[y*width+x] = fg | (bg<<4);
}

void con_printxy(int x, int y, char *str)
{
	int l = strlen(str);
	int offs = y*width+x;
	
	memcpy(&buffer[offs], str, l);
	memset(&colors[offs], color, l);
}

void con_multicolorxy(int x, int y, int len, int fg, int bg)
{
	uchar *ptr = colors + y*width+x;
	int c = fg | (bg<<4);
	
	while(len--)
		*ptr++ = c;
}

uchar font_putchar(Font *font, char c, uint16 *dest, int width);

void con_flush()
{
	int x,y,i,c;
	uint16 *ptr;
	uchar col = colors[0]+1;

	for(i=0, y=0; y<height; y++)
	{
		ptr = vram + 240*(font->height+linespace)*y;
		for(x=0; x<width; x++, i++)
		{
			if((c = buffer[i]))
			{
				if(col != colors[i])
				{
					col = colors[i];
					font_setcolor(ansicol[col&0xf], ansicol[col>>4]);
				}
				font_putchar(font, c, ptr, 240);
			}
			ptr += font->charwidth;
		}
	}
	memset(buffer, 0, width*height);
}
	
