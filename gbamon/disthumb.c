/*
 * Arm Thumb Disassembler - by Sasq
 */

//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>

#include <pogo.h>

//typedef unsigned short uint16;
//typedef unsigned int uint32;
//typedef unsigned char uchar;

#define REG0 0x0100
#define REG 0xF100
#define REGHI1 0x1100
#define REGHI2 0x0100
#define COND 0x200
#define OFFSET 0x300
#define IMM  0x0400
#define IMM2 0x1400
#define IMM4 0x2400
#define BOFFSET 0x900
#define REGLIST0 0x0A00
#define REGLIST 0xFA00
#define REGLISTLR 0x6A00
#define REGLISTPC 0x7A00

typedef struct
{
	uint16 opcode;
	uchar optype;
	char name[5];
} ThumbOpcode;

typedef struct
{
	char string[18];
	uint16 decode[3]; 
} OpcodeType;

enum {
	REG_REG, COND_OFFS, REG_REG_IMM5, REG_REG_REG, REG_REG_IMM3,
	REG_IMM8, REGHI_REGHI,REG_HI2, REG_PCREL, REG_INDREG_REG,REG_INDREG_OFFS,
	REG_INDREG_OFFS2, REG_INDREG_OFFS4,
	REG_SPREL, SP_IMM, REGLIST_LR, REGLIST_PC, REG_REGLIST, IMM8, OFFSET11, NONE
};

#include "dissdata.h"

static uint32 regs[16];
static uint16 bl_opcode = 0;

void diss_setreg(int n, uint32 val)
{
	regs[n] = val;
}

#define ARGCPY(ptr, src) (memcpy(ptr, src, strlen(src)), ptr += strlen(src))

static char *make_reglist(char *dest, int data)
{
	int i, in_list = -1;
	uint16 changes = data ^ (data<<1);
	char *d = dest;

	*dest++ = '{';
	
	for(i=0; i<17; i++)
	{
		if(changes & (1<<i))
		{
			if(in_list == -1)
			{
				strcpy(dest, reglist[i]);
				dest +=2;
				in_list = i;
			}
			else
			{
				if(i - in_list > 1)
				{
					*dest++ = '-';
					strcpy(dest, reglist[i-1]);
					dest +=2;
				}
				*dest++ = ',';
				in_list = -1;
			}
		}
	}
	if(dest[-1] == ',')
		dest--;
	*dest++ = '}';
	*dest = 0;
	return d;
}

static char *decode_opcode(char *dest, const char *name, const char *format, uint16 opcode, const uint16 *decdata)
{
	char tmp[20];
	int v2 = 0;
	int a,v,shift, size,extra;
	uint16 mask;
	char *p;
	
	uint32 location = regs[15];

	strncpy(dest, name, 5);
	dest[5] = 0;

	p = &dest[strlen(dest)];
	p[0] = ' ';
	p[1] = ' ';
	p[2] = ' ';
	strcpy(p, format);
	
	v2 = 0;

	while(*p)
	{
		if(*p >= '0' && *p <= '9')
		{
			a = *p - '1';
			shift = (decdata[a]>>4)&0xF;
			size = (decdata[a]&0xF);
			extra = (decdata[a]>>12);
			
			v = opcode>>shift;
			mask = ~(0xFFFF<<size);
			v &= mask;
			
			switch(decdata[a] & 0xF00)
			{
			case REG0:
				ARGCPY(p, reglist[v + ((opcode>>(extra+3)) & 8)]);
				break;
			case COND:
				ARGCPY(p, condcodes[v]);
				break;
			case OFFSET:
				sprintf(tmp, "%08x", (location & 0xFFFFFFFC) + (v<<2) + 4);
				ARGCPY(p, tmp);
				break;
			case BOFFSET:
				if(bl_opcode)
					v2 = ((bl_opcode&0x7FF)<<21)>>9;
				else
					v = (v<<(32-size))>>(32-size);
				sprintf(tmp, "%08x", location + v2 + (v<<1) + 4);
				ARGCPY(p, tmp);
				break;
			case IMM:
				sprintf(tmp, "%02x", v << extra);
				ARGCPY(p, tmp);
				break;
			case OFFSET11:
				sprintf(tmp, "%08x", location + (v<<1) + v2 + 2);
				ARGCPY(p, tmp);
				break;
			case REGLIST0:
				make_reglist(p, (v&0xFF) | ((v&0x100)<<extra));
				p = &p[strlen(p)];
				break;
			default:
				p++;
				break;
			}		
		}
		else
			p++;
	}
	
	return dest;
}

uint16 *diss_opcode(uint16 *opcode, char *dest, int flags)
{
	int i;
	const ThumbOpcode *t;
	uint32 mask;
	int size = sizeof(thumb_opcodes) / sizeof(ThumbOpcode);
	uint16 op = *opcode++;
	bl_opcode = 0;
	
	for(i=0; i<size; i++)
	{
		t = &thumb_opcodes[i];
		mask = 0xFFFF<<(t->opcode&0xf);
		if((op & mask) == (t->opcode & mask))
		{
			
			
			if(t->name[0] == '!')
			{
				bl_opcode = op;
				t = &thumb_opcodes[i+1];
				op = *opcode++;
			}
			
			decode_opcode(dest, t->name, optypes[t->optype].string, op, optypes[t->optype].decode);
			return opcode;
			
		}
	}
	
	strcpy(dest, "???");
	
	return opcode;
}
