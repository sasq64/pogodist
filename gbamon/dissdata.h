const static OpcodeType optypes[] =
{
	{ " 1x, 2x", REG | 0x03, REG | 0x33, 0 },
	{ "1x $2xxxxxxx", COND | 0x84, BOFFSET | 0x08, 0 },
	{ " 1x, 2x, #$3x", REG | 0x03, REG | 0x33, IMM | 0x65 },
	{ " 1x, 2x, 3x", REG | 0x03, REG | 0x33, REG | 0x63 },
	{ " 1x, 2x, #$3x", REG | 0x03, REG | 0x33, IMM | 0x63 },
	{ " 1x, #$2x", REG | 0x83, IMM | 0x08, 0 },
	{ " 1x, 2x", REGHI1 | 0x03, REGHI2 | 0x33, 0 },
	{ " 1x", REGHI2 | 0x33, 0, 0},
	{ " 1x, [$2xxxxxxx]", REG | 0x83, OFFSET | 0x08, 0 },
	{ " 1x, [2x, 3x]", REG | 0x03, REG | 0x33, REG | 0x63},
	{ " 1x, [2x, #$3x]", REG | 0x03, REG | 0x33, IMM | 0x65},
	{ " 1x, [2x, #$3x]", REG | 0x03, REG | 0x33, IMM2 | 0x65},
	{ " 1x, [2x, #$3x]", REG | 0x03, REG | 0x33, IMM4 | 0x65},
	{ " 1x, [SP, #$2x]", REG | 0x83, IMM | 0x08, 0 },
	{ " SP, SP, #$1x", IMM4 |0x07, 0, 0},
	{ " 1", REGLISTLR | 0x09, 0, 0 },
	{ " 1", REGLISTPC | 0x09, 0, 0 },
	{ " 1x!, 2", REG | 0x83, REGLIST | 0x08, 0 },
	{ " #$1x", IMM | 0x08, 0, 0},
	{ " $1xxxxxxx", BOFFSET | 0x0B, 0, 0 },
	{ "", 0, 0, 0 },
};

#define OP(a,b,c,d) {a | b, d, c},

/* VALUE, DATABITS, NAME, TYPE */
const static ThumbOpcode thumb_opcodes[] = {

OP(0x0000, 11, "LSL", REG_REG_IMM5)
OP(0x0800, 11, "LSR", REG_REG_IMM5)
OP(0x1000, 11, "ASR", REG_REG_IMM5)

OP(0x1800, 9, "ADD", REG_REG_REG)
OP(0x1A00, 9, "SUB", REG_REG_REG)
OP(0x1C00, 9, "ADD", REG_REG_IMM3)
OP(0x1E00, 9, "SUB", REG_REG_IMM3)

OP(0x2000, 11, "MOV", REG_IMM8)
OP(0x2800, 11, "CMP", REG_IMM8)
OP(0x3000, 11, "ADD", REG_IMM8)
OP(0x3800, 11, "SUB", REG_IMM8)

OP(0x4000, 6, "AND", REG_REG)
OP(0x4040, 6, "EOR", REG_REG)
OP(0x4080, 6, "LSL", REG_REG)
OP(0x40C0, 6, "LSR", REG_REG)
OP(0x4100, 6, "ASR", REG_REG)
OP(0x4140, 6, "ADC", REG_REG)
OP(0x4180, 6, "SBC", REG_REG)
OP(0x41C0, 6, "ROR", REG_REG)
OP(0x4200, 6, "TST", REG_REG)
OP(0x4240, 6, "NEG", REG_REG)
OP(0x4280, 6, "CMP", REG_REG)
OP(0x42C0, 6, "CMN", REG_REG)
OP(0x4300, 6, "ORR", REG_REG)
OP(0x4340, 6, "MUL", REG_REG)
OP(0x4380, 6, "BIC", REG_REG)
OP(0x43C0, 6, "MVN", REG_REG)

OP(0x4400, 8, "ADD", REGHI_REGHI)
OP(0x4500, 8, "CMP", REGHI_REGHI)

OP(0x46C0, 0, "NOP", NONE)
OP(0x4600, 8, "MOV", REGHI_REGHI)
OP(0x4700, 8, "BX", REG_HI2)

OP(0x4800, 11, "LDR", REG_PCREL)

OP(0x5000, 9, "STR ", REG_INDREG_REG)
OP(0x5200, 9, "STRSH", REG_INDREG_REG)
OP(0x5400, 9, "STRB", REG_INDREG_REG)
OP(0x5600, 9, "STRSB", REG_INDREG_REG)
OP(0x5800, 9, "LDR", REG_INDREG_REG)
OP(0x5A00, 9, "LDRSH", REG_INDREG_REG)
OP(0x5C00, 9, "LDRB", REG_INDREG_REG)
OP(0x5E00, 9, "LDRSB", REG_INDREG_REG)

OP(0x6000, 11, "STR", REG_INDREG_OFFS4)
OP(0x6800, 11, "LDR", REG_INDREG_OFFS4)
OP(0x7000, 11, "STRB", REG_INDREG_OFFS)
OP(0x7800, 11, "LDRB", REG_INDREG_OFFS)
OP(0x8000, 11, "STRH", REG_INDREG_OFFS2)
OP(0x8800, 11, "LDRH", REG_INDREG_OFFS2)

OP(0x9000, 11, "STR", REG_SPREL)
OP(0x9800, 11, "LDR", REG_PCREL)
OP(0xA000, 11, "ADD", REG_PCREL)
OP(0xA800, 11, "ADD", REG_SPREL)

OP(0xB000, 10, "SUB", SP_IMM)
OP(0xB400, 10, "PUSH", REGLIST_LR)
OP(0xBC00, 10, "POP", REGLIST_PC)

OP(0xC000, 11, "STMIA", REG_REGLIST)
OP(0xC800, 11, "LDMIA", REG_REGLIST)

OP(0xDF00, 8, "SWI", IMM8)
OP(0xD000, 12, "B", COND_OFFS)

OP(0xE000, 11, "B", OFFSET11)

OP(0xF000, 11, "!X", OFFSET11)
OP(0xF800, 11, "BL", OFFSET11)


};

const static char *reglist[] = {"R0", "R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8", "R9", "R10", "R11", "R12", "SP", "LR", "PC" };
const static char *condcodes[] = {"EQ", "NE", "HS", "LO", "MI", "PL", "VS", "VC", "HI", "LS", "GE", "LT", "GT", "LE", "AL", "NE"};
