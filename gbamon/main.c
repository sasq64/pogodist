#include <pogo.h>
//#include <rtc.h>

#include "console.h"
#include "gba_defs.h"
#include "disthumb.h"
/*#include "doc.h"*/

enum { MODE_MEM, MODE_CODE, 
/*MODE_TEXT, */
MODE_END };

//#define IN_IWRAM __attribute__ ((section (".iwram")))
//extern void __FarProcedure2 (void (*ptr)(), int a, int b);  // Reference to routine in crt0.S

void setRamStart_XG(unsigned int a);
void setRomStart_XG(unsigned int a);
void setRamStart_VIS(unsigned int a);
void setRomStart_VIS(unsigned int a);

#define free_space() ioctl(sram_fd, SR_FREESPACE)
int __gba_multiboot;


// Device init funtions
void filesys_init();
void deb_init();
void screen_init();
void key_init();
void sram_init();
void gamesys_init();

void Halt(void);
int key_get_qualifiers(void);

int dputchar (int c);


int mode = MODE_MEM;

void PrintStr(char *str)
{
	while(*str)
		dputchar(*str++);
}

uchar *memstart = (uchar *)0x02000000;

int change_nibble = 2;

char *itoa_size(int v, char *dst, unsigned int base, int size, char fill_char);

int lines = 14;
int cols = 12;

int editmode = 0;
int editx = 0;
int edity = 0;
int bytes = 4;

int page_changed = 1;
int reg_changed = 1;
uchar *oldmemstart = 0;

uint32 clipreg;
uint32 reg[16];
int bank = 0;
int current = 0;

#define REG(x) reg[bank*4+x]

uchar *modbuf;

void hexcpy(uchar *dst, uchar *src, int size)
{
	register uchar s;
	
	while(size--)
	{
		s = *src++;
		if(s < 0x20 || s > 125)
			*dst++ = '.';
		else
			*dst++ = s;
	}
}

int dfopen (const char *file, const char *type);
int dfclose (int fp);
int dfputc (int ch, int fp);


void EZWrite(uint32 adr, uint16 val)
{
	volatile uint16 *ezreg = (uint16*)0x08000000;
	volatile uint16 *ezreg2 = (uint16*)0x09FE0000;
	int offset = 0x00020000/2;

	uint16* dest = 	(uint16*)adr;
	
	ezreg2[0] = 0xd200;
	ezreg[0] = 0x1500;
	ezreg[offset] = 0xd200;
	ezreg[offset*2] = 0x1500;
	*dest = val;
	ezreg2[-offset] = 0x1500;	
}

void EZWriteFunc(void)
{
	EZWrite(REG(0), REG(1));
}

void ReadPage(uint32 page, uint32 dest)
{
	register uint32 a = (page>>8)&0xff;
	register uint32 b = page&0xff;
	int i;
	volatile uint16 *timers = (uint16*)0x04000100;
	volatile uint32 *dma = (uint32*)0x040000D4;
	volatile uchar *nand = (uchar *)0x09FFFFE0;

	EZWrite(0x09C40000, 0x1500);
	EZWrite(0x09400000, 0x0003);

	nand[2] = 0xFF;
	nand[2] = 0;
	
	nand[0] = 0;
	nand[0] = 0;
	
	nand[0] = a;
	nand[0] = b;

	nand[2] = 0x30;
	
	timers[7] = 0x83;
	for(i=0; i<8; i++)
	{
		timers[6] = 0;
		while(timers[6] <= 0x10);
	}
		
	dma[0] = 0x09FFC000;
	dma[1] = dest;
	dma[2] = 0x80000000 | (2112/2);
	
	EZWrite(0x09400000, 0x0000);
	EZWrite(0x09C40000, 0xD200);
}

void ReadPageFunc(void)
{
	ReadPage(REG(0), REG(1));
	REG(0) += 1;
	REG(1) += 2112;
}

void DumpPagesFunc()
{
	int fd,i,size;
	char tmp[32];
	uint32 page[2112/4];
	uchar *ptr8;
	int start = REG(0);
	int stop = REG(1);
	
	sprintf(tmp, "nand_dump%04x-%04x.bin", start, stop);
	fd = dfopen(tmp, "wb");
	
	for(i=start; i<=stop; i++)
	{
		ReadPage(i, page);
		size = 2112;
		ptr8 = page;
		while(size--)
		{
			dfputc(*ptr8++, fd);
		}
	}
	
	dfclose(fd);
	
}

void Jump(void)
{
	void (*f)(uint32 a, uint32 b, uint32 c, uint32 d) = (void *)memstart;
	f(REG(0), REG(1), REG(2), REG(3));
}

void XGRam(void)
{
	setRamStart_XG(reg[0]);
}

void XGRom(void)
{
	setRomStart_XG(reg[0]);
}

void F2ARam(void)
{
	setRamStart_VIS(reg[0]);
}

void F2ARom(void)
{
	setRomStart_VIS(reg[0]);
}

void FillMemory(void)
{
	uchar *ptr8 = (uchar *)REG(0);
	uint16 *ptr16 = (uint16*)(REG(0) & 0xFFFFFFFE);
	uint32 *ptr32 = (uint32*)(REG(0) & 0xFFFFFFFC);
	
	int size = (REG(1) - REG(0))/bytes;
	
	uint32 data = REG(2);
	
	while(size--)
	{
		if(bytes == 1)
			*ptr8++ = data;
		else
		if(bytes == 2)
			*ptr16++ = data;
		else
			*ptr32++ = data;
	}
}


void DumpMem(void)
{
	int fd;
	char tmp[32];
	uchar *ptr8 = (uchar *)REG(0);
	int size = (REG(1) - REG(0))/bytes;
	
	sprintf(tmp, "data%08x.bin", reg[0]);
	fd = dfopen(tmp, "wb");
	while(size--)
	{
		dfputc(*ptr8++, fd);
	}
	dfclose(fd);
}

void Quit(void)
{
	void (*f)(void) = (void *)0x00000008C;
	f();
}

typedef struct
{
	char *name;
	void (*func)(void);
	char *desc;

} Function;



Function func_list[] = {

	{"  FillMem   ", &FillMemory, "Fill from %1 -> %2 with %3"},
	{"    Jump    ", &Jump, "Jump to %0"},// with bank contents in R0-R4"},
	{"   DumpMem  ", &DumpMem, "Transfer %1-%2 to hostfile"},
	{"    Quit    ", &Quit, "Quit GBAMon (Reset)"},
	{" Read Page  ", &ReadPageFunc, "Read NANDpage %1 to %2"},
	{" DumpPages  ", &DumpPagesFunc, "Write pages %1->%2 to hostfile"},
	{" EZRegWrite ", &EZWriteFunc, "Write %2 to EZReg %1"},
	{"  XG RamSw  ", &XGRam, "XG Switch to SRAM bank %1"},
	{"  XG RomSw  ", &XGRom, "XG Romwitch to %1"},
	{" F2A RamSw  ", &F2ARam, "F2A Switch to SRAM bank %1"},
	{" F2A RomSw  ", &F2ARom, "F2A Romwitch to %1"},
	//{ NULL, NULL, NULL  }
};

void make_regline(char *dest, char *src)
{
	char *d = dest;
	while(*src)
	{
		if(*src == '%')
		{
			if(src[1] == '0')
				itoa_size((uint32)memstart, dest, 16, 8, '0');
			else
				itoa_size(REG(src[1]-0x31), dest, 16, 8, '0');
			dest += 8;
			src += 2;
		}
		else
			*dest++ = *src++;
	}
	while(dest-d < 48)
		*dest++ = ' ';
	*dest = 0;
}

void display_functions(void)
{
	int i,c,x;
	int done = 0;
	int marked = 0;
	int curr = 0;
	Function *f;

	int total = sizeof(func_list)/sizeof(Function);
	
	while(!done)
	{
		x = 0;
		for(i=curr; i<curr+4; i++)
		{
			f = &func_list[i];
			if(i == marked)
				con_setcolor(CON_WHITE, CON_RED);
			else
				con_setcolor(CON_WHITE,  CON_BLUE);
			con_printxy(x, 0, f->name);
			x += 12;
		}
		con_flush();

		while((c = getchar()) == EOF)
			Halt();
		
		switch(c)
		{
		case RAWKEY_LEFT:
			marked--;
			break;
		case RAWKEY_RIGHT:
			marked++;
			break;
		case RAWKEY_START:
		case RAWKEY_A:
			done = 1;
			break;
		case RAWKEY_SELECT:
		case RAWKEY_B:
			done = 2;
			break;
		}
		if(marked < 0) marked = 0;
		if(marked >= total) marked = total-1;
		if(marked < curr)
			curr = marked;
		if(marked >= curr+4)
			curr = marked-3;
	}
	
	if(done == 1)
	{
		char tmp[50];
		f = &func_list[marked];

		while(getchar() != EOF);

		con_setcolor(CON_WHITE,  CON_GREEN);
		make_regline(tmp, f->desc);
		con_printxy(0,0, tmp);
		con_printxy(0,15, "(A) = Confirm, (B) = Cancel                     ");
		con_flush();
		while((c = getchar()) == EOF || (c & 0x80))
			Halt();
		
		if(c == RAWKEY_A || c == RAWKEY_START)
			f->func();
	}
	
	reg_changed = 1;
}

void display_memory()
{
	int l,fg,bg;
	int x,y,i,j,k;
	char tmp[52];
	//char tmp2[16];

	tmp[12] = 0;

	j = 0;
	for(y=0; y<lines; y++)
	{
		if(memstart != oldmemstart)
		{
			con_setcolor(CON_CYAN+8, CON_BLACK);
			hexcpy(tmp, &memstart[j], 12);
			con_printxy(36, y+1, tmp);
			con_setcolor(CON_WHITE, CON_BLACK);
		}
		
		for(x=0; x<cols; x++)
		{
			k = j & (bytes-1);
			i = j - 2*k + (bytes-1);
			l = editmode ? modbuf[i] : memstart[i];
			
			if(editmode && memstart[i] != modbuf[i])
				fg = CON_YELLOW + 8;
			else
				fg = CON_WHITE;
			
			if(editmode && y == edity)
			{
				if(x == editx)
					bg = CON_BLUE + 8;
				else
				if((x & ~(bytes-1)) == (editx & ~(bytes-1)))
					bg = CON_BLUE;
				else
					bg = CON_BLACK;
			}
			else
				bg = CON_BLACK;
			
			con_setcolor(fg, bg);

			//if(page_changed || bg != BLACK)
			{
				itoa_size(l, tmp, 16, 2, '0');
				tmp[2] = ' ';
				tmp[2+1] = 0;				
				con_printxy(x*3, y+1, tmp);
			}
			j++;
		}
	}

}

void diss_memory()
{
	int i;
	char tmp[48];
	char *p = &tmp[20];
	uint16 *ptr = (uint16 *)memstart;

	con_setcolor(CON_WHITE, CON_BLACK);
	
	for(i=0; i<14; i++)
	{
		diss_setreg(15, (uint32)ptr);
		sprintf(tmp, "%08x: [%04x]                            ", ptr, *ptr);
		ptr = diss_opcode(ptr, p, 0);
		p[strlen(p)] = ' ';
		con_printxy(0,i+1,tmp);
		con_multicolorxy(0, i+1, 8, CON_CYAN+8, CON_BLACK);
		con_multicolorxy(10, i+1, 6, CON_GRAY, CON_BLACK);
	}

}
/*
int first_line = 0;
int page_size = 0;

void display_text(void)
{
	int i;
	char tmp[48];
	char *d;
	uchar *s = memstart;

	con_setcolor(CON_WHITE, CON_BLACK);
	con_clear(1,14);
	
	for(i=0; i<14; i++)
	{
		d = tmp;
		while((d-tmp < 48) && (*s != 10))
		{
			if(*s >= 0x20 && *s <= 125)
				*d++ = *s++;
			else
				s++;
		}
		*d = 0;
		s++;
		if(!i)
			first_line = s-memstart;
		con_printxy(0,i+1, tmp);
	}
	page_size = s-memstart;
}
*/

void display_page(void)
{
	char tmp[52];
	
	con_setcolor(15, CON_RED);

	sprintf(tmp, "START:%08x [%s] %02d bits    CLIP:[%08x]", memstart, editmode ? "EDIT" : "    ", bytes*8, clipreg);
	con_printxy(0, 0, tmp);
	con_putcolorxy(13-change_nibble, 0, 15, CON_BLUE);

	//con_putxy(5+(7-change_nibble), 0, tmp[5+7-change_nibble]);
	
	if(reg_changed)
	{
		sprintf(tmp, "BNK%01d [%08x] [%08x] [%08x] [%08x]", bank, reg[bank*4],
				reg[bank*4+1], reg[bank*4+2], reg[bank*4+3]);
		con_printxy(0, 15, tmp);

		sprintf(tmp, "[%08x]", reg[bank*4+current]);
		con_setcolor(CON_YELLOW + 8, CON_BLUE);
		con_printxy(5+current*11, 15, tmp);
  
		reg_changed = 0;
	}

	if(!editmode && (memstart == oldmemstart))
		return;
	
	if(mode == MODE_CODE)
		diss_memory();
	else
	/*if(mode == MODE_TEXT)
		display_text();
	else*/
		display_memory();

	
	oldmemstart = memstart;
	page_changed = 0;
}


uint32 getbuf(int o)
{
	if(o < 0)
		return memstart;
	else
	if(o >= lines*cols)
	   return(reg[(o-lines*cols)/4]);

	if(bytes == 1)
		return modbuf[o];
	else
	if(bytes == 2)
		return *((uint16*)(&modbuf[o & 0xFFFFFFFE]));
	else
		return *((uint32*)(&modbuf[o & 0xFFFFFFFC]));
}

void setbuf(int o, uint32 v)
{
	if(o < 0)
		memstart = v;
	else
	if(o >= lines*cols)
		reg[(o-lines*cols)/4] = v;
	else
	{
		if(bytes == 1)
			modbuf[o] = v;
		else
			if(bytes == 2)
				*((uint16*)(&modbuf[o & 0xFFFFFFFE])) = v;
		else
			*((uint32*)(&modbuf[o & 0xFFFFFFFE])) = v;
	}
}


void handle_editmode(int c, int q)
{
	int i;
	int o = editx+edity*cols;
	int adder = 1<<(8*((bytes-1)-o&(bytes-1)));
	
	uint32 v = getbuf(o);
	
	switch(c)
	{
	case RAWKEY_UP:
		if(q == 1)
			bank--;
		else
		if(q == 2)
			setbuf(o, v+adder);
		else
			edity--;
		break;

	case RAWKEY_DOWN:
		if(q == 1)
			bank++;
		else
		if(q == 2)
			setbuf(o, v-adder);
		else
			edity++;
		break;

	case RAWKEY_RIGHT:
		if(q == 1)
			current++;
		else
		if(q == 2)
			setbuf(o, v>>1); //modbuf[o] >>= 1;
		else
			editx++;
		break;

	case RAWKEY_LEFT:
		if(q == 1)
			current--;
		else
		if(q == 2)
			setbuf(o, v<<1); //modbuf[o] <<= 1;
		else
			editx--;
		break;

	case RAWKEY_A:
		if(q == 2)
		{
			setbuf(o, 0);
		}
		else
		if(q == 1)
			setbuf(o, reg[bank*4+current]);
		else
		if(q == 3)
		{
			for(i=0; i<16; i++)
				((uint32 *)modbuf)[i] = reg[i];
		}
		else
			setbuf(o, clipreg);
		break;
			
	case RAWKEY_B:
		if(q == 2)
		{
			setbuf(o, reg[bank*4+current]);
			reg[bank*4+current] = v;	
		}
		else
		if(q == 1)
			reg[bank*4+current] = v;
		else
		if(q == 3)
		{
			for(i=0; i<16; i++)
				reg[i] = ((uint32 *)modbuf)[i];
		}
		else
			clipreg = v;
			
		reg_changed = 1;
		break;

	case RAWKEY_START:
		memcpy8(memstart, modbuf, lines*cols);
		editmode = 0;
		oldmemstart = memstart+1;
		break;

	case RAWKEY_SELECT:
		if(q == 1)
		{
			bytes *= 2;
			if(bytes == 8)
				bytes = 1;
		}
		else
		if(q == 0)
		{
			editmode = 0;
			oldmemstart = memstart+1;
		}
		break;
	}
	
}

void handle_browsemode(int c, int q)
{
	int i, up_add, left_add;
	
	int nib = 1<<(change_nibble<<2);
	
	if(mode == MODE_MEM)
		up_add = cols;
	else
/*	if(mode == MODE_TEXT)
		up_add = first_line;
	else*/
		up_add = 2;

	if(mode == MODE_MEM)
		left_add = 1;
	else
/*	if(mode == MODE_TEXT)
		left_add = page_size;
	else*/
		left_add = 0x1C;

	switch(c)
	{
	case RAWKEY_UP:
		if(q == 1)
			bank--;
		else
		if(q == 2)
			memstart += nib;
		else
		if(q == 3)
			memstart = (uchar *)(((uint32)memstart & 0xFF000000) - 0x01000000);
		else
			memstart -= up_add;
		break;
		
	case RAWKEY_DOWN:
		if(q == 1)
			bank++;
		else
		if(q == 2)
			memstart -= nib;
		else
		if(q == 3)
			memstart = (uchar *)(((uint32)memstart & 0xFF000000) + 0x01000000);
		else
			memstart += up_add;
		break;
		
	case RAWKEY_RIGHT:
		if(q == 1)
			current++;
		else
		if(q == 2)
			change_nibble--;
			//memstart += 0x80;
		else
		if(q == 3)
			memstart += 0x10000;
		else
			memstart += left_add;
		break;
		
	case RAWKEY_LEFT:
		if(q == 1)
			current--;
		else
		if(q == 2)
			//memstart -= 0x80;
			change_nibble++;
		else
		if(q == 3)
			memstart -= 0x10000;
		else
			memstart -= left_add;
		break;
		
	case RAWKEY_A:
		if(q == 1)
		{
			memstart = (uchar *)reg[bank*4+current];
		}
		/*else
		if(q == 3)
		{
			for(i=0; i<16; i++)
				reg[i] = getbuf(i);
		}*/
		else
			memstart = (uchar *)clipreg;
		break;
		
	case RAWKEY_B:
		if(q == 1)
		{
			reg[bank*4+current] = (uint32)memstart;
			reg_changed = 1;
		}
		else
		if(q == 3)
		{
			for(i=0; i<16; i++)
				reg[i] = ((uint32 *)modbuf)[i];
		}
		else
			clipreg = (uint32)memstart;
		break;
	
	case RAWKEY_START:
		display_functions();
		break;
				
	case RAWKEY_SELECT:

		if(q == 1)
		{
			if(mode == MODE_MEM)
			{
				bytes *= 2;
				if(bytes == 8)
					bytes = 1;
			}
		}
		else
		if(q == 2)
		{
			if(mode == MODE_MEM)
			{
				editmode = 1;
				memcpy8(modbuf, memstart, cols*lines);
			}
		}
		else
		{
			mode++;
			if(mode >= MODE_END)
				mode -= MODE_END;
			if(mode == MODE_CODE)
				bytes = 2;
			
			/*if(mode == MODE_TEXT)
				memstart = doc;*/

			oldmemstart = memstart+1;
			reg_changed = 1;
			con_clear(1,14);
		}
		break;
	}
	
	if(change_nibble > 7)
		change_nibble = 7;
	else
	if(change_nibble < 0)
		change_nibble = 0;
	
	page_changed = 1;
}

int main(int argc, char **argv)
{
	int q;
	int count;
	int c,o;
	int ob, oc;
	int quit = 0;
	//Font *font;
	//uchar *p;

	EZWrite(0x09880000, 0x8000);
	
//	dprint("Here we are!");
	
	// Use wram for allocations
	memory_setarea(0);
	modbuf = malloc(16*16);
	//modbuf = (uchar *)((uint32)&empty[256] & 0xFFFFFFFC);
	
	screen_init();
	key_init();

	con_init();	

	display_page();
	con_flush();
	
	//PrintStr("Console Test\n");

	while(!quit)
	{
		while((c = getchar()) == EOF)
			Halt();

		q = key_get_qualifiers();
		
		ob = bank;
		oc = current;
		
		if(editmode)
			handle_editmode(c, q);
		else
			handle_browsemode(c, q);
		
		if(bank != ob || current != oc)
			reg_changed = 1;
		
		//if(c == RAWKEY_START && q == 1)
		//	quit=1;
		
		if(bank < 0) bank = 0;
		if(bank > 3) bank = 3;
		if(current < 0) current = 0;
		if(current > 3) current = 3;
		
		if(bytes > 4) bytes=4;
		
		if(editx < 0)
		{
			editx = cols-1;
			edity--;
		}

		if(editx >= cols)
		{
			if(edity == lines)
			{
				if(editx > 15)
					editx = 15;
			}
			else
			{
				edity++;
				editx=0;
			}
		}
		
		if(edity<0)
		{
			//memstart -= cols;
			edity=0;
		}
			
		if(edity>=lines)
		{
			//memstart += cols;
			edity=lines-1;
		}

		if(edity == lines)
		{
			if(current != editx>>2)
			{
				current = editx/4;
				reg_changed = 1;
			}
		}
	
		display_page();
		con_flush();
	}
	
	return 0;
}

