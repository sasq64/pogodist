#ifndef DISTHUMB_H
#define DISTHUMB_H

uint16 *diss_opcode(uint16 *opcode, char *dest, int flags);
void diss_setreg(int n, uint32 val);

#endif
