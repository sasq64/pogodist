#ifndef CONSOLE_H
#define CONSLE_H

void con_clear(int sl, int el);
void con_init();
void con_setcolor(int fg, int bg);
void con_printxy(int x, int y, char *str);
void con_flush();
void con_putcolorxy(int x, int y, int fg, int bg);
void con_multicolorxy(int x, int y, int len, int fg, int bg);

enum { CON_BLACK, CON_RED, CON_GREEN, CON_YELLOW, CON_BLUE,
CON_PURPLE, CON_CYAN, CON_GRAY, CON_WHITE = 15};

#endif
