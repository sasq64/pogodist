/*
 * MAKEFS sourcecode (makefs.c) - created by Jonas Minnberg 2002
 *
 * Generates rom filesystems and adds to a GBA binary
 *
 * Part of the pogo distribution.
 * Do what you want with this but please credit me.
 * -- jonas@nightmode.org
 ****/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

unsigned char version[8] =
	{ 0x45, 0x45, 0x50, 0x52, 0x4F, 0x4D, 0x5F };

unsigned char st_orig[2][10] =
{
	{0x0E, 0x48, 0x39, 0x68, 0x01, 0x60, 0x0E, 0x48, 0x79, 0x68},
	{0x13, 0x4B, 0x18, 0x60, 0x13, 0x48, 0x01, 0x60, 0x13, 0x49}
};

unsigned char st_repl[2][10] =
{
	{0x00, 0x48, 0x00, 0x47, 0x01, 0xFF, 0xFF, 0x08, 0x79, 0x68},
	{0x01, 0x4C, 0x20, 0x47, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0x08}
};

unsigned char fl_orig[2][24] =
{
	{0xD0, 0x20, 0x00, 0x05, 0x01, 0x88, 0x01, 0x22, 0x08, 0x1C, 0x10, 0x40,
	 0x02, 0x1C, 0x11, 0x04, 0x08, 0x0C, 0x00, 0x28, 0x01, 0xD0, 0x1B, 0xE0},
	{0xD0, 0x21, 0x09, 0x05, 0x01, 0x23, 0x0C, 0x4A, 0x08, 0x88, 0x18, 0x40,
	 0x00, 0x28, 0x08, 0xD1, 0x10, 0x78, 0x00, 0x28, 0xF8, 0xD0, 0x08, 0x88}
};

unsigned char fl_repl[2][24] =
{
	{0xE0, 0x20, 0x00, 0x05, 0x01, 0x88, 0x01, 0x22, 0x08, 0x1C, 0x10, 0x40,
	 0x02, 0x1C, 0x11, 0x04, 0x08, 0x0C, 0x00, 0x28, 0x01, 0xD0, 0x1B, 0xE0},
	{0xE0, 0x21, 0x09, 0x05, 0x01, 0x23, 0x0C, 0x4A, 0x08, 0x88, 0x18, 0x40,
	 0x00, 0x28, 0x08, 0xD1, 0x10, 0x78, 0x00, 0x28, 0xF8, 0xD0, 0x08, 0x88}
};

unsigned char p_repl[2][188] =
{
	{0x39, 0x68, 0x27, 0x48, 0x81, 0x42, 0x23, 0xD0, 0x89, 0x1C, 0x08,
	 0x88, 0x01, 0x28, 0x02, 0xD1, 0x24, 0x48, 0x78, 0x60, 0x33, 0xE0,
	 0x00, 0x23, 0x00, 0x22, 0x89, 0x1C, 0x10, 0xB4, 0x01, 0x24, 0x08,
	 0x68, 0x20, 0x40, 0x5B, 0x00, 0x03, 0x43, 0x89, 0x1C, 0x52, 0x1C,
	 0x06, 0x2A, 0xF7, 0xD1, 0x10, 0xBC, 0x39, 0x60, 0xDB, 0x01, 0x02,
	 0x20, 0x00, 0x02, 0x1B, 0x18, 0x0E, 0x20, 0x00, 0x06, 0x1B, 0x18,
	 0x7B, 0x60, 0x39, 0x1C, 0x08, 0x31, 0x08, 0x88, 0x09, 0x38, 0x08,
	 0x80, 0x16, 0xE0, 0x15, 0x49, 0x00, 0x23, 0x00, 0x22, 0x10, 0xB4,
	 0x01, 0x24, 0x08, 0x68, 0x20, 0x40, 0x5B, 0x00, 0x03, 0x43, 0x89,
	 0x1C, 0x52, 0x1C, 0x06, 0x2A, 0xF7, 0xD1, 0x10, 0xBC, 0xDB, 0x01,
	 0x02, 0x20, 0x00, 0x02, 0x1B, 0x18, 0x0E, 0x20, 0x00, 0x06, 0x1B,
	 0x18, 0x08, 0x3B, 0x3B, 0x60, 0x0B, 0x48, 0x39, 0x68, 0x01, 0x60,
	 0x0A, 0x48, 0x79, 0x68, 0x01, 0x60, 0x0A, 0x48, 0x39, 0x1C, 0x08,
	 0x31, 0x0A, 0x88, 0x80, 0x21, 0x09, 0x06, 0x0A, 0x43, 0x02, 0x60,
	 0x07, 0x48, 0x00, 0x47, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0D, 0x00,
	 0x00, 0x00, 0x0E, 0x04, 0x00, 0x00, 0x0E, 0xD4, 0x00, 0x00, 0x04,
	 0xD8, 0x00, 0x00, 0x04, 0xDC, 0x00, 0x00, 0x04, 0xFF, 0xFF, 0xFF,
	 0x08 },

	{0x22, 0x4C, 0x84, 0x42, 0x20, 0xD0, 0x80, 0x1C, 0x04, 0x88, 0x01, 
	 0x25, 0x2C, 0x40, 0x01, 0x2C, 0x02, 0xD1, 0x80, 0x1E, 0x1E, 0x49, 
	 0x2E, 0xE0, 0x00, 0x23, 0x00, 0x24, 0x80, 0x1C, 0x40, 0xB4, 0x01, 
	 0x26, 0x05, 0x68, 0x35, 0x40, 0x5B, 0x00, 0x2B, 0x43, 0x80, 0x1C, 
	 0x64, 0x1C, 0x06, 0x2C, 0xF7, 0xD1, 0x40, 0xBC, 0xDB, 0x01, 0x02, 
	 0x24, 0x24, 0x02, 0x1B, 0x19, 0x0E, 0x24, 0x24, 0x06, 0x1B, 0x19, 
	 0x19, 0x1C, 0x09, 0x3A, 0x16, 0xE0, 0x12, 0x48, 0x00, 0x23, 0x00, 
	 0x24, 0x40, 0xB4, 0x01, 0x26, 0x05, 0x68, 0x35, 0x40, 0x5B, 0x00, 
	 0x2B, 0x43, 0x80, 0x1C, 0x64, 0x1C, 0x06, 0x2C, 0xF7, 0xD1, 0x40, 
	 0xBC, 0xDB, 0x01, 0x02, 0x24, 0x24, 0x02, 0x1B, 0x19, 0x0E, 0x24, 
	 0x24, 0x06, 0x1B, 0x19, 0x08, 0x3B, 0x18, 0x1C, 0x08, 0x4C, 0x20, 
	 0x60, 0x08, 0x4C, 0x21, 0x60, 0x08, 0x49, 0x80, 0x20, 0x00, 0x06, 
	 0x02, 0x43, 0x0A, 0x60, 0x06, 0x4C, 0x20, 0x47, 0x00, 0x00, 0x00, 
	 0x0D, 0x00, 0x00, 0x00, 0x0E, 0x04, 0x00, 0x00, 0x0E, 0xD4, 0x00, 
	 0x00, 0x04, 0xD8, 0x00, 0x00, 0x04, 0xDC, 0x00, 0x00, 0x04, 0xFF, 
	 0xFF, 0xFF, 0x08 }
};

int p_size[2] = { 188, 168 };

char *find(char *data, int datasize, char *key, int keysize)
{
	char *endp = data + datasize;
	char *endk = key + keysize;
	char *keyp = key;
	int found = 0;

	while(data < endp)
	{
		if(*data == *keyp)
		{
			keyp++;
			if(keyp == endk)
				break;
		}
		else
		if(keyp != key)
		{
			keyp = key;
			data--;
		}
		data++;
	}

	return (keyp == endk) ? (data-keysize+1) : NULL;
}

char *find_freespace(char *data, int size, int freesize)
{
	int endl;
	char *endp = data + size -1;
	char fill = *endp;

	while(*endp == fill) *endp--;
	endp++;

	endl = (int)(endp-data);
	endl = (endl + 255) & 0xFFFFFF00;
	endp = &data[endl];

	if((endp + freesize) < (data + size))
		return endp;
	return NULL;
}



void patch_sram(unsigned char *rom, int size)
{
	unsigned char v0,v1,v2;
	unsigned char *v, *st, *fl, *space;
	
	v = find(&rom[180], size - 180, version, 7);

	if(v)
	{
		v0 = v[8] - '0';
		v1 = v[9] - '0';
		v2 = v[10] - '0';
		printf("Found SRAM library v%d.%d.%d at offset 0x%x\n", v0, v1, v2, v-rom);
		st = find(&rom[180], size-180, st_orig[v1-1], sizeof(st_orig[v1-1]));

		if(st)
		{
			int stloc = st - rom;

			switch(v1)
			{
			case 1:
				p_repl[v1-1][184] = stloc+0x21;
				p_repl[v1-1][186] = stloc>>16;
				break;
			case 2:
				p_repl[v1-1][164] = stloc+0x13;
				p_repl[v1-1][165] = stloc>>8;
				p_repl[v1-1][166] = stloc>>16;
				break;			 
			}

			fl = find(&rom[180], size-180, fl_orig[v1-1], sizeof(fl_orig[v1-1]));
			if(fl)
			{
				memcpy(fl, fl_repl[v1-1], sizeof(fl_repl[v1-1])); 

				space = find_freespace(rom, size, 200);
				if(space)
				{
					int spaceloc = space - rom;
					printf("Found free space at 0x%x\n", space - rom);

					switch(v1) {
					case 1:
						if (*--space == 0xFF)
						{
							p_repl[v1-1][185] = stloc >> 8;	
						} else
						{
							stloc += 0x1F;
							p_repl[v1-1][185] = stloc >> 8;
						}
						st_repl[v1-1][5] = spaceloc >> 8;
						st_repl[v1-1][6] = spaceloc >> 16;
						space++;
						break;
					case 2:
						st_repl[v1-1][7] = spaceloc >> 8;
						st_repl[v1-1][8] = spaceloc >> 16;
						break;
					}
					memcpy(st, st_repl[v1-1], sizeof(st_repl[v1-1]));
					memcpy(space, p_repl[v1-1], p_size[v1-1]); 
					printf("Patch done!\n");
				}
			}
			else
				printf("Couldn't find fl location\n");
		}
		else
			printf("Couldn't find st location\n");
	}
}

#ifdef _WIN32

#include <io.h>

struct dirent
{
	char *d_name;
};

typedef struct
{
	struct dirent de;
	struct _finddata_t fd;
	int handle;
} DIR;


DIR *opendir(char *name)
{
	char tmp[256];
	DIR *dir = malloc(sizeof(DIR));
	dir->de.d_name = NULL;
	sprintf(tmp, "%s/*", name);
	//printf("Looking for %s\n", tmp);
	dir->handle = _findfirst(tmp, &dir->fd);
	return dir;
}

struct dirent *readdir(DIR *dir)
{
	int rc = dir->handle;
	if(dir->de.d_name)
		rc = _findnext(dir->handle, &dir->fd);
	if(rc == -1)
		return NULL;
	dir->de.d_name = dir->fd.name;
	return &dir->de;
}

void closedir(DIR *dir)
{
	_findclose(dir->handle);
	free(dir);
}

#define strcasecmp stricmp 

#else
#include <dirent.h>
#endif // WIN32


typedef struct
{
	char name[32];
	int size;
	int start;

} RomDir;

//#define PAD_FILE(fp) { int l = ftell(fp); if(l % 4) fwrite(&l, 1, 4-(l % 4), fp); }

int fix_roms = 0;
int return_patch = 0;
int sram_patch = 0;
int split256 = 0;

const unsigned char good_header[] =
{
	36,255,174,81,105,154,162,33,61,132,130,10,
	132,228,9,173,17,36,139,152,192,129,127,33,163,82,190,25,
	147,9,206,32,16,70,74,74,248,39,49,236,88,199,232,51,
	130,227,206,191,133,244,223,148,206,75,9,193,148,86,138,192,
	19,114,167,252,159,132,77,115,163,202,154,97,88,151,163,39,
	252,3,152,118,35,29,199,97,3,4,174,86,191,56,132,0,
	64,167,14,253,255,82,254,3,111,149,48,241,151,251,192,133,
	96,214,128,37,169,99,190,3,1,78,56,226,249,162,52,255,
	187,62,3,68,120,0,144,203,136,17,58,148,101,192,124,99,
	135,240,60,175,214,37,228,139,56,10,172,114,33,212,248,7
};
const unsigned char visoly[]=
{
  0x03,0x14,0xa0,0xe3,0x01,0x00,0x8f,0xe2,0x10,0xff,0x2f,0xe1,0x03,
  0xa2,0x4c,0x1c,0x1a,0x23,0x01,0xca,0x01,0xc1,0x01,0x3b,0xfb,0xd1,
  0x20,0x47,0x0f,0xa0,0x7e,0xc8,0x0d,0x80,0x30,0x1c,0x00,0xf0,0x12,
  0xf8,0x15,0x80,0x20,0x1c,0x00,0xf0,0x0e,0xf8,0x0d,0x80,0x15,0x80,
  0x0f,0xa0,0x1c,0x80,0x18,0xc8,0x1e,0x80,0x20,0x1c,0x00,0xf0,0x05,
  0xf8,0x0d,0x80,0x0d,0x49,0x0f,0x80,0x01,0xdf,0x00,0xdf,0xfa,0x27,
  0x7f,0x00,0x10,0x80,0x01,0x3f,0xfc,0xd1,0x70,0x47,0x00,0x00,0x00,
  0x00,0xa8,0xec,0x30,0x09,0x8a,0x46,0x02,0x08,0x00,0xa8,0xec,0x08,
  0x78,0x56,0x00,0x00,0x54,0x53,0x00,0x00,0x34,0x12,0x00,0x00,0xa0,
  0x68,0x02,0x08,0xcd,0xab,0x00,0x00,0x2e,0x59,0x6b,0x09,0xc0,0x00,
  0x00,0x08
};
 
/*
 If file first_child = NULL, otherwise first dir/file in directory
 next is next file/dir in parent dir
*/
typedef struct _DirFile
{
	char name[256];
	int size;
	struct _DirFile *next;
	struct _DirFile *first_child;
	int filetype;
	int offset;
} DirFile;

int bin_offset = 0;


int bincount = 0;
DirFile *binfiles[128];

int gbacount = 0;
DirFile *gbafiles[128];

int stdcount = 0;
DirFile *stdfiles[128];

int dircount = 0;

DirFile *bootfile = NULL;

int excludecount = 0;
char *excludefiles[128];


int filesysoffset = 0;

char *rombuf = NULL;

char *bootname = "pogo.gba";

const char spaces[] = "                                                            ";

void print_tree(DirFile *df, int l)
{
	if(df->first_child)
	{
		printf("%.*s[%s]\n", l*2, spaces, df->name);
		df = df->first_child;
		while(df)
		{
			print_tree(df, l+1);
			df = df->next;
		}
	}
	else
		printf("%.*s%s (%d bytes)\n", l*2, spaces, df->name, df->size);
}


void cutname(char *dst, char *src, int len)
{
	char *p = strrchr(src, '.');
	strncpy(dst, src, len);
	if(p)
		strcpy(&dst[strlen(dst)]-strlen(p), p);
}

int dump_tree(DirFile *df, char *dest)
{
	char *p;
	RomDir tmpdir;
	DirFile *f;

	char *orgdest = dest;

	printf("Dumping %s\n", df->name);

	// Iterate over all files in this directory
	f = df->first_child;
	while(f)
	{

		// Construct name and other data
		memset(tmpdir.name, 0, 32);
		p = strrchr(f->name, '/');
		if(p)
			p++;
		else
			p = f->name;
		cutname(tmpdir.name, p, 31);
		tmpdir.size = f->size;
		tmpdir.start = f->offset - filesysoffset - 8;


		// Is this a dir
		if(f->first_child)
		{
			tmpdir.size |= 0x80000000;
			// Save offset for later patching
			f->offset = (int)dest;
		}

		memcpy(dest, &tmpdir, sizeof(RomDir));
		dest += sizeof(RomDir);
		f = f->next;
	}

	f = df->first_child;
	while(f)
	{
		if(f->first_child)
		{
			// Now we know the offset for this dir
			RomDir *rd = (RomDir *)f->offset;
			rd->start = (dest - rombuf) - filesysoffset - 8;
			dest += dump_tree(f, dest);
		}
		f = f->next;
	}


	return dest-orgdest;

}

int check_exclude(char *name)
{
	int i;
	char *q = strrchr(name, '/');
	if(q) q++;
	else q = name;

	for(i=0; i<excludecount; i++)
		if(strcasecmp(q, excludefiles[i]) == 0)
			return 1;
	return 0;
}

void make_tree(DirFile *df)
{
	DIR *dir;
	struct dirent *d;
	struct stat statbuf;
	DirFile *p, *lastp;
	int l = strlen(df->name);
	if(df->name[l-1] == '/')
		df->name[l-1] = 0;
	if(stat(df->name, &statbuf) != -1)
	{  
		if(statbuf.st_mode & S_IFDIR)
		{
			int i = 0;
			lastp = NULL;
			df->first_child = NULL;
			dir = opendir(df->name);
			p = NULL;
			while((d = readdir(dir)))
			{
				if((strlen(d->d_name) > 2) || d->d_name[0] != '.')
				{
					if(!check_exclude(d->d_name))
					{
						p = malloc(sizeof(DirFile));
						if(df->first_child == NULL)
							df->first_child = p;
						sprintf(p->name, "%s/%s", df->name, d->d_name);
						make_tree(p);
						if(lastp)
							lastp->next = p;
						lastp = p;
						i++;
					}
				}
			}
			if(p)
				p->next = NULL;
			df->size = i*sizeof(RomDir);
			closedir(dir);

			dircount++;
		}
		else
		{
			char *p = strrchr(df->name, '.');
			char *q = strrchr(df->name, '/');
			if(q) q++;
			else q = df->name;

			if(strcasecmp(q, bootname) == 0)
			{
				bootfile = df;
				df->filetype = 4;
			}
			else
			if(p)
			{
				/* Lowercase extention */
				char *q = ++p;
				while(*q)
				{
					*q = tolower(*q);
					q++;
				}

				if(strcmp(p, "bin") == 0)
				{
					df->filetype = 1;
					binfiles[bincount++] = df;
				}
				else
				if(strcmp(p, "gba") == 0)
				{
					df->filetype = 2;
					gbafiles[gbacount++] = df;
				}
				else
				{
					df->filetype = 0;
					stdfiles[stdcount++] = df;
				}
			}
			else
			{
				df->filetype = 0;
				stdfiles[stdcount++] = df;
			}

			df->first_child = NULL;
			df->size = statbuf.st_size;
		}
	}
}


RomDir *rootdir = NULL;

static unsigned int magic = 0xFAB0BABE;

#define BUF_SIZE 64*1024*1024

//unsigned char *buf;

int align_size = 32768;

#define ALIGN_BUF(ptr) {int l = (((int)ptr + align_size-1) & ~(align_size-1)) - (int)ptr; memset(ptr, -1, l); ptr += l;}

#define ALIGN_OFFSET(ptr, offset) {int l = ((offset + align_size-1) & ~(align_size-1)) - offset; memset(&ptr[offset], -1, l); offset += l;}


enum {FLG_ALIGN = 1, FLG_PATCH = 2};

int dump_file(DirFile *file, int offset, int flags)
{
	int rc;
	char *ptr;
	int startoffs = offset;

	FILE *fp = fopen(file->name, "rb");

	if(flags & FLG_ALIGN)
		ALIGN_OFFSET(rombuf, offset)
	else
		offset = (offset + 3) & 0xFFFFFFFC;

	if(!fp)
		printf("Open failed!!!!\n");

	ptr = &rombuf[offset];

	rc = fread(ptr, 1, 32*1024*1024, fp);
	fclose(fp);

	file->offset = offset;

	printf("Adding %s (%d bytes) to offset 0x%x\n", file->name, rc, offset);

	offset += rc;

	if(flags & FLG_PATCH)
	{

		int cc = 0;
		if(fix_roms)
		{
			int j;
			for(j=0xA0; j<0xBD; j++)
				cc += ptr[j];
			ptr[j] = (0-(0x19+cc)) & 0xff;;
			memcpy(&ptr[4], good_header, sizeof(good_header));
			ptr[0xb2] = 0x96;
		}
		
		if(sram_patch)
			patch_sram(ptr, rc);

		if(return_patch)
			memcpy(ptr, visoly, sizeof(visoly));
	}
	
	
	return offset - startoffs;

}


int main(int argc, char **argv)
{
	int rc,i,j,l;
	char *outrom = NULL;
	char *rootdir = NULL;
	FILE *fp;
	DirFile df;
	unsigned char *binstart;
	unsigned char *romstart;
	unsigned char *ptr;

	int offset;

	rombuf = malloc(BUF_SIZE * 2);

	for(i=1;i<argc; i++)
	{
		if(argv[i][0] == '-')
		{
			switch(argv[i][1])
			{
			case 'f':
				printf("Fixing ROMS\n");
				fix_roms = 1;
				break;
			case 'r':
				printf("Applying return-patches to ROMS\n");
				return_patch = 1;
				break;
			case 's':
				printf("Applying sram-patches to ROMS\n");
				sram_patch = 1;
				break;
			case 'x':
				excludefiles[excludecount++] = argv[++i];
				printf("Excluding file %s from filesystem\n", argv[i]);
				break;
			case 'S':
				split256 = 1;
				printf("Aligning roms to not cross 256MBit boundry\n");
				break;
			case 'b':
				bootname = argv[++i];
				printf("Setting file %s as bootfile\n", bootname);
				break;
			case 'a':
				if(strcasecmp(argv[i+1], "xrom") == 0)
				{
					printf("Using XROM alignment\n");
					align_size = 128*1024;
				}
				else
				{
					align_size = atoi(argv[i+1])*1024;
					printf("Setting ROM alignment to %d KByte\n", align_size/1024);
				}
				i++;
				break;
			}
		}
		else
		{
			if(!rootdir)
				rootdir = argv[i];
			else
			if(!outrom)
				outrom = argv[i];
		}
	}


	if(!(outrom && rootdir))
	{
		printf("Usage: makefs [-a <alignment>] [-b <bootfile>] [-x <exclude>] <rootdir> <outfile>\n");
		return 0;
	}

	strcpy(df.name, rootdir);
	df.next = NULL;

	printf("Building File Tree...\n");
	make_tree(&df);
	print_tree(&df, 0);

	offset = 0;

	if(bootfile)
	{
		printf("Dumping bootfile\n");
		offset += dump_file(bootfile, offset, FLG_ALIGN);
	}

	printf("Dumping plugins\n");
	for(i=0; i<bincount; i++)
	{
		offset += dump_file(binfiles[i], offset, FLG_ALIGN);
	}

	ALIGN_OFFSET(rombuf, offset);

	filesysoffset = offset;

	printf("Filesystem at %08x\n", filesysoffset);

	offset += ((dircount + stdcount + bincount + gbacount + 8)  * sizeof(RomDir));

	printf("Dumping files\n");
	for(i=0; i<stdcount; i++)
	{
		offset += dump_file(stdfiles[i], offset, 0);
	}

	printf("Dumping games\n");
	for(i=0; i<gbacount; i++)
	{

		if( split256 && ((offset>>25) != ((gbafiles[i]->size - 1 + offset)>>25)) )
		{
			printf("Passed 256MBit border, aligning from %08x ", offset);
			//We passed a 256MBit limit, align
			offset = (offset + 0x01FFFFFF) & 0xFE000000;
			printf("to %08x\n", offset);
		}

		offset += dump_file(gbafiles[i], offset, FLG_ALIGN | FLG_PATCH);
	}

	ptr = &rombuf[filesysoffset];

	memcpy(ptr, &magic, 4);
	memcpy(ptr+4, &df.size, 4);

	dump_tree(&df, ptr+8);

	fp = fopen(outrom, "wb");
	fwrite(rombuf, offset, 1, fp);
	fclose(fp);

	return 0;
}
