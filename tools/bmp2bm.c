
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef unsigned int uint32;
typedef unsigned short uint16;
typedef unsigned char uchar;


enum { ERR_BMP_OK, ERR_BMP_NOBMP, ERR_BMP_COMPRESS, ERR_BMP_NOTFOUND };

int read_bmp(char *fname, void **dest, int dest_bpp, int *w, int *h, int alpha, int header_size);

int main(int argc, char **argv)
{
	FILE *fp;
	uint16 *pix;
	uint16 w, h, bpp;
	char head[2] = "BM";
	int i,width, height;

	if(argc < 2)
	{
		printf("Usage: bmp2bp <infile.bmp> [outfile.bm]\nConverts a BMP to a 16bit raw pogoshell file\n");
		return 0;
	}

	if(read_bmp(argv[1], (void **)&pix, 16, &width, &height, 0, 0) == ERR_BMP_OK)
	{
		char outfile[256];
		w = width;
		h = height;
		bpp = 16;

		for(i=0; i<width*height; i++)
		{
			if(pix[i] == 0x03E0)
				pix[i] |= 0x8000;
		}

		if(argc == 2)
		{
			char *p;
			strcpy(outfile, argv[1]);
			p = strrchr(outfile, '.');
			if(!p) 
				p = &outfile[strlen(outfile)];
			else
				p++;
			strcpy(p, ".bm");
		}
		else
			strcpy(outfile, argv[2]);

		if(fp = fopen(outfile, "wb"))
		{
			fwrite(&head, 2, 1, fp);
			fwrite(&bpp, 2, 1, fp);
			fwrite(&w, 2, 1, fp);
			fwrite(&h, 2, 1, fp);
			fwrite(pix, 2, width*height, fp);
			fclose(fp);
		}
		else
			printf("Couldn't write output file!\n");
	}
}
