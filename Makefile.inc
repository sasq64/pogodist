
# GLOBAL SETTINGS FOR POGODIST

PREFIX=arm-elf-
CC = $(PREFIX)gcc
CXX  = $(PREFIX)g++
LD = $(PREFIX)gcc
AS = $(PREFIX)as
AR = $(PREFIX)ar
OBJCOPY = $(PREFIX)objcopy

LDFLAGS = -Tlnkscript
OPTFLAGS = -fomit-frame-pointer -Os -funit-at-a-time -fweb
CFLAGS = $(OPTFLAGS) -Wall -c -fno-builtin -mthumb -mthumb-interwork -Os -nostdinc
ASFLAGS = -mthumb-interwork

